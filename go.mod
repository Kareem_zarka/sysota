module gitlab.com/zygoon/sysota

go 1.22

require (
	github.com/godbus/dbus/v5 v5.1.0
	gitlab.com/zygoon/go-cmdr v1.9.0
	gitlab.com/zygoon/go-grub v0.2.0
	gitlab.com/zygoon/go-ini v1.2.0
	gitlab.com/zygoon/go-raspi v1.3.0
	gitlab.com/zygoon/go-squashfstools v1.1.0
	gitlab.com/zygoon/netota v0.3.3
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.1.0 // indirect
)
