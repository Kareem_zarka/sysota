<!--
SPDX-License-Identifier: CC-BY-4.0
SPDX-FileCopyrightText: Huawei Inc.
-->
# 3. Use D-Bus for system-level IPC

Date: 2021-07-09

## Status

Accepted

## Context

OTA may be designed and implemented as either a library that is linked into a
larger application or a service which runs in a separate process. Since linking
heavily impacts the set of available technologies, like implementation language
or memory safety model, it is more desirable for the OTA to be a system service.

In the service model the OTA runs as a separate process, performing necessary
interactions internally. If other applications wish or need to interact with the
update process, they must have a way to perform interactions with the OTA
service. In practice, this is often done over an UNIX socket with the D-Bus
protocol used on top.

D-Bus has extensive integration throughout the system, with libraries available
for nearly every language, command line tools, debuggers, traffic analyzers and
more. Since a typical Linux system uses D-Bus for security critical tasks, as
well as for privilege separation, D-Bus has grown extensive security mechanism,
including bus policy system implemented by dbus-daemon, kernel-enforced access
control implemented as a part of the Linux Security Module (LSM), like AppArmor
and extensive support for sand-boxing like the Flatpak D-Bus proxy mechanism,
making it both practical and easy to mediate access to the OTA service APIs from
system components and applications alike.

This was weight in comparison to developing a custom protocol or using a foreign
protocol, like HTTP over UNIX sockets. Those ideas were considered but abandoned
as development of IPC systems is not seen as competitive advantage of the OTA
stack.

In addition, using D-Bus for control over OTA allows downstream vendors to
implement a management agent in nearly any language without any impact on our
choices. It also allows us to re-implement the OTA stack in another language
down the line, for example in Rust or C, without breaking the API contract.

## Decision

Use D-Bus as the means of exposing APIs from the OTA service to other parts of
the operating system.

## Consequences

Security policy determining which component can use OTA APIs is practical. The
OTA service can be bus-activated, giving the impression that it is always
available while conserving resources when not actively needed, by shutting down
during periods of inactivity.

Applications expecting C-level APIs or JavaScript level APIs will need to either
provide their own bindings using one of the commonly available D-Bus libraries
or we may offer blessed APIs ourselves. In either case this is an extra cost
before the OTA service can be controlled from other parts of the system.

The system must provide a D-Bus system bus, but this is not a new requirement
since Systemd, Network Manager, Modem Manager and other low-level components
depend on it as well.
