<!--
SPDX-License-Identifier: CC-BY-4.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# 2. Use Go as the main implementation language

Date: 2021-07-09

## Status

Accepted

## Context

OTA is a system which pulls binaries from the internet and puts them on the
block device, hoping for the best. It is absolutely critical to implement a
system like that with security in mind, both in the overall architecture, in the
set of dependencies and in the line-by-line implementation. Using a high-level
language with a modern and rich testing ecosystem and memory safety is a big
step towards avoiding security problems.

## Decision

Use Go as the primary development language for the OTA service, client and any
related tools. We will call to C programs where they have a good security track
record or when it is not practical to use an off-the-shelf Go implementation.

Certain libraries may be important enough to link via the Cgo mechanism, where
the system linker instead of the Go linker is used, and any C code can be used
and linked in directly. Using Cgo is discouraged until it cannot be entirely
avoided, as it dilutes the security model.

## Consequences

We have access to a memory-safe, concurrent and productive language. Go's
standard library has a high-quality HTTP client and server implementations,
making writing correct network code easier.

On the other hand the static linking model results in a binary that mostly just
works but in consequence is rather large, so we must be mindful of the size of
the OTA software as a fraction of the overall system image.

Using Go may also prevent us from trivially linking a specific host library,
like SSL stack or similar, resulting in possible inconsistencies.
