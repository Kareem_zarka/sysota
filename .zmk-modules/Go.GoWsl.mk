# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

$(eval $(call ZMK.Import,Go))

Go.GoWsl.Cmd ?= $(strip $(shell PATH="$(PATH):$(Go.Path)/bin" command -v wsl 2>/dev/null))
Go.GoWsl.Options ?=

# Note that wsl doesn't handle full import path, it's therefore omitted from the command below.
.PHONY: static-check-go-wsl
static-check-go-wsl:
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(strip $(Go.GoWsl.Cmd) $(Go.GoWsl.Options) ./...)

# Run "wsl" when running static checks, but only when installed.
# You can install this tool by following instructions on https://github.com/bombsimon/wsl
ifneq (,$(Go.GoWsl.Cmd))
static-check:: static-check-go-wsl
else
$(if $(filter check static-check,$(MAKECMDGOALS)),$(warning The wsl program is not available, static-check target will not depend on static-check-go-wsl))
endif
