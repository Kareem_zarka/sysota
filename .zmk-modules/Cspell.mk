# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

Cspell.CspellCmd ?= $(strip $(shell command -v cspell 2>/dev/null))
Cspell.Options ?=

.PHONY: static-check-cspell-lint
static-check-cspell-lint:
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(strip $(Cspell.CspellCmd) lint $(Cspell.Options))

# Run "cspell lint" when running static checks, but only when npx is installed.
ifneq (,$(Cspell.CspellCmd))
static-check:: static-check-cspell-lint
else
$(if $(filter check static-check,$(MAKECMDGOALS)),$(warning The cspell program is not available, static-check target will not depend on static-check-cspell-lint))
endif
