# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

Spread.Cmd ?= SPREAD_QEMU_KVM=$(if $(wildcard /dev/kvm),1,0) $(strip $(shell command -v spread 2>/dev/null))
Spread.TaskFilter ?= $(if $(value T),$(T))
Spread.Suites ?= $(error Define Spread.SuitesDir to a list of directories containing spread test suites)
Spread.Sources ?= spread.yaml $(patsubst $(ZMK.SrcDir)/%,%,$(foreach suite,$(Spread.Suites),$(shell find $(ZMK.SrcDir)/$(suite) -type f -o -type l)))
Spread.Options ?=

.PHONY: check-spread
check-spread:
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(Spread.Cmd) $(if $(value D),-debug) $(Spread.Options)

ifneq (,$(Spread.Cmd))
check:: check-spread
else
$(if $(filter check,$(MAKECMDGOALS)),$(warning The spread program is not available, check target will not depend on check-spread))
endif
