# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

$(eval $(call ZMK.Import,Go))

Go.GoConst.Cmd ?= $(strip $(shell PATH="$(PATH):$(Go.Path)/bin" command -v goconst 2>/dev/null))
Go.GoConst.Options ?=

# Note that goconst doesn't handle full import path, it's therefore omitted
# from the command below.
.PHONY: static-check-go-goconst
static-check-go-goconst:
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(strip $(Go.GoConst.Cmd) $(Go.GoConst.Options) -set-exit-status ./...)

# Run "goconst" when running static checks, but only when installed.
# You can install this tool by following instructions on https://github.com/jgautheron/goconst
ifneq (,$(Go.GoConst.Cmd))
static-check:: static-check-go-goconst
else
$(if $(filter check static-check,$(MAKECMDGOALS)),$(warning The goconst program is not available, static-check target will not depend on static-check-go-goconst))
endif
