# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

systemd.AtLeastVersion = 244
# There are versions of systemd which provide systemd_system_unit_dir and there
# are some that only provide that variable without any _. Use the more
# compatible version.
systemd.Variables = systemdsystemunitdir systemduserunitdir
$(eval $(call ZMK.Expand,PkgConfig,systemd))

ifneq ($(PkgConfig.Modules.systemd.Status),sufficient)
$(error pkg-config package for systemd is $(PkgConfig.Modules.systemd.Status))
endif

SystemdUnit.Variables = IsUser InstallDir

define SystemdUnit.Template
$1.IsUser ?=
$1.systemdUnitVarName = systemd$$(if $$($1.IsUser),user,system)unitdir
$1.InstallDir ?= $$(or $$(PkgConfig.Modules.systemd.Variables.$$($1.systemdUnitVarName)),$$(error pkg-conifg variable $$($1.systemdUnitVarName) for module systemd is undefined or empty))
$$(eval $$(call ZMK.Expand,InstallUninstall,$1))
endef
