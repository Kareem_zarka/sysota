# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

$(eval $(call ZMK.Import,Go))

Go.GoErrCheck.Cmd ?= $(strip $(shell PATH="$(PATH):$(Go.Path)/bin" command -v errcheck 2>/dev/null))
Go.GoErrCheck.Options ?=

.PHONY: static-check-go-errcheck
static-check-go-errcheck:
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(strip $(Go.GoErrCheck.Cmd) $(Go.GoErrCheck.Options) $(Go.ImportPath)/...)

# Run "errcheck" when running static checks, but only when installed.
# You can install this tool by following instructions on https://github.com/kisielk/errcheck
ifneq (,$(Go.GoErrCheck.Cmd))
static-check:: static-check-go-errcheck
else
$(if $(filter check static-check,$(MAKECMDGOALS)),$(warning The errcheck program is not available, static-check target will not depend on static-check-go-errcheck))
endif
