// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package dbustest

const systemBusPathEnvVar = "DBUS_LAUNCHD_SESSION_BUS_SOCKET"
