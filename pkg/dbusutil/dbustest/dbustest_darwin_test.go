// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package dbustest_test

import (
	"os"
	"strings"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
)

func (s *testbusSuite) TestSetDBusSystemBusAddress(c *C) {
	restore, err := dbustest.SetDBusSystemBusAddress("potato")
	c.Assert(err, IsNil)

	defer func() {
		c.Assert(restore(), IsNil)
	}()

	c.Check(os.Getenv("DBUS_LAUNCHD_SESSION_BUS_SOCKET"), Equals, "potato")
}

func (s *suiteSuite) TestSystemBusAddress(c *C) {
	// Tests see a test system bus.
	addr := os.Getenv("DBUS_LAUNCHD_SESSION_BUS_SOCKET")
	c.Check(strings.HasPrefix(addr, "unix:path=/tmp/test-bus-"), Equals, true)
}
