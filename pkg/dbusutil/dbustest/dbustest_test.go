// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package dbustest_test

import (
	"fmt"
	"os/exec"
	"strings"
	"testing"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
)

func Test(t *testing.T) { TestingT(t) }

type testbusSuite struct{}

var _ = Suite(&testbusSuite{})

func (s *testbusSuite) TestNewTestBus(c *C) {
	_, err := exec.LookPath("dbus-daemon")
	if err != nil {
		c.Skip(fmt.Sprintf("cannot initialize TestBus: %v", err))
	}

	bus, err := dbustest.NewTestBus()
	c.Assert(err, IsNil)

	defer func() {
		c.Check(bus.Close(), IsNil)
	}()

	addr := bus.BusAddress()
	c.Check(strings.HasPrefix(addr, "unix:path=/tmp/test-bus-"), Equals, true)
}

type suiteSuite struct {
	dbustest.Suite
}

var _ = Suite(&suiteSuite{})
