// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package dbusutil_test

import (
	"encoding/xml"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
	"github.com/godbus/dbus/v5/prop"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
)

type serviceHostSuite struct {
	dbustest.Suite

	conn *dbus.Conn
}

var _ = Suite(&serviceHostSuite{})

func (s *serviceHostSuite) SetUpTest(c *C) {
	conn, err := dbusutil.SystemBus()
	c.Assert(err, IsNil)

	s.conn = conn
}

func (s *serviceHostSuite) TearDownTest(c *C) {
	c.Assert(s.conn.Close(), IsNil)
}

type TestService struct {
	conn        *dbus.Conn
	props       *dbusutil.InterfaceProperties
	helloCalled bool
}

func (s *serviceHostSuite) TestUsageInit(c *C) {
	var sh dbusutil.ServiceHost

	ts := TestService{}

	sh.Init(s.conn)

	s.testCommon(c, &sh, &ts)
}

func (s *serviceHostSuite) testCommon(c *C, sh *dbusutil.ServiceHost, ts *TestService) {
	sh.AddHostedService(ts)
	// Bus connection is not relayed yet.
	c.Check(ts.conn, IsNil)

	// Services are exported correctly.
	c.Assert(sh.Export(), IsNil)

	// Register a bus name to make testing easier.
	reply, err := s.conn.RequestName("org.example.Test", dbus.NameFlagDoNotQueue)

	c.Assert(err, IsNil)
	c.Assert(reply, Equals, dbus.RequestNameReplyPrimaryOwner)

	// Bus connection was relayed correctly.
	c.Check(ts.conn, Equals, s.conn)

	// We can call functions correctly.
	obj := s.conn.Object("org.example.Test", "/test")
	err = obj.Call("org.example.TestService.Hello", 0).Store()
	c.Assert(err, IsNil)
	c.Check(ts.helloCalled, Equals, true)

	// Introspection data is correctly published.
	node, err := introspect.Call(obj)
	c.Assert(err, IsNil)
	c.Check(node, DeepEquals, &introspect.Node{
		XMLName: xml.Name{Local: "node"},
		Name:    "/test",
		Interfaces: []introspect.Interface{{
			Name: "org.example.TestService",
			Methods: []introspect.Method{
				{Name: "Hello"}},
			Properties: []introspect.Property{
				{Name: "Const", Type: "s", Access: "read", Annotations: []introspect.Annotation{{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "const"}}},
				{Name: "Notifiable", Type: "s", Access: "read", Annotations: []introspect.Annotation{{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "invalidates"}}},
				{Name: "Observable", Type: "s", Access: "read", Annotations: []introspect.Annotation{{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "true"}}},
				{Name: "Readable", Type: "s", Access: "read", Annotations: []introspect.Annotation{{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "false"}}},
				{Name: "Writable", Type: "s", Access: "readwrite", Annotations: []introspect.Annotation{{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "false"}}}},
		}, {
			Name: "org.freedesktop.DBus.Properties",
			Methods: []introspect.Method{
				{Name: "Get", Args: []introspect.Arg{
					{Name: "interface", Type: "s", Direction: "in"},
					{Name: "property", Type: "s", Direction: "in"},
					{Name: "value", Type: "v", Direction: "out"}}},
				{Name: "GetAll", Args: []introspect.Arg{
					{Name: "interface", Type: "s", Direction: "in"},
					{Name: "props", Type: "a{sv}", Direction: "out"}}},
				{Name: "Set", Args: []introspect.Arg{
					{Name: "interface", Type: "s", Direction: "in"},
					{Name: "property", Type: "s", Direction: "in"},
					{Name: "value", Type: "v", Direction: "in"}}}},
			Signals: []introspect.Signal{
				{Name: "PropertiesChanged", Args: []introspect.Arg{
					{Name: "interface", Type: "s", Direction: "out"},
					{Name: "changed_properties", Type: "a{sv}", Direction: "out"},
					{Name: "invalidates_properties", Type: "as", Direction: "out"}}}},
		}, {
			Name: "org.freedesktop.DBus.Introspectable",
			Methods: []introspect.Method{{
				Name: "Introspect", Args: []introspect.Arg{
					{Name: "out", Type: "s", Direction: "out"}}}}}}})

	// We can look at the properties we've exported.
	c.Check(ts.props.GetMust("Const"), Equals, "value-const")
	c.Check(ts.props.GetMust("Notifiable"), Equals, "value-notifiable")
	c.Check(ts.props.GetMust("Observable"), Equals, "value-observable")
	c.Check(ts.props.GetMust("Readable"), Equals, "value-readable")
	c.Check(ts.props.GetMust("Writable"), Equals, "value-writable")

	val, err := ts.props.Get("Const")
	c.Assert(err, IsNil)
	c.Assert(val, Equals, dbus.MakeVariant("value-const"))

	allProps, err := ts.props.GetAll()
	c.Assert(err, IsNil)
	c.Check(allProps, DeepEquals, map[string]dbus.Variant{
		"Const":      dbus.MakeVariant("value-const"),
		"Notifiable": dbus.MakeVariant("value-notifiable"),
		"Observable": dbus.MakeVariant("value-observable"),
		"Readable":   dbus.MakeVariant("value-readable"),
		"Writable":   dbus.MakeVariant("value-writable"),
	})

	// We can modify at the properties we've exported.
	c.Assert(ts.props.Set("Writable", dbus.MakeVariant("new-value")), IsNil)
	c.Check(ts.props.GetMust("Writable").(string), DeepEquals, "new-value")
	ts.props.SetMust("Writable", "newer-value")
	c.Check(ts.props.GetMust("Writable"), Equals, "newer-value")

	// We can unexport everything correctly.
	c.Assert(sh.Unexport(), IsNil)

	// After that we cannot call things anymore.
	err = s.conn.Object("org.example.Test", "/test").Call("org.example.TestService.Hello", 0).Store()
	c.Assert(err, ErrorMatches, "Object does not implement the interface 'org.example.TestService'")
}

func (s *serviceHostSuite) TestUsageNewServiceHost(c *C) {
	sh := dbusutil.NewServiceHost(s.conn)
	ts := TestService{}

	sh.AddHostedService(&ts)

	s.testCommon(c, sh, &ts)
}

func (s *serviceHostSuite) TestMisuse(c *C) {
	sh := dbusutil.NewServiceHost(s.conn)

	c.Assert(sh.Export(), IsNil)
	c.Assert(func() { _ = sh.Export() }, PanicMatches, `Export already called`)
	c.Assert(func() { sh.AddHostedService(&TestService{}) }, PanicMatches, `AddHostedService cannot be used after calling Export`)
}

func (ts *TestService) Hello() *dbus.Error {
	ts.helloCalled = true
	return nil
}

func (ts *TestService) JoinServiceHost(reg dbusutil.ServiceRegistration) error {
	ts.conn = reg.DBusConn()

	ifaceReg := reg.Object("/test").Interface("org.example.TestService")

	ifaceReg.SetMethods(map[string]interface{}{
		"Hello": ts.Hello,
	})

	ts.props = ifaceReg.SetProperties(map[string]*prop.Prop{
		"Const":      {Value: "value-const", Emit: prop.EmitConst},
		"Readable":   {Value: "value-readable"},
		"Writable":   {Value: "value-writable", Writable: true},
		"Observable": {Value: "value-observable", Emit: prop.EmitTrue},
		"Notifiable": {Value: "value-notifiable", Emit: prop.EmitInvalidates},
	})

	ifaceReg.SetIntrospection(&introspect.Interface{
		Name: "org.example.TestService",
		Methods: []introspect.Method{
			{
				Name: "Hello",
			},
		},
	})

	return nil
}
