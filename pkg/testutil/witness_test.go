// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package testutil_test

import (
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/testutil"
)

type witnessSuite struct{}

var _ = Suite(&witnessSuite{})

func (s *witnessSuite) TestSmoke(c *C) {
	var w testutil.CallWitness

	c.Check(w.Count(), Equals, int32(0))
	c.Check(w.Once(), Equals, false)

	w.Witness()
	c.Check(w.Count(), Equals, int32(1))
	c.Check(w.Once(), Equals, true)

	w.Witness()
	c.Check(w.Count(), Equals, int32(2))
	c.Check(w.Once(), Equals, false)
}

func (s *witnessSuite) TestGo(c *C) {
	var w testutil.CallWitness

	ch := make(chan struct{})

	go func() {
		w.Witness()
		ch <- struct{}{}
	}()
	<-ch

	c.Check(w.Count(), Equals, int32(1))
	c.Check(w.Once(), Equals, true)
}
