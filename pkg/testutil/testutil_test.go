// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package testutil_test

import (
	"os/exec"
	"testing"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/testutil"
)

func Test(t *testing.T) { TestingT(t) }

type suite struct {
	testutil.CommandSuite
}

var _ = Suite(&suite{})

func (s *suite) TestMockCommand(c *C) {
	restore, inspect := s.MockCommand(c, "prog")

	defer restore()

	c.Check(inspect(c), HasLen, 0)

	c.Assert(exec.Command("prog").Run(), IsNil)
	c.Check(inspect(c), DeepEquals, []string{"prog"})

	c.Assert(exec.Command("prog", "--arg1").Run(), IsNil)
	c.Assert(exec.Command("prog", "--arg2").Run(), IsNil)
	c.Check(inspect(c), DeepEquals, []string{"prog --arg1", "prog --arg2"})
}
