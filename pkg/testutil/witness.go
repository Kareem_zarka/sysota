// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package testutil

import "sync/atomic"

// CallWitness maintains a call counter for test functions.
type CallWitness int32

// Witness increments the call counter.
func (w *CallWitness) Witness() {
	atomic.AddInt32((*int32)(w), 1)
}

// Once returns true if Witness was called exactly once.
func (w *CallWitness) Once() bool {
	return atomic.LoadInt32((*int32)(w)) == 1
}

// Count returns the number of times Witness was called.
func (w *CallWitness) Count() int32 {
	return atomic.LoadInt32((*int32)(w))
}
