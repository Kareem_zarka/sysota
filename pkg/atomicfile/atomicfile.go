// SPDX-License-Identifier: LicenseRef-GoLicense
// SPDX-FileCopyrightText: The Go Authors

// Package atomicfile implements atomic write-rename operation.
package atomicfile

import (
	"errors"
	"os"
	"path/filepath"
)

var (
	errCrossDirectoryRename = errors.New("cannot rename across directories")
)

// File represents an open file descriptor to a temporary file.
//
// Unlike plain os.File, this File remembers its name across updates.
type File struct {
	*os.File
	newName string
}

// CreateTemp returns a file with a unique name.
//
// The pattern not include a path separator. It may optionally contain "*" as a
// hint on where to place a variable while constructing unique file names.
func CreateTemp(dir, pattern string) (*File, error) {
	f, err := os.CreateTemp(dir, pattern)
	if err != nil {
		return nil, err
	}

	return &File{File: f}, nil
}

// Name returns the name as presented to Open but keeps track of rename.
func (f *File) Name() string {
	if f.newName != "" {
		return f.newName
	}

	return f.File.Name()
}

// AtomicRename renames the file to a given name atomically.
//
// The file must not be closed at the time this function is called.
func (f *File) AtomicRename(newName string) error {
	dirName := filepath.Dir(f.Name())
	if dirName != filepath.Dir(newName) {
		return errCrossDirectoryRename
	}

	d, err := os.Open(dirName)
	if err != nil {
		return err
	}

	defer func() {
		_ = d.Close()
	}()

	// As the first step of the best-effort attempt to atomically replace a
	// file, synchronize the content of the temporary file.
	if err := f.Sync(); err != nil {
		return err
	}

	// As the second step of the best-effort attempt to atomically replace a
	// file, rename the temporary file over to the desired name.
	if err := os.Rename(f.Name(), newName); err != nil {
		return err
	}

	f.newName = newName

	// As the third and last step of best-effort attempt to atomically replace a
	// file, synchronize the directory it resides in.
	return d.Sync()
}

// RemoveUnlessRenamed removes the file if it was not yet renamed by AtomicRename.
func (f *File) RemoveUnlessRenamed() error {
	if f.newName != "" {
		return nil
	}

	return os.Remove(f.File.Name())
}
