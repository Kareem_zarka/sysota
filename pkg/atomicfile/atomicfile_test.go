// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package atomicfile_test

import (
	"os"
	"path/filepath"
	"strings"
	"testing"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/atomicfile"
)

func Test(t *testing.T) { TestingT(t) }

type fileSuite struct {
	d string
}

var _ = Suite(&fileSuite{})

func (s *fileSuite) SetUpTest(c *C) {
	s.d = c.MkDir()
}

func (s *fileSuite) TestEmptyNamePattern(c *C) {
	f, err := atomicfile.CreateTemp(s.d, "")
	c.Assert(err, IsNil)

	defer func() {
		c.Assert(f.Close(), IsNil)
	}()

	c.Check(filepath.Dir(f.Name()), Equals, s.d)
	c.Check(filepath.Base(f.Name()), Not(Equals), "")
}

func (s *fileSuite) TestSpecificNamePattern(c *C) {
	f, err := atomicfile.CreateTemp(s.d, "potato-*")
	c.Assert(err, IsNil)

	defer func() {
		c.Assert(f.Close(), IsNil)
	}()

	c.Check(filepath.Dir(f.Name()), Equals, s.d)
	c.Check(strings.HasPrefix(filepath.Base(f.Name()), "potato-"), Equals, true)
}

func (s *fileSuite) TestRenamingWorks(c *C) {
	f, err := atomicfile.CreateTemp(s.d, "tmp-*")
	c.Assert(err, IsNil)

	defer func() {
		c.Assert(f.Close(), IsNil)
	}()

	n, err := f.Write([]byte("potato"))
	c.Assert(err, IsNil)
	c.Check(n, Equals, 6)

	newName1 := filepath.Join(s.d, "name-1.txt")
	c.Assert(f.AtomicRename(newName1), IsNil)

	data, err := os.ReadFile(newName1)
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte("potato"))

	newName2 := filepath.Join(s.d, "name-2.txt")
	c.Assert(f.AtomicRename(newName2), IsNil)

	data, err = os.ReadFile(newName2)
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte("potato"))
}

func (s *fileSuite) TestNameIsTracked(c *C) {
	f, err := atomicfile.CreateTemp(s.d, "tmp-*")
	c.Assert(err, IsNil)

	defer func() {
		c.Assert(f.Close(), IsNil)
	}()

	c.Check(filepath.Dir(f.Name()), Equals, s.d)
	c.Check(strings.HasPrefix(filepath.Base(f.Name()), "tmp-"), Equals, true)

	newName1 := filepath.Join(s.d, "name-1.txt")
	c.Assert(f.AtomicRename(newName1), IsNil)
	c.Check(f.Name(), Equals, newName1)

	newName2 := filepath.Join(s.d, "name-2.txt")
	c.Assert(f.AtomicRename(newName2), IsNil)
	c.Check(f.Name(), Equals, newName2)
}

func (s *fileSuite) TestRenameViolations(c *C) {
	f, err := atomicfile.CreateTemp(s.d, "tmp-*")
	c.Assert(err, IsNil)

	defer func() {
		c.Assert(f.Close(), IsNil)
	}()

	c.Check(f.AtomicRename(filepath.Join(s.d, "dir", "name.txt")), ErrorMatches, "cannot rename across directories")
}

func (s *fileSuite) TestTemporaryFilesAreRemoved(c *C) {
	f, err := atomicfile.CreateTemp(s.d, "tmp-*")
	c.Assert(err, IsNil)

	defer func() {
		c.Assert(f.Close(), IsNil)
	}()

	c.Assert(f.RemoveUnlessRenamed(), IsNil)

	files, err := os.ReadDir(s.d)
	c.Assert(err, IsNil)
	c.Assert(files, HasLen, 0)
}

func (s *fileSuite) TestRenamedFilesAreKept(c *C) {
	f, err := atomicfile.CreateTemp(s.d, "tmp-*")
	c.Assert(err, IsNil)

	defer func() {
		c.Assert(f.Close(), IsNil)
	}()

	newName := filepath.Join(s.d, "name.txt")
	c.Assert(f.AtomicRename(newName), IsNil)
	c.Assert(f.RemoveUnlessRenamed(), IsNil)

	files, err := os.ReadDir(s.d)
	c.Assert(err, IsNil)
	c.Assert(files, HasLen, 1)
	c.Assert(files[0].Name(), Equals, "name.txt")
}
