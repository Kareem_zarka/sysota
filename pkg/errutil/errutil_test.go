// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package errutil_test

import (
	"bytes"
	"errors"
	"io"
	"testing"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/errutil"
)

func Test(t *testing.T) { TestingT(t) }

type errutilSuite struct {
	buf bytes.Buffer
	old io.Writer
}

var _ = Suite(&errutilSuite{})

func (s *errutilSuite) SetUpSuite(c *C) {
	s.old = errutil.ErrorWriter
	errutil.ErrorWriter = &s.buf
}

func (s *errutilSuite) TearDownSuite(c *C) {
	errutil.ErrorWriter = s.old
}

func (s *errutilSuite) SetUpTest(c *C) {
	s.buf.Reset()
}

func (s *errutilSuite) TestForward(c *C) {
	var err error

	// Forward forward first non-nin error.
	errutil.Forward(&err, nil, errors.New("potato"))
	c.Check(err, ErrorMatches, "potato")

	// Forward does not clobber errors.
	errutil.Forward(&err, errors.New("onion"))
	c.Check(err, ErrorMatches, "potato")
	c.Check(s.buf.String(), Equals, "unable to forward error: onion\n")
}
