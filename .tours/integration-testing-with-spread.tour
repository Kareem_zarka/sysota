{
  "$schema": "https://aka.ms/codetour-schema",
  "title": "Integration Testing With Spread",
  "steps": [
    {
      "file": "spread.yaml",
      "description": "Integration tests are implemented with the Spread tool.\n\nSpread reads a spread.yaml file to know what kind of tests, or tasks, exist and how to execute them.",
      "line": 4,
      "title": "What is spread?"
    },
    {
      "file": "spread.yaml",
      "description": "Spread is a full-system integration test environment. Spread works by allocating systems where tests are executed.\n\nSpread can allocate local virtual machines, containers or remote virtual machines from cloud providers. All of those are grouped by *backends*.\n\nIn SystemOTA we use the `qemu` and `lxd` backends, which operate on locally-created virtual machines and containers, respectively.",
      "line": 6,
      "title": "Spread backends"
    },
    {
      "file": "spread.yaml",
      "description": "The LXD backend is used for most of the integration tests. It is widely supported and uses very little memory.\n\nInside the LXD backend, we define the single `ubuntu-21.04` system. This tells spread that we want to use a container with Ubuntu 20.04, or Hirsute Hippo.",
      "line": 7,
      "title": "The LXD backend"
    },
    {
      "file": "spread.yaml",
      "description": "We also use the QEMU backend, which creates virtual machines locally.\n\nEach backend type has some strengths and weaknesses. QEMU is much heavier to run but works well inside containers, while LDX does not. Inside QEMU, we can also mount file systems, something that LXD sandbox disallows.\n\nLater you can see how each test, or task, can define the set of backends or systems it is compatible or incompatible with.\nWe have several tests that rely on RAUC mounting file systems, and we mark those as incompatible with LXD.",
      "line": 10,
      "title": "The QEMU backend"
    },
    {
      "file": "spread.yaml",
      "description": "Spread `path` is the place inside the allocated machine where spread will copy your project source tree to.\n\nBy default spread copies all the files in the source tree. We exclude the cache directory that may be present inside a CI environment but other than that, you can assume that all the files in your local workspace system are available in the testing environment. This makes it easy to have files that are needed just for testing and to access them when the time is right.",
      "line": 18,
      "title": "Spread project path"
    },
    {
      "file": "spread.yaml",
      "description": "The prepare section is a snippet of bash script that is executed once, when Spread prepares a given machine. If spread allocates multiple machines, perhaps to exercise the code in multiple environments or perhaps to parallelize test execution, the prepare section is executed once inside each machine.\n\nIf you ask spread to reuse an existing machine it had created before, the prepare section is re-executed each time, allowing you to evolve both the code and the test environment as you make progress.",
      "line": 24,
      "title": "Project-wide prepare script"
    },
    {
      "file": "spread.yaml",
      "description": "Similarly to prepare, the restore section cleans up after all tests have executed.\n\nThis pattern of prepare - restore is recurring throughout Spread. You can have prepare/restore logic at the level of the whole project, like we see now, but also at the level of a backend, suite and task.",
      "line": 53,
      "title": "Project-wide restore script"
    },
    {
      "file": "spread.yaml",
      "description": "Spread organizes tasks into suites.\n\nEach suite is a directory with tasks and must be declared in the top-level `spread.yaml` file. Once you declare a suite, all tasks inside it are found automatically.\n\nA suite has a name, summary for the humans to understand and a set of prepare/restore sections.",
      "line": 61,
      "title": "Spread task suites"
    },
    {
      "file": "spread.yaml",
      "description": "The `prepare-each` section is a snippet of bash that executes before each task present in the suite. This construct is mirrored by `restore-each` and allows moving common prepare/restore logic from the level of a single task to the level of the suite, where it applies to all the tasks.\n\nThis is different from suite-level `prepare` and `restore`, which only execute once, before the first task of a suite is executed. You can think of suite-level `prepare` as a more scoped project-level `prepare`.",
      "line": 64,
      "title": "Suite-level prepare-each scripts"
    },
    {
      "file": "spread.yaml",
      "description": "The environment section can be placed at the level of a project, backend, suite or task. It defines environment variables that are set during execution of the tasks contained in that scope.\n\nWe use several variables to point to directories or files, to void having to copy-paste fragile strings. Spread runs bash with `set -e` active, which allows us to spot mistakes, like typos in a variable name, more easily because any such mistake causes the test to fail.",
      "line": 82,
      "title": "Defining environment variables"
    },
    {
      "file": "spread.yaml",
      "description": "The `debug-each` instructs spread to execute commands in case any test inside the suite fails. We commonly use it to look at system logs, inspect services and files. This is most useful in non-interactive tests, where all you get is a log file and looking at additional information in case something fails may be useful to debug the problem.",
      "line": 158,
      "title": "Debugging with debug and debug-each"
    },
    {
      "file": "rauc/tests/rauc-install-bad/task.yaml",
      "description": "We've looked at the top-level `spread.yaml` long enough. Time to look at a `task.yaml` file.\n\nEach such file represents a single test or task spread can execute.\n\nEach task file is placed in a separate directory. Spread executes each task from the directory where it is defined it, making it practical to store temporary files or logs.\n\nThis task examines what happens when `rauc install` is used to install a bundle, pretending that the update fails by configuring the running system in a particular way.",
      "line": 4,
      "title": "Spread tasks"
    },
    {
      "file": "rauc/tests/rauc-install-bad/task.yaml",
      "description": "This test relies on RAUC mounting the update bundle so it cannot execute in LXD. LXD sandbox prevents containers from mounting new file systems outside of FUSE-based userspace file systems. Since RAUC is not doing that, this test will only run in other environments, most prominently QEMU. ",
      "line": 8,
      "title": "Spread tasks and backends"
    },
    {
      "file": "rauc/tests/rauc-install-bad/task.yaml",
      "description": "While not executable, the `details` section provides useful context to the human who is reading or perhaps debugging the test. It's like an optional, multi-line summary line.",
      "line": 9,
      "title": "Describing task details to other humans"
    },
    {
      "file": "rauc/tests/rauc-install-bad/task.yaml",
      "description": "This test is also special because it uses *variants*.\n\nVariants are a way to easily run a test several times, changing some aspect as expressed by a different value of a particular environment variable. Variants are encoded in the particular syntax of a environment variable definition.\nThe syntax is `VAR/variant` where `VAR` is the variable name, here `AFTER_REBOOT_SLOT`, followed by `/` and the name of the variant.\n\nVariant name is present in the name of the task, allowing developers to easily differentiate failures or run a specific variant by hand. Here the variant decides if slot `A` or `B` should be seen as the booted slot after a pretended reboot.\n\nYou can use variants to avoid copying code and covering more execution space with fewer things to maintain.",
      "line": 13,
      "title": "Spread task variants"
    },
    {
      "file": "spread.yaml",
      "description": "Back to `spread.yaml`. We will now look at the specific test suites we have defined and what they contain.\n\nThe first suite is placed in `cmd/sysotad/spread.suite/`. It contains various assorted tests as it was the first test suite that was defined. Over time tests placed here may move to a more appropriate location.\n\nWhen moving tests across suites please keep in mind that the test most likely relies on what happens in the suite `prepare` or `prepare-each` snippets. It's good practice to organize suites so that they are scoped but otherwise have a familiar environment, so less context is required to understand what happens in practice.",
      "line": 62,
      "title": "The sysotad suite"
    },
    {
      "file": "spread.yaml",
      "description": "The next suite is the `boot/piboot/spread.suite/`.\n\nHere we mainly look at how the `piboot` boot-loader backend operates by calling it in a simulated environment.",
      "line": 80,
      "title": "The piboot suite"
    },
    {
      "file": "spread.yaml",
      "description": "The man suite looks at the properties of our manual pages.\n\nIt's distinctive by the lack of any prepare/restore logic. Most tests are heavier in what they need to operate.\n\nMoving tests into lighter suites is one way to keep your overall test execution fast. Do less if you can.",
      "line": 177,
      "title": "The man suite"
    },
    {
      "file": "spread.yaml",
      "description": "The final test suite is `rauc/tests/`.\n\nHere we check both what RAUC does by itself and how we interact with RAUC.",
      "line": 179,
      "title": "The RAUC suite"
    },
    {
      "file": "README.md",
      "description": "This concludes our introduction to integration tests with spread.\n\nRemember that to run the test suite you need to use `spread`. We use a fork called `oh-spread` that you can install from one of the sources listed below. The upstream version of Spread may work as well but may be missing compatibility patches.\n\nHappy testing and good luck!",
      "line": 58,
      "title": "Running spread tests and closing words"
    }
  ]
}