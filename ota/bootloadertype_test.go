// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ota_test

import (
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/ota"
)

type bootLoaderTypeSuite struct{}

var _ = Suite(&bootLoaderTypeSuite{})

func (*bootLoaderTypeSuite) TestString(c *C) {
	c.Check(ota.InertBootLoader.String(), Equals, "inert")
	c.Check(ota.TestBoot.String(), Equals, "test")
	c.Check(ota.PiBoot.String(), Equals, "pi-boot")
	c.Check(ota.GRUB.String(), Equals, "GRUB")
	c.Check(ota.BootLoaderType(42).String(), Equals, "BootLoaderType(42)")
}

func (*bootLoaderTypeSuite) TestMarshalText(c *C) {
	data, err := ota.InertBootLoader.MarshalText()
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte("inert"))

	_, err = ota.TestBoot.MarshalText()
	c.Assert(err, ErrorMatches, `test boot loader type cannot be marshaled`)

	data, err = ota.PiBoot.MarshalText()
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte("pi-boot"))

	data, err = ota.GRUB.MarshalText()
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte("GRUB"))

	_, err = ota.BootLoaderType(42).MarshalText()
	c.Assert(err, ErrorMatches, `invalid boot loader type: 42`)
}

func (*bootLoaderTypeSuite) TestUnmarshalText(c *C) {
	var typ ota.BootLoaderType

	err := typ.UnmarshalText([]byte(`inert`))
	c.Assert(err, IsNil)
	c.Check(typ, Equals, ota.InertBootLoader)

	err = typ.UnmarshalText([]byte(`test`))
	c.Assert(err, ErrorMatches, `cannot unmarshal boot loader type from "test"`)

	err = typ.UnmarshalText([]byte(`pi-boot`))
	c.Assert(err, IsNil)
	c.Check(typ, Equals, ota.PiBoot)

	err = typ.UnmarshalText([]byte(`GRUB`))
	c.Assert(err, IsNil)
	c.Check(typ, Equals, ota.GRUB)

	err = typ.UnmarshalText([]byte(`potato`))
	c.Assert(err, ErrorMatches, `cannot unmarshal boot loader type from "potato"`)
}
