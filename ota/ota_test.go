// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ota_test

import (
	"errors"
	"os"
	"path/filepath"
	"runtime"
	"testing"

	"gitlab.com/zygoon/go-ini"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/ota"
)

func Test(t *testing.T) { TestingT(t) }

type confSuite struct{}

var _ = Suite(&confSuite{})

func (s *confSuite) TestEncodeConfPiBootLoader(c *C) {
	conf := ota.Config{
		BootLoaderType:        ota.PiBoot,
		BootPartitionMountDir: "/boot/firmware",
	}
	data, err := ini.Marshal(conf)
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, ""+
		"[OTA]\n"+
		"BootLoaderType=pi-boot\n"+
		"BootPartitionMountDir=/boot/firmware\n")
}

func (s *confSuite) TestLoadConfigMissingFiles(c *C) {
	d := c.MkDir()

	conf, err := ota.LoadConfig([]string{filepath.Join(d, "one.conf"), filepath.Join(d, "two.conf")})
	c.Assert(err, IsNil)
	c.Check(conf, DeepEquals, &ota.Config{})
}

func (s *confSuite) TestDefaultConfigFiles(c *C) {
	c.Check(ota.DefaultConfigFiles(), DeepEquals, []string{
		"/usr/lib/sysota/sysotad.conf",
		"/etc/sysota/sysotad.conf",
		"/run/sysota/sysotad.conf",
	})
}

const otaConfForPiBoot = `
[OTA]
BootLoaderType=pi-boot
BootPartitionMountDir=/boot/firmware
`

func (s *confSuite) TestLoadConfig(c *C) {
	d := c.MkDir()
	name := filepath.Join(d, "sysotad.conf")

	c.Assert(os.WriteFile(name, []byte(otaConfForPiBoot), 0600), IsNil)

	conf, err := ota.LoadConfig([]string{name})
	c.Assert(err, IsNil)
	c.Check(conf, DeepEquals, &ota.Config{
		BootLoaderType:        ota.PiBoot,
		BootPartitionMountDir: "/boot/firmware",
	})
}

func (s *confSuite) TestLoadConfigConfigPriority(c *C) {
	d := c.MkDir()
	lower := filepath.Join(d, "lower.conf")
	higher := filepath.Join(d, "higher.conf")

	c.Assert(os.WriteFile(lower, []byte("[OTA]\nBootLoaderType=pi-boot\n"), 0600), IsNil)
	c.Assert(os.WriteFile(higher, []byte("[OTA]\nBootLoaderType=inert\n"), 0600), IsNil)

	conf, err := ota.LoadConfig([]string{lower, higher})
	c.Assert(err, IsNil)
	c.Check(conf, DeepEquals, &ota.Config{BootLoaderType: ota.InertBootLoader})
}

func (s *confSuite) TestLoadConfigCorruptedFile(c *C) {
	d := c.MkDir()
	broken := filepath.Join(d, "broken.conf")
	c.Assert(os.WriteFile(broken, []byte("[BootL...!@#!$%"), 0600), IsNil)

	_, err := ota.LoadConfig([]string{broken})
	c.Assert(err, ErrorMatches, `cannot load configuration: .*[\\/]broken.conf: cannot decode line 1: unexpected input: .*`)
	// If necessary, wrapped errors can be extracted and analyzed
	var cfgErr *ota.ConfigError

	c.Assert(errors.As(err, &cfgErr), Equals, true)
	c.Assert(cfgErr.FileName, Equals, broken)

	var decErr *ini.DecoderError

	c.Assert(errors.As(err, &decErr), Equals, true)
	c.Assert(decErr, ErrorMatches, `cannot decode line 1: unexpected input: .*`)
}

func (s *confSuite) TestLoadConfigInaccessibleFile(c *C) {
	if runtime.GOOS == "windows" {
		c.Skip("cannot run on Windows, test depends on UNIX permissions")
	}

	if os.Geteuid() == 0 {
		c.Skip("cannot run as root, test depends on chmod")
	}

	d := c.MkDir()
	inaccessible := filepath.Join(d, "inaccessible.conf")
	c.Assert(os.WriteFile(inaccessible, []byte("[BootLoader]\nType=foo\n"), 0600), IsNil)
	c.Assert(os.Chmod(inaccessible, 0o200), IsNil)

	_, err := ota.LoadConfig([]string{inaccessible})
	c.Assert(err, ErrorMatches, `cannot load configuration: .* open .*: permission denied`)
	c.Assert(errors.Is(err, os.ErrPermission), Equals, true)
}

func (s *confSuite) TestSaveConfig(c *C) {
	cfg := &ota.Config{
		UpdateServerURL: "test-server",
		UpdateStream:    "test-stream",
	}
	d := c.MkDir()
	n := filepath.Join(d, "config.ini")

	err := ota.SaveConfig(cfg, n)
	c.Assert(err, IsNil)

	data, err := os.ReadFile(n)
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, ""+
		"[Update]\n"+
		"ServerURL=test-server\n"+
		"Stream=test-stream\n")
}

func (s *confSuite) TestSaveConfigProblems(c *C) {
	cfg := &ota.Config{
		BootLoaderType: ota.TestBoot, // TestBoot is non-marshalable.
	}
	d := c.MkDir()
	n := filepath.Join(d, "config.ini")

	err := ota.SaveConfig(cfg, n)
	c.Assert(err, ErrorMatches, "cannot save configuration: cannot encode Config.BootLoaderType: test boot loader type cannot be marshaled")

	_, err = os.Stat(n)
	c.Assert(errors.Is(err, os.ErrNotExist), Equals, true)
}

// Tests for state guard and config guard are nearly identical. After Go 1.18
// both types should be unified to a Guard type and the tests de-duplicated.

var _ = Suite(&configGuardSuite{})

type configGuardSuite struct {
	guard ota.ConfigGuard

	// For LoadConfig and SaveConfig.
	saved *ota.Config
	ioErr error
}

func (s *configGuardSuite) SetUpTest(c *C) {
	s.ioErr = nil
	s.saved = &ota.Config{}

	c.Assert(s.guard.BindAndLoad(s), IsNil)

	s.saved = nil
}

func (s *configGuardSuite) SaveConfig(cfg *ota.Config) error {
	if s.ioErr != nil {
		return s.ioErr
	}

	s.saved = cfg

	return nil
}

func (s *configGuardSuite) LoadConfig() (*ota.Config, error) {
	if s.ioErr != nil {
		return nil, s.ioErr
	}

	return s.saved, nil
}

func (s *configGuardSuite) TestConfigFiles(c *C) {
	c.Check(s.guard.ConfigFiles(), Equals, s)
}

func (s *configGuardSuite) TestGetSetConfig(c *C) {
	s.saved = nil
	c.Check(s.guard.Config(), Equals, ota.Config{})
	c.Assert(s.guard.SetConfig(&ota.Config{BootLoaderType: ota.PiBoot}), IsNil)
	c.Check(s.guard.Config(), Equals, ota.Config{BootLoaderType: ota.PiBoot})
	c.Check(s.saved, DeepEquals, &ota.Config{BootLoaderType: ota.PiBoot})
}

func (s *configGuardSuite) TestLoadConfigIOError(c *C) {
	s.saved = &ota.Config{BootLoaderType: ota.PiBoot}
	s.ioErr = errBoom

	c.Assert(s.guard.LoadConfig(), ErrorMatches, "boom")
	c.Check(s.guard.Config(), Equals, ota.Config{BootLoaderType: ota.InertBootLoader})
}

func (s *configGuardSuite) TestLoadConfig(c *C) {
	s.saved = &ota.Config{BootLoaderType: ota.PiBoot}
	c.Check(s.guard.LoadConfig(), IsNil)
	c.Check(s.guard.Config(), Equals, ota.Config{BootLoaderType: ota.PiBoot})
}

func (s *configGuardSuite) TestSaveConfigIOError(c *C) {
	s.ioErr = errBoom

	// Failures are reported but do not cause the in-memory change to be discarded.
	c.Assert(s.guard.SetConfig(&ota.Config{BootLoaderType: ota.PiBoot}), ErrorMatches, "boom")
	c.Check(s.saved, IsNil)
	c.Check(s.guard.Config(), Equals, ota.Config{BootLoaderType: ota.PiBoot})
	c.Check(s.saved, IsNil)

	// If we retry successfully without any actual change, we still get saved.
	s.ioErr = nil
	c.Assert(s.guard.SetConfig(&ota.Config{BootLoaderType: ota.PiBoot}), IsNil)
	c.Check(s.saved, DeepEquals, &ota.Config{BootLoaderType: ota.PiBoot})
}

func (s *configGuardSuite) TestAlterConfigSuccess(c *C) {
	err := s.guard.AlterConfig(func(cfg *ota.Config) (bool, error) {
		cfg.BootLoaderType = ota.PiBoot

		return true, nil
	})

	c.Assert(err, IsNil)
	c.Check(s.guard.Config(), Equals, ota.Config{BootLoaderType: ota.PiBoot})
	c.Check(s.saved, DeepEquals, &ota.Config{BootLoaderType: ota.PiBoot})
}

type configObserverKey struct{}

func (s *configGuardSuite) TestObserver(c *C) {
	s.guard.RegisterObserver(configObserverKey{}, func(st *ota.Config) error {
		c.Assert(st.BootLoaderType, Equals, ota.PiBoot)
		return nil
	})
	defer s.guard.RemoveObserver(configObserverKey{})

	err := s.guard.AlterConfig(func(st *ota.Config) (bool, error) {
		st.BootLoaderType = ota.PiBoot

		return true, nil
	})

	c.Assert(err, IsNil)
	c.Check(s.guard.Config(), Equals, ota.Config{BootLoaderType: ota.PiBoot})
	c.Check(s.saved, DeepEquals, &ota.Config{BootLoaderType: ota.PiBoot})
}

func (s *configGuardSuite) TestObserverError(c *C) {
	s.guard.RegisterObserver(configObserverKey{}, func(st *ota.Config) error {
		c.Assert(st.BootLoaderType, Equals, ota.PiBoot)
		return errBoom
	})
	defer s.guard.RemoveObserver(configObserverKey{})

	err := s.guard.AlterConfig(func(st *ota.Config) (bool, error) {
		st.BootLoaderType = ota.PiBoot

		return true, nil
	})

	c.Assert(err, ErrorMatches, "boom")
	c.Check(s.guard.Config(), Equals, ota.Config{BootLoaderType: ota.InertBootLoader})
	c.Check(s.saved, IsNil)
}

func (s *configGuardSuite) TestAlterConfigFailure(c *C) {
	err := s.guard.AlterConfig(func(st *ota.Config) (bool, error) {
		st.BootLoaderType = ota.PiBoot

		return false, errBoom
	})

	c.Assert(err, ErrorMatches, "boom")
	c.Check(s.guard.Config(), Equals, ota.Config{BootLoaderType: ota.InertBootLoader})
	c.Check(s.saved, IsNil)
}

func (s *configGuardSuite) TestAlterConfigUnchanged(c *C) {
	err := s.guard.AlterConfig(func(st *ota.Config) (bool, error) {
		st.BootLoaderType = ota.PiBoot

		return false, nil
	})

	c.Assert(err, IsNil)
	c.Check(s.guard.Config(), Equals, ota.Config{BootLoaderType: ota.InertBootLoader})
	c.Check(s.saved, IsNil)
}

func (s *configGuardSuite) TestZeroValue(c *C) {
	var g ota.ConfigGuard // Config file is not bound

	c.Check(g.Config(), Equals, ota.Config{})
	c.Assert(g.SetConfig(&ota.Config{BootLoaderType: ota.PiBoot}), IsNil)
	c.Check(g.AlterConfig(func(*ota.Config) (bool, error) { return true, nil }), IsNil)
}
