#!/bin/sh
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.
cat >/backup.log << BACKUP_LOG
backup.sh invoked at $(date).

Running processes:
$(ps aux)

Mounted file systems:
$(cat /proc/self/mountinfo)

Number of postgres proceses: $(pgrep --count postgres)
BACKUP_LOG

cat >/backup.inc << BACKUP_INC
NUM_POSTGRES_DURING_BACKUP=$(pgrep --count postgres)
BACKUP_INC
