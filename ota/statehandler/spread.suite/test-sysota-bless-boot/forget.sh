#!/bin/sh
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.
cat >/forget.log << FORGET_LOG
forget.sh invoked at $(date).

Running processes:
$(ps aux)

Mounted file systems:
$(cat /proc/self/mountinfo)

Number of postgres proceses: $(pgrep --count postgres)
FORGET_LOG

cat >/forget.inc << FORGET_INC
NUM_POSTGRES_DURING_FORGET=$(pgrep --count postgres)
FORGET_INC
