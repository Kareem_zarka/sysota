# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: explore the behavior of systemd related to state management logic

environment:
  TEST_CRITICAL_SERVICE_CMD/true: /bin/true
  TEST_CRITICAL_SERVICE_CMD/false: /bin/false
prepare: |
  # TODO(zyga): test the real service.

  cat >/etc/systemd/system/test-sysota-bless-boot.service <<MARK_GOOD_SERVICE
  [Unit]
  # This may replace the equivalent RAUC service.
  Description=Mark the boot as good
  DefaultDependencies=no
  Requires=boot-complete.target
  After=local-fs.target boot-complete.target
  Conflicts=shutdown.target
  Before=shutdown.target

  [Service]
  # This may even just set the status of the slot to good, as RAUC does.
  ExecStart=$(pwd)/forget.sh
  Type=oneshot
  RemainAfterExit=true

  [Install]
  WantedBy=basic.target
  MARK_GOOD_SERVICE

  cat >/etc/systemd/system/test-critical.service <<CRITICAL_SERVICE
  [Unit]
  Description=Test service critical for system operation

  [Service]
  ExecStart=$TEST_CRITICAL_SERVICE_CMD
  Type=oneshot

  [Install]
  WantedBy=basic.target
  CRITICAL_SERVICE

  mkdir -p /etc/systemd/system/boot-complete.target.d
  cat  >/etc/systemd/system/boot-complete.target.d/test-critical.conf <<CONF
  [Unit]
  # Inject dependency on the test critical service.
  # In a real system this could be any of the services deemed essential for operation.
  Requires=test-critical.service
  After=test-critical.service
  CONF

  cat  >/etc/systemd/system/boot-complete.target.d/test-critical.conf <<CONF
  [Unit]
  # Inject dependency on the postgresql service.
  # We measure the state of postgresql when the "forget" state handler is invoked.
  Requires=postgresql.service
  After=postgresql.service
  CONF

  systemctl enable test-critical.service
  systemctl enable test-sysota-bless-boot.service

  # XXX: is this still required?
  test "$(systemctl is-enabled systemd-boot-check-no-failures.service)" = disabled
  systemctl enable systemd-boot-check-no-failures.service
  test "$(systemctl is-enabled systemd-boot-check-no-failures.service)" = enabled

execute: |
  if [ $SPREAD_REBOOT = 0 ]; then
    REBOOT
  fi

  case "$TEST_CRITICAL_SERVICE_CMD" in
    /bin/true)
      # The system is normal.
      test "$(systemctl is-system-running --wait)" = running

      # bless-boot is active because the test-critical.service is working.
      test "$(systemctl is-active test-sysota-bless-boot.service)" = active
      systemctl status systemd-boot-check-no-failures.service

      # When we were working, postgresql was already operational.
      test -f /forget.log
      . /forget.inc
      test "$NUM_POSTGRES_DURING_FORGET" -gt 0
      ;;
    /bin/false)
      # The system is degraded because the test-critical.service has failed to start.
      test "$(systemctl is-system-running --wait)" = degraded

      # bless-boot is inactive because the test-critical.service has failed to start.
      test "$(systemctl is-active test-sysota-bless-boot.service)" = inactive
      systemctl status systemd-boot-check-no-failures.service || true

      test ! -f /forget.log
      ;;
  esac

restore: |
  systemctl disable systemd-boot-check-no-failures.service
  systemctl disable test-critical.service
  systemctl disable test-sysota-bless-boot.service

  test "$(systemctl is-enabled systemd-boot-check-no-failures.service)" = disabled
  test "$(systemctl is-enabled test-sysota-bless-boot.service)" = disabled

  rm -f /etc/systemd/system/test-sysota-bless-boot.service
  rm -f /etc/systemd/system/boot-complete.target.d/test-critical.conf
  rm -f /etc/systemd/system/boot-complete.target.d/postgresql.conf
  rmdir /etc/systemd/system/boot-complete.target.d
  rm -f /forget.log
  rm -f /forget.inc

debug: |
  systemctl --failed || true
