#!/bin/sh
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.
cat >/restore.log << RESTORE_LOG
restore.sh invoked at $(date).

Running processes:
$(ps aux)

Mounted file systems:
$(cat /proc/self/mountinfo)

Number of postgres proceses: $(pgrep --count postgres)
RESTORE_LOG

cat >/restore.inc << RESTORE_INC
NUM_POSTGRES_DURING_RESTORE=$(pgrep --count postgres)
RESTORE_INC
