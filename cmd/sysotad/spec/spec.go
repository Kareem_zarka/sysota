// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package spec defines the shared specification between parts of SysOTA D-Bus
// service components (hosted services), the service host and the client.
package spec

const (
	// BusName is the D-Bus bus name used by the SystemOTA service.
	BusName = "org.oniroproject.sysota1"

	// ObjectPath is the D-Bus object path used by the SystemOTA service.
	ObjectPath = "/org/oniroproject/sysota1/Service"

	// MakerProperty is the name of the Maker D-Bus property.
	MakerProperty = "Maker"
	// ModelProperty is the name of the Model D-Bus property.
	ModelProperty = "Model"

	// ServerURLProperty is the name of the ServerURL D-Bus property.
	ServerURLProperty = "ServerURL"
	// SystemImagePackageProperty is the name of the SystemImagePackage D-Bus property.
	SystemImagePackageProperty = "SystemImagePackage"
	// StreamProperty is the name of the Stream D-Bus property.
	StreamProperty = "Stream"

	// InstalledPackageNameProperty is the name of the InstalledPackageName D-Bus property.
	InstalledPackageNameProperty = "InstalledPackageName"
	// InstalledPackageVersionProperty is the name of the InstalledPackageVersion D-Bus property.
	InstalledPackageVersionProperty = "InstalledPackageVersion"
	// InstalledSourceRevisionProperty is the name of the InstalledSourceRevision D-Bus property.
	InstalledSourceRevisionProperty = "InstalledSourceRevision"

	// GetStreamsMethod is the name of the GetStreams D-Bus method.
	GetStreamsMethod = "GetStreams"
	// UpdateMethod is the name of the Update D-Bus method.
	UpdateMethod = "Update"
)
