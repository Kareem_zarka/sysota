// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotad

import (
	"fmt"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/zygoon/go-raspi/pimodel"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/boot/grub"
	"gitlab.com/zygoon/sysota/boot/piboot"
	"gitlab.com/zygoon/sysota/cmd/sysotad/boothosted"
	"gitlab.com/zygoon/sysota/cmd/sysotad/raucadapter"
	"gitlab.com/zygoon/sysota/cmd/sysotad/raucboothosted"
	"gitlab.com/zygoon/sysota/cmd/sysotad/raucinstallhosted"
	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/cmd/sysotad/testhosted"
	"gitlab.com/zygoon/sysota/cmd/sysotad/updatehosted"
	"gitlab.com/zygoon/sysota/dirs"
	"gitlab.com/zygoon/sysota/ota"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
)

// RequestBusName requests a well-known name on the bus.
//
// Bus name must be claimed after creating and exporting the service
// object, as it is used as an indicator that a systemd-service is ready
// for accepting connections.
func RequestBusName(conn *dbus.Conn) error {
	reply, err := conn.RequestName(svcspec.BusName, dbus.NameFlagDoNotQueue)
	if err != nil {
		return err
	}

	if reply != dbus.RequestNameReplyPrimaryOwner {
		return fmt.Errorf("cannot claim bus name %s", svcspec.BusName)
	}

	return nil
}

// Option is a function for configuring the service during construction.
type Option func(*Service) error

// WithConfigFiles returns a ServiceOption for defining the service ConfigFiles interface.
//
// When used, the service knows how to load and save configuration on demand. Without it
// the service runs with all-defaults that are never saved.
func WithConfigFiles(cf ota.ConfigFiles) Option {
	return func(svc *Service) error {
		return svc.confGuard.BindAndLoad(cf)
	}
}

// WithStateFile returns a ServiceOption for defining the service StateFile interface.
//
// When used, the service knows how to load and save the state on demand. Without it
// the service is stateless and all state changes are discarded.
func WithStateFile(sf ota.StateFile) Option {
	return func(svc *Service) error {
		return svc.stateGuard.BindAndLoad(sf)
	}
}

// WithExitChannel returns a ServiceOption for defining a service exit channel.
func WithExitChannel(exit chan<- struct{}) Option {
	return func(svc *Service) error {
		svc.exit = exit
		return nil
	}
}

// Service implements the D-Bus service capable of downloading system updates.
type Service struct {
	dbusutil.ServiceHost

	stateGuard ota.StateGuard
	confGuard  ota.ConfigGuard

	update    updatehosted.HostedService
	bootProto boot.Protocol

	test testhosted.HostedService
	exit chan<- struct{}
}

var bootProviders = map[ota.BootLoaderType]func(config *ota.Config) (boot.Protocol, error){
	ota.InertBootLoader: func(config *ota.Config) (boot.Protocol, error) { return nil, nil },
	ota.PiBoot: func(config *ota.Config) (proto boot.Protocol, err error) {
		revCode := config.OverrideRevisionCode
		serialNum := config.OverrideSerialNumber

		// Probe the real values if the overrides are not both set.
		if revCode == 0 || serialNum == 0 {
			revCode, serialNum, err = pimodel.ProbeCPUInfo(pimodel.ProcCPUInfoFile)
			if err != nil {
				return nil, err
			}
		}

		return piboot.New(config.BootPartitionMountDir, dirs.Proc, revCode, serialNum)
	},
	ota.GRUB: func(config *ota.Config) (proto boot.Protocol, err error) {
		return grub.New(config.GrubEnvPath), nil
	},
}

type unsupportedBootLoaderError ota.BootLoaderType

func (e unsupportedBootLoaderError) Error() string {
	return fmt.Sprintf("unsupported boot loader type: %s", ota.BootLoaderType(e))
}

// configuredBootProtocol returns the boot protocol described by the configuration.
func configuredBootProtocol(cfgGuard *ota.ConfigGuard) (boot.Protocol, error) {
	cfg := cfgGuard.Config()
	if loader, ok := bootProviders[cfg.BootLoaderType]; ok {
		return loader(&cfg)
	}

	return nil, unsupportedBootLoaderError(cfg.BootLoaderType)
}

// NewService returns a new service.
//
// In practice, you want to call it with both the WithConfigurer WithStater
// service options. In their absence the service will run devoid of
// configuration and state and all changes will be ephemeral.
func NewService(conn *dbus.Conn, opts ...Option) (*Service, error) {
	svc := &Service{}
	svc.ServiceHost.Init(conn)

	for _, opt := range opts {
		if err := opt(svc); err != nil {
			return nil, err
		}
	}

	bootProto, err := configuredBootProtocol(&svc.confGuard)
	if err != nil {
		return nil, err
	}

	svc.bootProto = bootProto

	// Ask the boot protocol to delay the reboot process if this is supported.
	if delayer, ok := bootProto.(boot.RebootDelayer); ok {
		delayer.SetRebootDelay(time.Duration(svc.confGuard.Config().QuirksRebootDelay) * time.Second)
	}

	// Configure and add the boot service if the corresponding
	// debug flag is enabled.
	if svc.confGuard.Config().DebugBootAPI {
		bootService := &boothosted.HostedService{}
		bootService.Init(bootProto)
		svc.AddHostedService(bootService)
	}

	// Configure and add the update service.
	svc.update.Init(&svc.confGuard, &svc.stateGuard)
	svc.AddHostedService(&svc.update)

	// Configure and add the RAUC boot handler and install handler services
	adapter := raucadapter.New(bootProto, svc)
	svc.AddHostedService(raucboothosted.New(adapter))
	svc.AddHostedService(raucinstallhosted.New(adapter))

	svc.setupTestService()

	return svc, nil
}

// setupTestService configures and adds the test service
// if the corresponding debug flag is enabled in the configuration system.
func (svc *Service) setupTestService() {
	if !svc.confGuard.Config().DebugTestAPI {
		return
	}

	svc.test.Init(&svc.confGuard, &svc.stateGuard, svc.exit)

	svc.AddHostedService(&svc.test)
}

// BootMode implements raucadapter.BootModeHolder and returns the current boot mode.
func (svc *Service) BootMode() boot.Mode {
	return svc.stateGuard.State().BootMode
}

// SetBootMode implements raucadapter.BootModeHolder and sets the new boot mode.
func (svc *Service) SetBootMode(bootMode boot.Mode, isRollback bool) error {
	return svc.stateGuard.AlterState(func(st *ota.SystemState) (bool, error) {
		var changed bool

		if isRollback {
			_, _ = fmt.Printf("ROLLBACK to %s (%s), installed source revision kept at %s, pending reset\n",
				st.InstalledPackageName, st.InstalledPackageVersion, st.InstalledSourceRevision)

			changed = changed || st.PendingPackageName != ""
			st.PendingPackageName = ""
			changed = changed || st.PendingPackageVersion != ""
			st.PendingPackageVersion = ""
			changed = changed || st.PendingSourceRevision != ""
			st.PendingSourceRevision = ""
		} else {
			if bootMode == boot.Normal {
				_, _ = fmt.Printf("COMMIT to %s (%s), installed revision updated to %s, pending reset\n",
					st.PendingPackageName, st.PendingPackageVersion, st.PendingSourceRevision)

				changed = changed || st.InstalledPackageName != st.PendingPackageName
				st.InstalledPackageName = st.PendingPackageName
				changed = changed || st.InstalledPackageName != st.PendingPackageName
				st.InstalledPackageVersion = st.PendingPackageVersion
				changed = changed || st.InstalledSourceRevision != st.PendingSourceRevision
				st.InstalledSourceRevision = st.PendingSourceRevision

				changed = changed || st.PendingPackageName != ""
				st.PendingPackageName = ""
				changed = changed || st.PendingPackageVersion != ""
				st.PendingPackageVersion = ""
				changed = changed || st.PendingSourceRevision != ""
				st.PendingSourceRevision = ""
			}
		}

		changed = changed || st.BootMode != bootMode
		st.BootMode = bootMode

		return changed, nil
	})
}

// IsIdle returns true if all the hosted service are idle.
func (svc *Service) IsIdle() bool {
	// Pending reboot always postpones service shutdown.
	if proto, ok := svc.bootProto.(boot.RebootDelayer); ok && proto.RebootPending() {
		return false
	}

	return svc.update.IsIdle()
}
