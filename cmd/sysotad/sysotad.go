// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package sysotad implements the sysotad system service.
package sysotad

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/sysota/ota"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/errutil"
)

// Options describe input to sysotad necessary to find configuration and state.
type Options struct {
	// IdleDuration defines the frequency of inactivity checks.
	IdleDuration time.Duration
	ConfigFiles  []string
	StateFile    string
}

// DefaultOptions returns options suitable for normal operation.
func DefaultOptions() *Options {
	return &Options{
		IdleDuration: time.Minute,
		ConfigFiles:  ota.DefaultConfigFiles(),
		StateFile:    ota.StateFileName,
	}
}

// Name is the name of the sysotad command in the sysota-mux executable.
const Name = "sysotad"

// Cmd implements the sysotad service.
type Cmd struct {
	Opts *Options
}

// OneLiner returns a help message when used inside Router.
func (Cmd) OneLiner() string {
	return "SysOTA D-Bus service"
}

// Run implements the sysotad binary.
func (cmd *Cmd) Run(ctx context.Context, _ []string) (err error) {
	_, stdout, _ := cmdr.Stdio(ctx)

	// Connect to the system bus and defer disconnect on return.
	conn, err := dbusutil.SystemBus()
	if err != nil {
		return err
	}

	defer func() {
		errutil.Forward(&err, conn.Close())
	}()

	exitCh := make(chan struct{})
	defer close(exitCh)

	// Create the SystemOTA service.
	svc, err := NewService(conn,
		WithConfigFiles(&ConfigFiles{
			ReadFiles: cmd.Opts.ConfigFiles,
			WriteFile: ota.DefaultStatefulConfigFile(),
		}),
		WithStateFile(StateFile(cmd.Opts.StateFile)),
		WithExitChannel(exitCh),
	)
	if err != nil {
		return err
	}

	// Export the service to D-Bus having finished configuration.
	if err := svc.Export(); err != nil {
		return err
	}

	// Request bus name after exporting all objects, to signal readiness to systemd.
	if err := RequestBusName(conn); err != nil {
		return err
	}

	_, _ = fmt.Fprintf(stdout, "Listening ...\n")

	// Set up a ticker to check for inactivity.
	idleTicker := time.NewTicker(cmd.Opts.IdleDuration)
	defer idleTicker.Stop()

	run := true
	for run {
		select {
		// Check if the service is idle and quit if so.
		case <-idleTicker.C:
			if svc.IsIdle() {
				_, _ = fmt.Fprintln(stdout, "Exiting due to inactivity")
				run = false
			}
		case <-ctx.Done():
			_, _ = fmt.Fprintf(stdout, "Exiting due to interrupt\n")
			run = false
		case <-exitCh:
			_, _ = fmt.Fprintf(stdout, "Exiting due to exit request\n")
			run = false
		}
	}

	return nil
}
