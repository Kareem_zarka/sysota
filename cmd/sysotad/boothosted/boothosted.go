// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package boothosted contains the SysOTA boot protocol component of sysotad.
package boothosted

import (
	"errors"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"

	"gitlab.com/zygoon/sysota/boot"
	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
)

const (
	// InterfaceName is the name of the SystemOTA BootLoader interface.
	InterfaceName = "org.oniroproject.sysota1.BootLoader"
)

// IntrospectData is the D-Bus introspection data for the BootLoader interface of the service object.
var IntrospectData = introspect.Interface{
	Name: InterfaceName,
	Methods: []introspect.Method{
		{
			Name: "QueryActive",
			Args: []introspect.Arg{
				{
					Name:      "slot",
					Type:      "s",
					Direction: dbusutil.Out,
				},
			},
		},
		{
			Name: "QueryInactive",
			Args: []introspect.Arg{
				{
					Name:      "slot",
					Type:      "s",
					Direction: dbusutil.Out,
				},
			},
		},
		{
			Name: "TrySwitch",
			Args: []introspect.Arg{
				{
					Name:      "slot",
					Type:      "s",
					Direction: dbusutil.In,
				},
			},
		},
		{
			Name: "Reboot",
			Args: []introspect.Arg{
				{
					Name:      "flags",
					Type:      "u",
					Direction: dbusutil.In,
				},
			},
		},
		{
			Name: "CommitSwitch",
		},
		{
			Name: "CancelSwitch",
		},
	},
}

var errNoBootProtocol = errors.New("boot protocol is not set")

// HostedService is a dbusutil.HostedService exporting boot.Protocol over D-BUs.
type HostedService struct {
	bootProto boot.Protocol
}

func (hs *HostedService) Init(proto boot.Protocol) {
	hs.bootProto = proto
}

// JoinServiceHost integrates with dbusutil.ServiceHost.
func (hs *HostedService) JoinServiceHost(reg dbusutil.ServiceRegistration) error {
	ifaceReg := reg.Object(svcspec.ObjectPath).Interface(InterfaceName)

	ifaceReg.SetMethods(map[string]interface{}{
		"QueryActive":   hs.QueryActive,
		"QueryInactive": hs.QueryInactive,
		"TrySwitch":     hs.TrySwitch,
		"Reboot":        hs.Reboot,
		"CommitSwitch":  hs.CommitSwitch,
		"CancelSwitch":  hs.CancelSwitch,
	})
	ifaceReg.SetIntrospection(&IntrospectData)

	return nil
}

// QueryActive exposes the boot.Protocol.QueryActive method over D-Bus.
func (hs *HostedService) QueryActive() (string, *dbus.Error) {
	if hs.bootProto == nil {
		return "", dbus.MakeFailedError(errNoBootProtocol)
	}

	slot, err := hs.bootProto.QueryActive()
	if err != nil {
		return "", dbus.MakeFailedError(err)
	}

	return slot.String(), nil
}

// QueryInactive exposes the boot.Protocol.QueryInactive method over D-Bus.
func (hs *HostedService) QueryInactive() (string, *dbus.Error) {
	if hs.bootProto == nil {
		return "", dbus.MakeFailedError(errNoBootProtocol)
	}

	slot, err := hs.bootProto.QueryInactive()
	if err != nil {
		return "", dbus.MakeFailedError(err)
	}

	return slot.String(), nil
}

// TrySwitch exposes the boot.Protocol.TrySwitch method over D-Bus.
func (hs *HostedService) TrySwitch(slotName string) *dbus.Error {
	if hs.bootProto == nil {
		return dbus.MakeFailedError(errNoBootProtocol)
	}

	var slot boot.Slot
	if err := slot.UnmarshalText([]byte(slotName)); err != nil {
		return dbus.MakeFailedError(err)
	}

	if err := hs.bootProto.TrySwitch(slot); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}

// Reboot exposes the boot.Protocol.Reboot method over D-Bus.
//
// The flags argument is not translated in any way.
func (hs *HostedService) Reboot(flags uint) *dbus.Error {
	if hs.bootProto == nil {
		return dbus.MakeFailedError(errNoBootProtocol)
	}

	if err := hs.bootProto.Reboot(boot.RebootFlags(flags)); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}

// CommitSwitch exposes the boot.Protocol.CommitSwitch method over D-Bus.
func (hs *HostedService) CommitSwitch() *dbus.Error {
	if hs.bootProto == nil {
		return dbus.MakeFailedError(errNoBootProtocol)
	}

	if err := hs.bootProto.CommitSwitch(); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}

// CancelSwitch exposes the boot.Protocol.CancelSwitch method over D-Bus.
func (hs *HostedService) CancelSwitch() *dbus.Error {
	if hs.bootProto == nil {
		return dbus.MakeFailedError(errNoBootProtocol)
	}

	if err := hs.bootProto.CancelSwitch(); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}
