// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package testhosted contains a hosted service used for testing SystemOTA.
package testhosted

import (
	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"

	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/ota"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
)

const (
	// InterfaceName is the name of the debug interface of the service.
	InterfaceName = "org.oniroproject.sysota1.Test"

	loadStateFunc = "LoadState"
	saveStateFunc = "SaveState"

	loadConfigFunc = "LoadConfig"
	saveConfigFunc = "SaveConfig"

	exitFunc = "Exit"
)

// IntrospectData is the D-Bus introspection data for DebugService interface of the service object.
//
// It is provided explicitly without using reflection for a bit more control
// over the provided data, given that we implement the properties interface
// ourselves.
var IntrospectData = introspect.Interface{
	Name: InterfaceName,
	Methods: []introspect.Method{
		{
			Name: loadStateFunc,
		},
		{
			Name: saveStateFunc,
		},
		{
			Name: loadConfigFunc,
		},
		{
			Name: saveConfigFunc,
		},
		{
			Name: exitFunc,
			Annotations: []introspect.Annotation{{
				Name:  "org.freedesktop.DBus.Method.NoReply",
				Value: "true",
			}},
		},
	},
}

// HostedService exposes debugging functions not meant for production.
type HostedService struct {
	cfgGuard *ota.ConfigGuard
	stGuard  *ota.StateGuard
	exit     chan<- struct{}
}

// Init initializes Service with parts of the Service data.
func (hs *HostedService) Init(cfgGuard *ota.ConfigGuard, stGuard *ota.StateGuard, exit chan<- struct{}) {
	hs.cfgGuard = cfgGuard
	hs.stGuard = stGuard
	hs.exit = exit
}

// JoinServiceHost integrates with dbusutil.ServiceHost.
func (hs *HostedService) JoinServiceHost(reg dbusutil.ServiceRegistration) error {
	ifaceReg := reg.Object(svcspec.ObjectPath).Interface(InterfaceName)

	ifaceReg.SetMethods(map[string]interface{}{
		loadStateFunc:  hs.LoadState,
		saveStateFunc:  hs.SaveState,
		loadConfigFunc: hs.LoadConfig,
		saveConfigFunc: hs.SaveConfig,
		exitFunc:       hs.Exit,
	})

	ifaceReg.SetIntrospection(&IntrospectData)

	return nil
}

// LoadState replaces system state with that loaded from the state file.
func (hs *HostedService) LoadState() *dbus.Error {
	if err := hs.stGuard.LoadState(); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}

// SaveState saves system state to the state file.
func (hs *HostedService) SaveState() *dbus.Error {
	st := hs.stGuard.State()

	if err := hs.stGuard.StateFile().SaveState(&st); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}

// LoadConfig replaces service configuration with that loaded from the config files.
func (hs *HostedService) LoadConfig() *dbus.Error {
	if err := hs.cfgGuard.LoadConfig(); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}

// SaveConfig saves service configuration to the config files.
func (hs *HostedService) SaveConfig() *dbus.Error {
	cfg := hs.cfgGuard.Config()

	if err := hs.cfgGuard.ConfigFiles().SaveConfig(&cfg); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}

// Exit causes the service to exit immediately.
func (hs *HostedService) Exit() *dbus.Error {
	hs.exit <- struct{}{}
	return nil
}
