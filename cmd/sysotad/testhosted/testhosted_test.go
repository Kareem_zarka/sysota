// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package testhosted_test

import (
	"encoding/xml"
	"errors"
	"testing"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/cmd/sysotad"
	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/cmd/sysotad/testhosted"
	"gitlab.com/zygoon/sysota/ota"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
)

var errBoom = errors.New("boom")

func Test(t *testing.T) { TestingT(t) }

type hostedServiceSuite struct {
	dbustest.Suite

	sh dbusutil.ServiceHost
	hs testhosted.HostedService

	cfg   *ota.Config
	st    *ota.SystemState
	ioErr error

	cg        ota.ConfigGuard
	sg        ota.StateGuard
	exitCh    chan struct{}
	conn      *dbus.Conn
	svcObject dbus.BusObject
}

var _ = Suite(&hostedServiceSuite{})

func (s *hostedServiceSuite) LoadConfig() (*ota.Config, error) {
	if s.cfg == nil && s.ioErr == nil {
		panic("s.cfg and s.ioErr are both nil")
	}

	return s.cfg, s.ioErr
}

func (s *hostedServiceSuite) SaveConfig(cfg *ota.Config) error {
	if s.ioErr == nil {
		s.cfg = cfg
	}

	return s.ioErr
}

func (s *hostedServiceSuite) LoadState() (*ota.SystemState, error) {
	if s.st == nil && s.ioErr == nil {
		panic("s.st and s.ioErr are both nil")
	}

	return s.st, s.ioErr
}

func (s *hostedServiceSuite) SaveState(st *ota.SystemState) error {
	if s.ioErr == nil {
		s.st = st
	}

	return s.ioErr
}

func (s *hostedServiceSuite) SetUpSuite(c *C) {
	s.Suite.SetUpSuite(c)

	conn, err := dbusutil.SystemBus()
	c.Assert(err, IsNil)

	s.svcObject = conn.Object("org.oniroproject.sysota1", "/org/oniroproject/sysota1/Service")

	s.conn = conn
	s.exitCh = make(chan struct{}, 1)

	s.cfg = &ota.Config{}
	s.st = &ota.SystemState{}

	c.Assert(s.cg.BindAndLoad(s), IsNil)
	c.Assert(s.sg.BindAndLoad(s), IsNil)

	s.hs.Init(&s.cg, &s.sg, s.exitCh)
	s.sh.Init(conn)
	s.sh.AddHostedService(&s.hs)

	err = s.sh.Export()
	c.Assert(err, IsNil)

	err = sysotad.RequestBusName(conn)
	c.Assert(err, IsNil)
}

func (s *hostedServiceSuite) SetUpTest(c *C) {
	s.ioErr = nil

	err := s.sg.SetState(&ota.SystemState{})
	c.Assert(err, IsNil)

	err = s.cg.SetConfig(&ota.Config{})
	c.Assert(err, IsNil)

	s.cfg = nil
	s.st = nil
}

func (s *hostedServiceSuite) TearDownSuite(c *C) {
	if s.conn != nil {
		c.Assert(s.conn.Close(), IsNil)
	}

	err := s.sh.Unexport()
	c.Assert(err, IsNil)

	s.Suite.TearDownSuite(c)
}

func (s *hostedServiceSuite) TestSaveStateHappy(c *C) {
	err := s.sg.SetState(&ota.SystemState{BootMode: boot.Try})
	c.Assert(err, IsNil)

	// SetState automatically saves so reset the pointer.
	s.st = nil

	err = s.svcObject.Call("org.oniroproject.sysota1.Test.SaveState", 0).Store()
	c.Assert(err, IsNil)

	c.Check(s.st, DeepEquals, &ota.SystemState{BootMode: boot.Try})
}

func (s *hostedServiceSuite) TestSaveConfigHappy(c *C) {
	err := s.cg.SetConfig(&ota.Config{BootLoaderType: ota.PiBoot})
	c.Assert(err, IsNil)

	// SetConfig automatically saves so reset the pointer.
	s.cfg = nil

	err = s.svcObject.Call("org.oniroproject.sysota1.Test.SaveConfig", 0).Store()
	c.Assert(err, IsNil)

	c.Check(s.cfg, DeepEquals, &ota.Config{BootLoaderType: ota.PiBoot})
}

func (s *hostedServiceSuite) TestSaveStateError(c *C) {
	s.ioErr = errBoom

	err := s.svcObject.Call("org.oniroproject.sysota1.Test.SaveState", 0).Store()
	c.Assert(err, ErrorMatches, "boom")

	c.Check(s.st, IsNil)
}

func (s *hostedServiceSuite) TestSaveConfigError(c *C) {
	s.ioErr = errBoom

	err := s.svcObject.Call("org.oniroproject.sysota1.Test.SaveConfig", 0).Store()
	c.Assert(err, ErrorMatches, "boom")

	c.Check(s.cfg, IsNil)
}

func (s *hostedServiceSuite) TestLoadStateHappy(c *C) {
	s.st = &ota.SystemState{BootMode: boot.Try}

	err := s.svcObject.Call("org.oniroproject.sysota1.Test.LoadState", 0).Store()
	c.Assert(err, IsNil)

	c.Check(s.sg.State(), DeepEquals, ota.SystemState{BootMode: boot.Try})
}

func (s *hostedServiceSuite) TestLoadConfigHappy(c *C) {
	s.cfg = &ota.Config{BootLoaderType: ota.PiBoot}

	err := s.svcObject.Call("org.oniroproject.sysota1.Test.LoadConfig", 0).Store()
	c.Assert(err, IsNil)

	c.Check(s.cg.Config(), DeepEquals, ota.Config{BootLoaderType: ota.PiBoot})
}

func (s *hostedServiceSuite) TestLoadStateError(c *C) {
	s.ioErr = errBoom

	err := s.svcObject.Call("org.oniroproject.sysota1.Test.LoadState", 0).Store()
	c.Assert(err, ErrorMatches, "boom")
}

func (s *hostedServiceSuite) TestLoadConfigError(c *C) {
	s.ioErr = errBoom

	err := s.svcObject.Call("org.oniroproject.sysota1.Test.LoadConfig", 0).Store()
	c.Assert(err, ErrorMatches, "boom")
}

func (s *hostedServiceSuite) TestExit(c *C) {
	err := s.svcObject.Call("org.oniroproject.sysota1.Test.Exit", 0).Store()
	c.Assert(err, IsNil)

	// Calling the Exit method sends a message to the exit channel. In a real
	// service this is handled in a way that actually causes the service to
	// exit. Here we just observe the mechanism.
	_, ok := <-s.exitCh
	c.Check(ok, Equals, true)
}

func (s *hostedServiceSuite) TestIntrospection(c *C) {
	obj := s.conn.Object(svcspec.BusName, svcspec.ObjectPath)
	node, err := introspect.Call(obj)
	c.Assert(err, IsNil)
	c.Check(node, DeepEquals, &introspect.Node{
		XMLName: xml.Name{Local: "node"},
		Name:    "/org/oniroproject/sysota1/Service",
		Interfaces: []introspect.Interface{{
			Name: "org.oniroproject.sysota1.Test",
			Methods: []introspect.Method{{
				Name: "LoadState",
			}, {
				Name: "SaveState",
			}, {
				Name: "LoadConfig",
			}, {
				Name: "SaveConfig",
			}, {
				Name: "Exit",
				Annotations: []introspect.Annotation{{
					Name:  "org.freedesktop.DBus.Method.NoReply",
					Value: "true",
				}},
			}},
		}, {
			Name: "org.freedesktop.DBus.Introspectable",
			Methods: []introspect.Method{{
				Name: "Introspect",
				Args: []introspect.Arg{
					{Name: "out", Type: "s", Direction: "out"}},
			}},
		}},
	})
}
