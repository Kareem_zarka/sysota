// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package oper implements SystemOTA Operation object
package oper

import (
	"fmt"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
	"github.com/godbus/dbus/v5/prop"

	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	opspec "gitlab.com/zygoon/sysota/cmd/sysotad/updatehosted/oper/spec"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
)

// Operation represents a background activity that takes a non-trivial amount of time.
type Operation struct {
	conn  *dbus.Conn
	path  dbus.ObjectPath
	props *dbusutil.InterfaceProperties
}

// NewOperation returns a new operation with the given ID.
func NewOperation(conn *dbus.Conn, opID int) (*Operation, error) {
	sh := dbusutil.NewServiceHost(conn)
	op := &Operation{
		conn: conn,
		path: dbus.ObjectPath(fmt.Sprintf("%s/Operation/%d", svcspec.ObjectPath, opID)),
	}

	sh.AddHostedService(op)

	if err := sh.Export(); err != nil {
		return nil, err
	}

	return op, nil
}

// SetProgress sets the progress to a given value.
//
// The value is automatically clamped to the range 0 .. 100.0.
func (op *Operation) SetProgress(p opspec.Progress) {
	op.props.SetMust(opspec.ProgressProperty, p.Clamp())
}

// Go a function representing the action of the operation.
//
// Go arranges for the function to execute while updating the status of the
// Operation object. Go returns immediately after starting the go-routine.
//
// Initially the Kind property is set to the provided value. Inside a new
// go-routine the State property is set to Running. Then the provided function
// is called. The function should update the progress value periodically, by
// calling SetProgress, for proper user experience. When the provided function
// returns the operation state is set to Done. If the function returned an error
// the Error property is set to the error message.
func (op *Operation) Go(kind opspec.Kind, fn func() error) {
	op.props.SetMust(opspec.KindProperty, kind)

	go func() {
		op.props.SetMust(opspec.StateProperty, opspec.Running)

		if err := fn(); err != nil {
			op.props.SetMust(opspec.ErrorProperty, err.Error())
		}

		op.props.SetMust(opspec.StateProperty, opspec.Done)
	}()
}

// JoinServiceHost implements the HostedService interface.
func (op *Operation) JoinServiceHost(reg dbusutil.ServiceRegistration) error {
	ifaceReg := reg.Object(op.path).Interface(opspec.Interface)

	op.props = ifaceReg.SetProperties(map[string]*prop.Prop{
		opspec.KindProperty: {
			Value: opspec.UnknownOperation,
			Emit:  prop.EmitConst,
		},
		opspec.StateProperty: {
			Value: opspec.Pending,
			Emit:  prop.EmitTrue,
		},
		opspec.ProgressProperty: {
			Value: 0.0,
			Emit:  prop.EmitTrue,
		},
		opspec.ErrorProperty: {
			Value: "",
			Emit:  prop.EmitTrue,
		},
	})

	ifaceReg.SetIntrospection(&introspect.Interface{
		Name: opspec.Interface,
	})

	return nil
}

// ObjectPath returns the path of the D-Bus object.
func (op *Operation) ObjectPath() dbus.ObjectPath {
	return op.path
}

// GetAll returns the values of all the properties of a given D-Bus interface.
func (op *Operation) GetAll(iface string) (map[string]dbus.Variant, *dbus.Error) {
	if iface == opspec.Interface {
		return op.props.GetAll()
	}

	return nil, &dbus.ErrMsgUnknownInterface
}
