// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotad

import (
	"gitlab.com/zygoon/sysota/ota"
)

// ConfigFiles provides a way for the Service to load and save configuration.
//
// Configuration is read from a number of files but only stored to one.
// No provisions exist for saving differential or partial configuration.
type ConfigFiles struct {
	ReadFiles []string
	WriteFile string
}

// LoadConfig loads service configuration.
func (cf *ConfigFiles) LoadConfig() (*ota.Config, error) {
	return ota.LoadConfig(cf.ReadFiles)
}

// SaveConfig saves service configuration.
func (cf *ConfigFiles) SaveConfig(cfg *ota.Config) error {
	return ota.SaveConfig(cfg, cf.WriteFile)
}
