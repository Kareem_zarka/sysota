// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package rauccustomboothandler implements the rauc-custom-boot-handler program.
//
// The program is compiled to sysota-mux executable. The entry point of the
// program is the Cmd.Run method. It exits as RAUC requires a specific handler
// program to use unsupported bootloaders.
//
// The program is a thin D-Bus bridge to the sysotad service, where a compatible
// interface is exposed by the RAUC boot adapter hosted service. The logic of
// adapting RAUC interfaces to SysOTA boot.Protocol interface is in the package
// ota/raucadapter.
package rauccustomboothandler

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/errutil"
	"gitlab.com/zygoon/sysota/rauc/boothandler"
	"gitlab.com/zygoon/sysota/rauc/boothandler/bootdbus"
)

// Name is the name of this command inside the sysota-mux executable.
const Name = "rauc-custom-boot-handler"

var (
	// errExpectedPrimarySlot records unexpected number of arguments to the "set-primary" command.
	errExpectedPrimarySlot = errors.New("expected name of the primary slot")
	// errExpectedSlotName records unexpected number of arguments to the "get-state" or "set-state" command.
	errExpectedSlotName = errors.New("expected slot name")
	// errExpectedSlotState records lack of slot state name argument in "set-state" command.
	errExpectedSlotState = errors.New("expected slot state")
	// errUnexpectedArgument records extraneous unexpected argument to any of the command.
	errUnexpectedArgument = errors.New("unexpected argument")
)

// Cmd implements the rauc-custom-boot-handler command line tool.
type Cmd struct{}

// OneLiner returns a help message when used inside Router.
func (Cmd) OneLiner() string {
	return "Act as a RAUC custom boot handler"
}

// Run connects to the system bus and invokes the command over D-Bus.
func (Cmd) Run(ctx context.Context, args []string) error {
	conn, err := dbusutil.SystemBus()
	if err != nil {
		return err
	}

	defer func() {
		errutil.Forward(&err, conn.Close())
	}()

	cmd := interfaceCmd{Interface: bootdbus.NewClient(conn)}

	return cmd.Run(ctx, args)
}

// InterfaceCmd implements custom boot handler user interface.
//
// The command is separate from Cmd to simplify testing. Cmd uses interfaceCmd
// with a D-Bus bootdbus.Client as the implementation of
// boothandler.Interface.
type interfaceCmd struct {
	boothandler.Interface
}

// Run routes execution to the appropriate command.
func (cmd interfaceCmd) Run(ctx context.Context, args []string) error {
	// router implementing boot handler command line behavior.
	r := router.Router{
		Name: "rauc-custom-boot-handler",
		Commands: map[string]cmdr.Cmd{
			"get-primary": getPrimarySlotCmd(cmd),
			"set-primary": setPrimarySlotCmd(cmd),
			"get-state":   getSlotStateCmd(cmd),
			"set-state":   setSlotStateCmd(cmd),
		},
	}

	return r.Run(ctx, args)
}

// getPrimarySlotCmd is the sub-command invoked by RAUC to get the name of the primary slot.
type getPrimarySlotCmd struct {
	boothandler.Interface
}

// OneLiner returns a help message when used inside Router.git
func (cmd getPrimarySlotCmd) OneLiner() string {
	return "get the name of the primary slot"
}

func (cmd getPrimarySlotCmd) Run(ctx context.Context, args []string) error {
	_, stdout, _ := cmdr.Stdio(ctx)

	switch len(args) {
	case 0:
		slot, err := cmd.PrimarySlot()
		if err != nil {
			return err
		}

		_, err = fmt.Fprintln(stdout, slot)

		return err
	default:
		return errUnexpectedArgument
	}
}

// setPrimarySlotCmd is the sub-command invoked by RAUC to set the name of the primary slot.
type setPrimarySlotCmd struct {
	boothandler.Interface
}

// OneLiner returns a help message when used inside Router.
func (cmd setPrimarySlotCmd) OneLiner() string {
	return "set the name of the primary slot"
}

func (cmd setPrimarySlotCmd) Run(_ context.Context, args []string) error {
	var slot boot.Slot

	switch len(args) {
	case 0:
		return errExpectedPrimarySlot
	case 1:
		if err := slot.UnmarshalText([]byte(args[0])); err != nil {
			return err
		}

		return cmd.SetPrimarySlot(slot)
	default:
		return errUnexpectedArgument
	}
}

// getSlotStateCmd is the sub-command invoked by RAUC to get the state of a given slot.
type getSlotStateCmd struct {
	boothandler.Interface
}

// OneLiner returns a help message when used inside Router.
func (cmd getSlotStateCmd) OneLiner() string {
	return "get the state of a given slot"
}

func (cmd getSlotStateCmd) Run(ctx context.Context, args []string) error {
	var slot boot.Slot

	_, stdout, _ := cmdr.Stdio(ctx)

	switch len(args) {
	case 0:
		return errExpectedSlotName
	case 1:
		if err := slot.UnmarshalText([]byte(args[0])); err != nil {
			return err
		}

		state, err := cmd.SlotState(slot)
		if err != nil {
			return err
		}

		_, err = fmt.Fprintln(stdout, state)

		return err
	default:
		return errUnexpectedArgument
	}
}

// setSlotStateCmd is the sub-command invoked by RAUC to set the state of a given slot.
type setSlotStateCmd struct {
	boothandler.Interface
}

// OneLiner returns a help message when used inside Router.
func (cmd setSlotStateCmd) OneLiner() string {
	return "set the state of a given slot"
}

func (cmd setSlotStateCmd) Run(_ context.Context, args []string) error {
	var slot boot.Slot

	var state boot.SlotState

	switch len(args) {
	case 0:
		return errExpectedSlotName
	case 1:
		return errExpectedSlotState
	case 2:
		if err := slot.UnmarshalText([]byte(args[0])); err != nil {
			return err
		}

		if err := state.UnmarshalText([]byte(args[1])); err != nil {
			return err
		}

		return cmd.SetSlotState(slot, state)
	default:
		return errUnexpectedArgument
	}
}
