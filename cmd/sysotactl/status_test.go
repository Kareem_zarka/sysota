// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl_test

import (
	"context"
	"flag"

	"github.com/godbus/dbus/v5"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/testutil"
)

type statusSuite struct {
	cmdSuite
}

var _ = Suite(&statusSuite{})

func (s *statusSuite) TestImplicitStatusCommand(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{})

	c.Assert(err, IsNil)
	c.Check(stdout.String(), Equals, ""+
		"** Device **\n"+
		"\n"+
		"  Maker: Mr Potato\n"+
		"  Model: Potato OS DevKit 9000\n"+
		"\n"+
		"** Update Server **\n"+
		"\n"+
		"  Server URL: https://example.org/\n"+
		"     Package: example-package\n"+
		"      Stream: latest/stable\n")
	c.Check(stderr.String(), Equals, "")
}

func (s *statusSuite) TestHelp(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"status", "--help"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"Usage: sysotactl status\n"+
		"\n"+
		"The status command displays configuration of the system.\n")
}

func (s *statusSuite) TestExcessiveArgument(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"status", "potato"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "Usage: sysotactl status\n")
}

func (s *statusSuite) TestSuccessFactory(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"status"})

	c.Assert(err, IsNil)
	c.Check(stdout.String(), Equals, ""+
		"** Device **\n"+
		"\n"+
		"  Maker: Mr Potato\n"+
		"  Model: Potato OS DevKit 9000\n"+
		"\n"+
		"** Update Server **\n"+
		"\n"+
		"  Server URL: https://example.org/\n"+
		"     Package: example-package\n"+
		"      Stream: latest/stable\n")
	c.Check(stderr.String(), Equals, "")
}

func (s *statusSuite) TestSuccessInstalled(c *C) {
	s.props.SetMust("InstalledPackageName", dbus.MakeVariant("potato-os"))
	s.props.SetMust("InstalledPackageVersion", dbus.MakeVariant("1.0"))
	s.props.SetMust("InstalledSourceRevision", dbus.MakeVariant("git-hash"))

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"status"})

	c.Assert(err, IsNil)
	c.Check(stdout.String(), Equals, ""+
		"** Device **\n"+
		"\n"+
		"  Maker: Mr Potato\n"+
		"  Model: Potato OS DevKit 9000\n"+
		"\n"+
		"** Installed System Image **\n"+
		"\n"+
		"   Package: potato-os\n"+
		"   Version: 1.0\n"+
		"  Revision: git-hash\n"+
		"\n"+
		"** Update Server **\n"+
		"\n"+
		"  Server URL: https://example.org/\n"+
		"     Package: example-package\n"+
		"      Stream: latest/stable\n")
	c.Check(stderr.String(), Equals, "")
}
