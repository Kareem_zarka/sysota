// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/sysota/pkg/errutil"
)

// UpdateCmd is the "sysotactl update" sub-command.
type UpdateCmd struct{}

// OneLiner returns a help message when used inside Router.
func (UpdateCmd) OneLiner() string {
	return "Check and update if possible"
}

func (UpdateCmd) parseArgs(stderr io.Writer, args []string) error {
	fs := flag.NewFlagSet(Name+" update", flag.ContinueOnError)
	fs.SetOutput(stderr)

	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), "Usage: %s update\n", Name)
	}

	if err := fs.Parse(args); err != nil {
		if errors.Is(err, flag.ErrHelp) {
			w := fs.Output()

			_, _ = fmt.Fprintf(w, "\n")
			_, _ = fmt.Fprintf(w, "The update command checks and applies update to the system image.\n")
			_, _ = fmt.Fprintf(w, "The device is rebooted automatically during the update process.\n")
		}

		return err
	}

	if fs.NArg() != 0 {
		fs.Usage()

		return flag.ErrHelp
	}

	return nil
}

// Run calls Update and waits for the operation to complete.
func (cmd UpdateCmd) Run(ctx context.Context, args []string) error {
	_, stdout, stderr := cmdr.Stdio(ctx)

	if err := cmd.parseArgs(stderr, args); err != nil {
		return err
	}

	client, err := NewClient()
	if err != nil {
		return err
	}

	defer func() {
		errutil.Forward(&err, client.Close())
	}()

	op, err := client.Update(ctx)
	if err != nil {
		return fmt.Errorf("cannot initialize system update: %w", err)
	}

	for range op.Changed() {
		_, _ = fmt.Fprintf(stdout, "kind:%s, state:%s, progress:%s\n", op.Kind(), op.State(), op.Progress())
	}

	<-op.Done()

	if err := ctx.Err(); err != nil {
		return fmt.Errorf("cannot complete system update: %w", err)
	}

	return op.Err()
}
