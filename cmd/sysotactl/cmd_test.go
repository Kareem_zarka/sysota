// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl_test

import (
	"context"
	"errors"
	"flag"
	"sync"
	"testing"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/prop"
	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/netota"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/cmd/sysotactl"
	"gitlab.com/zygoon/sysota/cmd/sysotad"
	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
	"gitlab.com/zygoon/sysota/pkg/testutil"
)

func Test(t *testing.T) { TestingT(t) }

type cmdSuite struct {
	dbustest.Suite

	m sync.RWMutex

	conn        *dbus.Conn
	cmd         cmdr.Cmd
	props       *dbusutil.InterfaceProperties
	changeFunc  func(ch *prop.Change) *dbus.Error
	streamsFunc func() (map[string]netota.Stream, *dbus.Error)
	updateFunc  func(map[string]dbus.Variant) (dbus.ObjectPath, *dbus.Error)
}

func (s *cmdSuite) MockChangeFunc(fn func(ch *prop.Change) *dbus.Error) {
	s.m.Lock()
	defer s.m.Unlock()

	s.changeFunc = fn
}

func (s *cmdSuite) MockStreamsFunc(fn func() (map[string]netota.Stream, *dbus.Error)) {
	s.m.Lock()
	defer s.m.Unlock()

	s.streamsFunc = fn
}

func (s *cmdSuite) MockUpdateFn(fn func(map[string]dbus.Variant) (dbus.ObjectPath, *dbus.Error)) {
	s.m.Lock()
	defer s.m.Unlock()

	s.updateFunc = fn
}

func (s *cmdSuite) SetUpSuite(c *C) {
	s.m.Lock()
	defer s.m.Unlock()

	s.Suite.SetUpSuite(c)

	conn, err := dbusutil.SystemBus(dbus.WithSignalHandler(dbus.NewSequentialSignalHandler()))
	c.Assert(err, IsNil)

	s.conn = conn

	sh := dbusutil.NewServiceHost(conn)
	sh.AddHostedService(s)

	err = sh.Export()
	c.Assert(err, IsNil)

	err = sysotad.RequestBusName(conn)
	c.Assert(err, IsNil)

	s.cmd = sysotactl.Cmd{}
}

func (s *cmdSuite) TearDownSuite(c *C) {
	s.m.Lock()
	defer s.m.Unlock()

	if s.conn != nil {
		err := s.conn.Close()
		c.Assert(err, IsNil)

		s.conn = nil
	}

	s.Suite.TearDownSuite(c)
}

func (s *cmdSuite) SetUpTest(_ *C) {
	s.m.Lock()
	defer s.m.Unlock()

	s.props.SetMust("ServerURL", "https://example.org/")
	s.props.SetMust("SystemImagePackage", "example-package")
	s.props.SetMust("Stream", "latest/stable")

	s.props.SetMust("InstalledPackageName", "")
	s.props.SetMust("InstalledPackageVersion", "")
	s.props.SetMust("InstalledSourceRevision", "")

	s.changeFunc = nil
	s.streamsFunc = nil
	s.updateFunc = nil
}

func (s *cmdSuite) propertyChange(ch *prop.Change) *dbus.Error {
	s.m.RLock()
	defer s.m.RUnlock()

	if s.changeFunc != nil {
		return s.changeFunc(ch)
	}

	return nil
}

func (s *cmdSuite) streams() (map[string]netota.Stream, *dbus.Error) {
	s.m.RLock()
	defer s.m.RUnlock()

	if s.streamsFunc != nil {
		return s.streamsFunc()
	}

	return nil, nil
}

func (s *cmdSuite) update(hints map[string]dbus.Variant) (dbus.ObjectPath, *dbus.Error) {
	s.m.RLock()
	defer s.m.RUnlock()

	if s.updateFunc != nil {
		return s.updateFunc(hints)
	}

	return "", dbus.MakeFailedError(errors.New("system is up-to-date"))
}

func (s *cmdSuite) JoinServiceHost(reg dbusutil.ServiceRegistration) error {
	ifaceReg := reg.Object(svcspec.ObjectPath).Interface("org.oniroproject.sysota1.Service")

	ifaceReg.SetMethods(map[string]interface{}{
		"GetStreams": s.streams,
		"Update":     s.update,
	})

	s.props = ifaceReg.SetProperties(map[string]*prop.Prop{
		"Maker": {
			Value: "Mr Potato",
		},
		"Model": {
			Value: "Potato OS DevKit 9000",
		},
		"ServerURL": {
			Value:    "",
			Writable: true,
			Emit:     prop.EmitTrue,
			Callback: s.propertyChange,
		},
		"SystemImagePackage": {
			Value:    "",
			Callback: s.propertyChange,
			Writable: true,
		},
		"Stream": {
			Value:    "",
			Writable: true,
			Emit:     prop.EmitTrue,
			Callback: s.propertyChange,
		},
		"InstalledPackageName": {
			Value: "",
			Emit:  prop.EmitTrue,
		},
		"InstalledPackageVersion": {
			Value: "",
			Emit:  prop.EmitTrue,
		},
		"InstalledSourceRevision": {
			Value: "",
			Emit:  prop.EmitTrue,
		},
	})

	return nil
}

type sysotactlSuite struct {
	cmdSuite
}

var _ = Suite(&sysotactlSuite{})

func (s *sysotactlSuite) TestHelp(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"--help"})

	c.Assert(err, Equals, flag.ErrHelp)

	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"Usage: sysotactl [COMMAND] [...]\n"+
		"\n"+
		"Available commands:\n"+
		"  set-package  Set the name of the system image package\n"+
		"  set-server   Set the URL of the update server\n"+
		"  set-stream   Set the name of the followed stream\n"+
		"  status       Display current state and configuration\n"+
		"  streams      Display available update streams\n"+
		"  update       Check and update if possible\n")
}

func (s *sysotactlSuite) TestUnknownOption(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"-potato"})

	c.Assert(err, ErrorMatches, "flag provided but not defined: -potato")
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"flag provided but not defined: -potato\n"+
		"Usage: sysotactl [COMMAND] [...]\n"+
		"\n"+
		"Available commands:\n"+
		"  set-package  Set the name of the system image package\n"+
		"  set-server   Set the URL of the update server\n"+
		"  set-stream   Set the name of the followed stream\n"+
		"  status       Display current state and configuration\n"+
		"  streams      Display available update streams\n"+
		"  update       Check and update if possible\n")
}
