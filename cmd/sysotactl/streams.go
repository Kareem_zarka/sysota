// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"

	"gitlab.com/zygoon/go-cmdr"
	"gopkg.in/yaml.v3"

	"gitlab.com/zygoon/sysota/pkg/errutil"
)

// StreamsCmd is the "sysotactl streams" sub-command.
type StreamsCmd struct{}

// OneLiner returns a help message when used inside Router.
func (StreamsCmd) OneLiner() string {
	return "Display available update streams"
}

func (StreamsCmd) parseArgs(stderr io.Writer, args []string) error {
	fs := flag.NewFlagSet(Name+" streams", flag.ContinueOnError)
	fs.SetOutput(stderr)

	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), "Usage: %s streams\n", Name)
	}

	if err := fs.Parse(args); err != nil {
		if errors.Is(err, flag.ErrHelp) {
			w := fs.Output()

			_, _ = fmt.Fprintf(w, "\n")
			_, _ = fmt.Fprintf(w, "The streams command displays available update streams\n")
		}

		return err
	}

	if fs.NArg() != 0 {
		fs.Usage()

		return flag.ErrHelp
	}

	return nil
}

// Run calls Streams and renders the response as YAML.
func (cmd StreamsCmd) Run(ctx context.Context, args []string) error {
	_, stdout, stderr := cmdr.Stdio(ctx)

	if err := cmd.parseArgs(stderr, args); err != nil {
		return err
	}

	client, err := NewClient()
	if err != nil {
		return err
	}

	defer func() {
		errutil.Forward(&err, client.Close())
	}()

	streams, err := client.Streams(ctx)
	if err != nil {
		return err
	}

	return yaml.NewEncoder(stdout).Encode(streams)
}
