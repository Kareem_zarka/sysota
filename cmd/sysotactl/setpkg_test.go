// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl_test

import (
	"context"
	"errors"
	"flag"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/prop"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/testutil"
)

type setPkgSuite struct {
	cmdSuite
}

var _ = Suite(&setPkgSuite{})

func (s *setPkgSuite) TestHelp(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-package", "--help"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"Usage: sysotactl set-package NAME\n"+
		"\n"+
		"The set-package command changes the package used for OS images.\n"+
		"\n"+
		"Updates are only installed if the machine is compatible.\n")
}

func (s *setPkgSuite) TestMissingArgument(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-package"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "Usage: sysotactl set-package NAME\n")
}

func (s *setPkgSuite) TestSuccessfulChange(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-package", "new-package"})

	c.Assert(err, IsNil)

	c.Check(stdout.String(), Equals, "Setting system image package name to \"new-package\"\n")
	c.Check(stderr.String(), Equals, "")

	c.Check(s.props.GetMust("SystemImagePackage").(string), Equals, "new-package")
}

func (s *setPkgSuite) TestServerSideValidationFailure(c *C) {
	s.MockChangeFunc(func(ch *prop.Change) *dbus.Error {
		return dbus.MakeFailedError(errors.New("boom"))
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-package", "new-package"})

	c.Assert(err, ErrorMatches, "cannot set system image package name to new-package: boom")

	c.Check(stdout.String(), Equals, "Setting system image package name to \"new-package\"\n")
	c.Check(stderr.String(), Equals, "")

	c.Check(s.props.GetMust("SystemImagePackage").(string), Equals, "example-package")
}
