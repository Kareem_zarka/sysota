// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl_test

import (
	"context"
	"errors"
	"flag"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/prop"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/testutil"
)

type setStreamSuite struct {
	cmdSuite
}

var _ = Suite(&setStreamSuite{})

func (s *setStreamSuite) TestHelp(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-stream", "--help"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"Usage: sysotactl set-stream NAME\n"+
		"\n"+
		"The set-stream command changes the stream used for OS images.\n")
}

func (s *setStreamSuite) TestMissingArgument(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-stream"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "Usage: sysotactl set-stream NAME\n")
}

func (s *setStreamSuite) TestSuccessfulChange(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-stream", "latest/nightly"})

	c.Assert(err, IsNil)
	c.Check(stdout.String(), Equals, "Setting update stream to \"latest/nightly\"\n")
	c.Check(stderr.String(), Equals, "")

	c.Check(s.props.GetMust("Stream").(string), Equals, "latest/nightly")
}

func (s *setStreamSuite) TestServerSideValidationFailure(c *C) {
	s.MockChangeFunc(func(ch *prop.Change) *dbus.Error {
		return dbus.MakeFailedError(errors.New("boom"))
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-stream", "latest/nightly"})

	c.Assert(err, ErrorMatches, "cannot set update stream to latest/nightly: boom")
	c.Check(stdout.String(), Equals, "Setting update stream to \"latest/nightly\"\n")
	c.Check(stderr.String(), Equals, "")

	c.Check(s.props.GetMust("Stream").(string), Equals, "latest/stable")
}
