// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/sysota/pkg/errutil"
)

// SetStreamCmd is the "sysotactl set-stream" sub-command.
type SetStreamCmd struct{}

// OneLiner returns a help message when used inside Router.
func (SetStreamCmd) OneLiner() string {
	return "Set the name of the followed stream"
}

func (SetStreamCmd) parseArgs(stderr io.Writer, args []string) (stream string, err error) {
	fs := flag.NewFlagSet(Name+" set-stream", flag.ContinueOnError)
	fs.SetOutput(stderr)

	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), "Usage: %s set-stream NAME\n", Name)
	}

	if err := fs.Parse(args); err != nil {
		if errors.Is(err, flag.ErrHelp) {
			w := fs.Output()

			_, _ = fmt.Fprintf(w, "\n")
			_, _ = fmt.Fprintf(w, "The set-stream command changes the stream used for OS images.\n")
		}

		return "", err
	}

	if fs.NArg() != 1 {
		fs.Usage()

		return "", flag.ErrHelp
	}

	// All validation is done on the service side.
	return fs.Arg(0), nil
}

// Run calls properties.Set with the appropriate property name.
func (cmd SetStreamCmd) Run(ctx context.Context, args []string) error {
	_, stdout, stderr := cmdr.Stdio(ctx)

	stream, err := cmd.parseArgs(stderr, args)
	if err != nil {
		return err
	}

	client, err := NewClient()
	if err != nil {
		return err
	}

	defer func() {
		errutil.Forward(&err, client.Close())
	}()

	_, _ = fmt.Fprintf(stdout, "Setting update stream to %q\n", stream)

	if err := client.SetStream(ctx, stream); err != nil {
		return fmt.Errorf("cannot set update stream to %s: %w", stream, err)
	}

	return nil
}
