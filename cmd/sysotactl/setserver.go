// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"net/url"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/sysota/pkg/errutil"
)

// SetServerCmd is the "sysotactl set-server" sub-command.
type SetServerCmd struct{}

// OneLiner returns a help message when used inside Router.
func (SetServerCmd) OneLiner() string {
	return "Set the URL of the update server"
}

func (SetServerCmd) parseArgs(stderr io.Writer, args []string) (u string, err error) {
	fs := flag.NewFlagSet(Name+" set-server\n", flag.ContinueOnError)
	fs.SetOutput(stderr)

	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), "Usage: %s set-server URL\n", Name)
	}

	if err := fs.Parse(args); err != nil {
		if errors.Is(err, flag.ErrHelp) {
			w := fs.Output()

			_, _ = fmt.Fprintf(w, "\n")
			_, _ = fmt.Fprintf(w, "The set-server command changes the URL of the NetOTA server to use.\n")
		}

		return "", err
	}

	if fs.NArg() != 1 {
		fs.Usage()

		return "", flag.ErrHelp
	}

	parsedURL, err := url.Parse(fs.Arg(0))
	if err != nil {
		return "", fmt.Errorf("cannot parse server URL: %w", err)
	}

	return parsedURL.String(), nil
}

// Run calls properties.Set with the appropriate property name.
func (cmd SetServerCmd) Run(ctx context.Context, args []string) error {
	_, stdout, stderr := cmdr.Stdio(ctx)

	u, err := cmd.parseArgs(stderr, args)
	if err != nil {
		return err
	}

	client, err := NewClient()
	if err != nil {
		return err
	}

	defer func() {
		errutil.Forward(&err, client.Close())
	}()

	_, _ = fmt.Fprintf(stdout, "Setting update server to %q\n", u)

	if err := client.SetServerURL(ctx, u); err != nil {
		return fmt.Errorf("cannot set update server URL to %s: %w", u, err)
	}

	return nil
}
