// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl

import (
	"context"
	"errors"

	"github.com/godbus/dbus/v5"
	"gitlab.com/zygoon/netota"

	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	opspec "gitlab.com/zygoon/sysota/cmd/sysotad/updatehosted/oper/spec"

	"gitlab.com/zygoon/sysota/cmd/sysotad/updatehosted"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
)

// Client allows interacting with sysotad over D-Bus.
type Client struct {
	conn *dbus.Conn
	svc  dbus.BusObject
}

// NewClient returns a new client for interacting with sysotad over D-Bus.
func NewClient() (*Client, error) {
	conn, err := dbusutil.SystemBus(dbus.WithSignalHandler(dbus.NewSequentialSignalHandler()))
	if err != nil {
		return nil, err
	}

	c := &Client{
		conn: conn,
		svc:  conn.Object(svcspec.BusName, svcspec.ObjectPath),
	}

	return c, nil
}

// Close closes the connection to the D-Bus system bus.
func (c *Client) Close() error {
	return c.conn.Close()
}

// SetSystemPackage sets the package name to the given value.
func (c *Client) SetSystemPackage(ctx context.Context, name string) error {
	return c.svc.CallWithContext(ctx,
		dbusutil.PropertiesInterface+"."+dbusutil.PropertySetMethod, 0,
		updatehosted.InterfaceName, svcspec.SystemImagePackageProperty, dbus.MakeVariant(name)).Store()
}

// SetServerURL sets the server URL to the given value.
func (c *Client) SetServerURL(ctx context.Context, url string) error {
	return c.svc.CallWithContext(ctx,
		dbusutil.PropertiesInterface+"."+dbusutil.PropertySetMethod, 0,
		updatehosted.InterfaceName, svcspec.ServerURLProperty, dbus.MakeVariant(url)).Store()
}

// SetStream sets the stream name to the given value.
func (c *Client) SetStream(ctx context.Context, stream string) error {
	return c.svc.CallWithContext(ctx,
		dbusutil.PropertiesInterface+"."+dbusutil.PropertySetMethod, 0,
		updatehosted.InterfaceName, svcspec.StreamProperty, dbus.MakeVariant(stream)).Store()
}

// Properties contain all the properties of the update server.
type Properties struct {
	Maker string
	Model string

	ServerURL          string
	SystemImagePackage string
	Stream             string

	InstalledPackageName    string
	InstalledPackageVersion string
	InstalledSourceRevision string
}

// Properties retrieves all server properties.
func (c *Client) Properties(ctx context.Context) (*Properties, error) {
	var props map[string]dbus.Variant

	call := c.svc.CallWithContext(ctx,
		dbusutil.PropertiesInterface+"."+dbusutil.PropertyGetAllMethod, 0,
		updatehosted.InterfaceName)
	if err := call.Store(&props); err != nil {
		return nil, err
	}

	p := &Properties{}

	if variant, ok := props[svcspec.MakerProperty]; ok {
		if value, ok := variant.Value().(string); ok {
			p.Maker = value
		}
	}

	if variant, ok := props[svcspec.ModelProperty]; ok {
		if value, ok := variant.Value().(string); ok {
			p.Model = value
		}
	}

	if variant, ok := props[svcspec.ServerURLProperty]; ok {
		if value, ok := variant.Value().(string); ok {
			p.ServerURL = value
		}
	}

	if variant, ok := props[svcspec.SystemImagePackageProperty]; ok {
		if value, ok := variant.Value().(string); ok {
			p.SystemImagePackage = value
		}
	}

	if variant, ok := props[svcspec.StreamProperty]; ok {
		if value, ok := variant.Value().(string); ok {
			p.Stream = value
		}
	}

	if variant, ok := props[svcspec.InstalledPackageNameProperty]; ok {
		if value, ok := variant.Value().(string); ok {
			p.InstalledPackageName = value
		}
	}

	if variant, ok := props[svcspec.InstalledPackageVersionProperty]; ok {
		if value, ok := variant.Value().(string); ok {
			p.InstalledPackageVersion = value
		}
	}

	if variant, ok := props[svcspec.InstalledSourceRevisionProperty]; ok {
		if value, ok := variant.Value().(string); ok {
			p.InstalledSourceRevision = value
		}
	}

	return p, nil
}

// Streams retrieves the map of available streams.
func (c *Client) Streams(ctx context.Context) (streams map[string]netota.Stream, err error) {
	call := c.svc.CallWithContext(ctx, updatehosted.InterfaceName+"."+svcspec.GetStreamsMethod, 0)
	if err := call.Store(&streams); err != nil {
		return nil, err
	}

	return streams, nil
}

// Update starts an asynchronous system update.
func (c *Client) Update(ctx context.Context) (*Operation, error) {
	var path dbus.ObjectPath

	// Request system update operation. If successful we get an operation object
	// path. If unsuccessful we get an error immediately.
	call := c.svc.CallWithContext(ctx, updatehosted.InterfaceName+"."+svcspec.UpdateMethod, 0, map[string]dbus.Variant{})
	if err := call.Store(&path); err != nil {
		return nil, err
	}

	return newOperation(ctx, c.conn, path)
}

// Operation represents an operation which executes in the service process.
//
// Operations are D-Bus objects used to represent long-running activity, such as
// downloading, verifying, unpacking, writing system disk images. Operations use
// properties and signals to communicate change to any participating observers.
//
// At the moment operations cannot be cancelled or influenced in any way.
type Operation struct {
	w       *dbusutil.WatchedObject
	done    chan struct{}
	changed chan struct{}
}

func newOperation(ctx context.Context, conn *dbus.Conn, path dbus.ObjectPath) (*Operation, error) {
	ctx, cancel := context.WithCancel(ctx)

	w, err := dbusutil.NewWatchedObject(ctx, conn, svcspec.BusName, path, opspec.Interface)
	if err != nil {
		cancel()

		return nil, err
	}

	op := &Operation{
		w:       w,
		done:    make(chan struct{}),
		changed: make(chan struct{}, 1),
	}

	// Notify about the initial state of the object.
	op.maybeNotify()

	go op.monitor(ctx, func() {
		// Cancel the monitoring context. This stops signal processing and begins
		// the shutdown of the monitoring process.
		cancel()

		// Wait for the monitoring shutdown to complete.
		//
		// This may take additional time as it requires further IPC to remove
		// signal notification registration. This can be interrupted by
		// cancelling the connection context.
		<-w.Done()

		// Notify about final state with a blocking send. This guarantees that
		// the receiver sees the final state of the operation, even if the
		// receiver is currently busy processing previous state.
		op.changed <- struct{}{}

		close(op.changed)
		close(op.done)
	})

	return op, nil
}

func (op *Operation) monitor(ctx context.Context, shutdown func()) {
	defer shutdown()

	// Send notifications for as long as the change is not done. Coalesce change
	// events and change lost events since for us, they are equivalent.
	for op.State() != opspec.Done {
		select {
		case <-ctx.Done():
			return
		case <-op.w.Changes():
			op.maybeNotify()
		case <-op.w.ChangeLost():
			op.maybeNotify()
		}
	}
}

// maybeNotify strobes the changed channel if possible.
//
// The notification may be lost if there are no receivers.
// This cannot be used to make the final notification.
func (op *Operation) maybeNotify() {
	select {
	case op.changed <- struct{}{}:
	default:
		break
	}
}

// Changed returns a channel notifying that the state of operation has changed.
//
// Changed is guaranteed to return at lest one value, even if the operation
// reaches final state before any changes are observed and even if the context
// is cancelled.
func (op *Operation) Changed() <-chan struct{} {
	return op.changed
}

// Done returns a channel which is closed when no further changes can occur.
func (op *Operation) Done() <-chan struct{} {
	return op.done
}

// Wait waits for the operation to complete and returns error, if any.
func (op *Operation) Wait() error {
	<-op.done

	return op.Err()
}

// Kind returns the kind of operation.
func (op *Operation) Kind() opspec.Kind {
	variant := op.w.CachedProperty(opspec.Interface, opspec.KindProperty)

	if val, ok := variant.Value().(string); ok {
		return opspec.Kind(val).Clamp()
	}

	return opspec.UnknownOperation
}

// State returns the state of the operation.
func (op *Operation) State() opspec.State {
	variant := op.w.CachedProperty(opspec.Interface, opspec.StateProperty)

	if val, ok := variant.Value().(int32); ok {
		return opspec.State(val).Clamp()
	}

	return opspec.UnknownState
}

// Progress returns the progress percentage of the operation.
func (op *Operation) Progress() opspec.Progress {
	variant := op.w.CachedProperty(opspec.Interface, opspec.ProgressProperty)

	if val, ok := variant.Value().(float64); ok {
		return opspec.Progress(val).Clamp()
	}

	return 0
}

// Err returns the error of the operation, if any.
func (op *Operation) Err() error {
	variant := op.w.CachedProperty(opspec.Interface, opspec.ErrorProperty)

	if val, ok := variant.Value().(string); ok && val != "" {
		return errors.New(val)
	}

	return nil
}
