// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
package main_test

import (
	"testing"

	. "gopkg.in/check.v1"

	main "gitlab.com/zygoon/sysota/cmd/sysota-mux"
)

func Test(t *testing.T) { TestingT(t) }

type mainSuite struct{}

var _ = Suite(&mainSuite{})

func (s *mainSuite) TestMain(_ *C) {
	main.Main()
}
