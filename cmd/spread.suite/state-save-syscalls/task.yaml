# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: measure particular system calls used in ota.SaveState
details: |
  We care about the correctness of the data synchronization dance implemented in
  ota.SaveState. We can do that by running sysotad through strace and looking at
  the sequence of system calls used by sysotad on shutdown.
prepare: |
  # Stop the service ahead of state modifications.
  systemctl stop sysotad.service

  # Enable the load-save debug interface.
  mkdir -p /run/sysota
  cat <<SYSOTAD_CONF >/run/sysota/sysotad.conf
  [Debug]
  TestAPI = true
  SYSOTAD_CONF

  sed -e 's!ExecStart=.*!Environment=GOMAXPROCS=1\nExecStart=/usr/bin/strace --string-limit=128 --trace=%%file,%%desc,sync,fsync,fdatasync --follow-forks --output='"$(pwd)"'/sysotad.strace /usr/libexec/sysota/sysotad!' </usr/lib/systemd/system/sysotad.service >/etc/systemd/system/sysotad.service
  systemctl daemon-reload
  systemctl stop sysotad.service

  # Create a state file we will attempt to read and write to.
  mkdir -p /var/lib/sysota
  truncate --size=0 /var/lib/sysota/state.ini

execute: |
  busctl call org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Test LoadState
  busctl call org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Test SaveState
  systemctl stop sysotad.service

  # NOTE: MATCH is fancy way to call "grep -E" and show the input on failure.

  # We this is how we read the state.
  MATCH '^[0-9]+\s+openat\(AT_FDCWD, "/var/lib/sysota/state.ini", O_RDONLY\|O_CLOEXEC( <unfinished \.\.\.>|\) = 7)$' < sysotad.strace
  MATCH '^[0-9]+\s+read\(7, "", 4096\)\s+= 0$' <sysotad.strace

  # This is how we write the state.
  #
  # We we do the open+rename+fsync+fsync (dir) dance on shutdown.
  # Note that fsync may be interrupted so we try to match both:
  # 47447 fsync(3 <unfinished ...>
  # 47447 fsync(7)                          = 0
  MATCH '^[0-9]+\s+openat\(AT_FDCWD, "/var/lib/sysota/tmp-[0-9]+", O_RDWR|O_CREAT|O_EXCL\|O_CLOEXEC, 0600\) = 7$' <sysotad.strace
  MATCH '^[0-9]+\s+openat\(AT_FDCWD, "/var/lib/sysota", O_RDONLY\|O_CLOEXEC( <unfinished \.\.\.>|\) = 8)$' <sysotad.strace
  MATCH '^[0-9]+\s+fsync\(7( <unfinished \.\.\.>|\)\s+= 0)$' <sysotad.strace
  MATCH '^[0-9]+\s+renameat\(AT_FDCWD, "/var/lib/sysota/tmp-[0-9]+", AT_FDCWD, "/var/lib/sysota/state.ini"( <unfinished \.\.\.>|\) = 0)$' <sysotad.strace
  MATCH '^[0-9]+\s+fsync\(8( <unfinished \.\.\.>|\)\s+= 0)$' <sysotad.strace
restore: |
  rm -f sysotad.strace
  rm -f /var/lib/sysota/state.ini
  rmdir /var/lib/sysota
  rm -f /run/sysota/sysotad.conf

  rm -f /etc/systemd/system/sysotad.service
  systemctl daemon-reload
  systemctl restart sysotad.service
