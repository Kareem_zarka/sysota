<!--
SPDX-License-Identifier: CC-BY-4.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# Architecture

tl;dr: SystemOTA is a D-Bus service which can be deployed on a appliance-like
Linux system to perform image-based updates. The system must be configured to
use a particular partition table and file system configuration. Internally
System OTA wraps RAUC, which performs the heavy lifting to validate and write
the image to the right slot. SystemOTA sits both on top of and below RAUC. At a
level higher than RAUC, SystemOTA handles network communication, update policy
and provides an API to the rest of the platform. At a level below RAUC, where
appropriate, SystemOTA controls the configuration of the boot loader.

## Overview and Update Modes

SystemOTA is a bus-activated D-Bus service exposing high-level APIs to
configure and perform system updates. There are two primary modes of operation,
automatic and managed updates. Automatic mode requires no additional software
and relies on systemd timers to perform periodic, random checks and updates.
Managed mode disables the timer and relinquishes control to a _Device Agent_, a
third-party application provided by the device manufacturer to communicate with
cloud services for precise control over update process.

In both of those modes, the SystemOTA service talks to an update server to
discover updates and how to obtain them. Updates are expressed as complete
system images. The mode of delivery of a system image can be highly optimized
with delta encoding based on the image present on the device making the
request, but from an architectural point of view, a complete image is replaced.
The image is immutable and is identical across all the devices of a given kind,
as identified by the _Maker_ and _Model_ properties associated with the device.

## Device Partitioning

From the point of view of SystemOTA, non-volatile storage is partitioned and
contains distinct partitions interacting with the update process. They are
_Slot A_, _Slot B_, _System Data_ and _Boot Data_. One of the _Slot_ partitions
is marked as _active_ and selected by the boot loader as well as early
user-space for the root file system. The _System Data_ partition contains the
same _A_ and _B_ areas, represented as directories holding specific files which
are combined with the immutable image to form a practical operating
environment. The other slot is _inactive_ and is used as storage for the update
process.

Not mentioned here beyond this paragraph, is the _App Data_ partition which is
expected to hold both the _installable_ applications as well as any data
belonging to said applications. As the name suggests, SystemOTA focuses on the
operating system, leaving application space to be managed with other tools that
are a part of the system image.

The particular way _System Data_ and _App Data_ is used does not strongly
impact on the design of SystemOTA itself. At a later stage a reference
configuration with opinionated choices are expected as a part of the testing
process of SystemOTA itself.

## A/B Update, Commit or Rollback

On update the _inactive_ slot is modified to contain the new system image. The
current state directory present on the _System Data_ partition is copied to
form the new system data. The _Boot Data_ partition is modified to boot the new
configuration **once**, falling back to the original configuration on failure
or just any power-cycle. Under the hood, RAUC is used to implement specific
elements of the update operations. RAUC integration will be described
separately later.

Because the _A_/_B_ directory on _System Data_ partition is copied, the new
system can freely change system state, including the format used to represent
persistent data, while still allowing a safe rollback to the original
configuration.

Upon successful update the boot system re-configured again to make the change
persistent and the roles of both slots ares swapped. The system is ready to
accept another update.

Upon failed update the system reboots or is rebooted by a watchdog back to the
slot used previously. The particular image that failed to work correctly is
added to a local disallow-list, which prevents looping over a failing update.

In both cases disk space associated with the now-inactive slot is reclaimed.

The precise policy which decides if a given system is working fine is
configurable, allowing the update process to roll back if the system breaks
itself or critical system applications.

## SystemOTA Server Architecture

SystemOTA uses HTTPS requests to discover and perhaps obtain updates. The
specific update server or update URL is a part of the internal configuration of
each device. The system was designed so that, theoretically, a single update
server could serve updates to all the devices using SystemOTA, from all the
vendors combined. The opposite extreme is also supported, using distinct update
servers for each device line.

On a given device the _Maker_ and _Model_ properties, combined with the name of
the update _Stream_ constitute the input to the update server. The specific URL
of the server is a part of internal device configuration and is not discussed
below. The system is designed such, so that the update server can be a single
system, a set of round-robin systems or a set of public mirrors combined with a
selection process. In the end a device looking for updates performs requests to
a specific update server.

There are two primary GET requests. One is used to discover the set of
available update _Streams_ and another is to discover the _Images_ published in
a given named stream.

A given stream contains a specific _Revision_. The revision is a string and
does not possess total or partial ordering. The system knows the revision it is
currently using. After making the request to inspect what is published in a
given stream, the device can immediately know:

1) If it is up-to-date or if it has an update available
2) If it can combine the image it is running with a delta image to save on
   bandwidth
3) Specific disk, memory and tooling required to apply a delta
4) If the device should attempt to update already or not, based on progressive
   release factor
5) How to find and download individual images

This has several interesting properties that are discussed below.

First of all, the update process does not rely on a complex server-side
application, and can be hosted from any HTTP server. Second, the device is able
to determine all the decisions with just one request, which is also
cache-friendly, making it practical for large fleets to be managed by a modest
infrastructure. Ability co combine deltas and observe progressive roll-out
factor reduces total bandwidth and momentary bandwidth respectively, preventing
the device fleet from overloading the update infrastructure.

Second, the system design allows to remove a problematic update without
creating a new artificial version, simply by pointing the update stream to a
previously published set of images. In the case of a problematic update
affecting devices in the wild in a way the rollback system does not cover, a
specific device model from a given maker can be pointed back to the previous
image, making it much faster and easier to abort and rollback.

Given the simplicity of the API, it is possible that multiple implementations
of the update server will be available. It is expected that at least a set of
command-line tools sufficient for managing a small server will be created,
mainly to test perform end-to-end testing on the update process.

The format is flexible enough that improved file system and compression formats
can be introduced over time, without locking out devices that are booting for
the first time and start with old system software.

The _Maker_ property allows a manufacturer or brand owner to manage any number
of distinct device type on a larger shared update server. The _Model_ property
allows the device maker to deliver unique software needed by any specific
device directly inside the system image. Together _Maker_ and _Model_ constrain
the set of compatible _Images_, preventing devices from attempting to install
incompatible updates.

The _Stream_ property allows fleets of identical devices to use different
versions, representing different major releases, different stability levels or
any special-purpose needs.

Lastly, the actual _Image_ or _Delta Image_ can be obtained with protocols other
than HTTP, allowing the use of distributed content sharing, mesh networks or
peer to peer networks to offload any central infrastructure.

Together with the ability to precisely control the update process through a
_Device Agent_, the design and architecture of the SystemOTA should be
sufficient to a wide range of appliance-like devices, like network gateways, IP
cameras, routers, switches and more.
