# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: play-through of "rauc install" and "rauc status mark-good"
# This test doesn't run in LXD as RAUC cannot  mount a squashfs due to the
# sandbox blocking that. It would only work if RAUC would be able to detect
# and handle that situation and switch to FUSE. This test works fine in qemu.
backends: [-lxd]
details: |
    Using a custom boot backend, not related to system ota, record what "rauc
    install", followed by "rauc mark-good" is doing on the system.
prepare: |
    mkdir bundle-dir
    mkdir system-dir

    # Create a fake system image
    mkdir system-dir/etc
    printf "ID=asos\nID_VERSION=1\nID_VARIANT=asos-fake\n" > system-dir/etc/os-release

    mksquashfs system-dir bundle-dir/system.img -comp zstd

    # Modify the fake rauc configuration file to set a custom bootloader handler.
    cat <<RAUC_SYSTEM_CONF >>/etc/rauc/system.conf
    [handlers]
    pre-install=$(pwd)/pre-install.sh
    post-install=$(pwd)/post-install.sh
    bootloader-custom-backend=$(pwd)/backend.sh
    RAUC_SYSTEM_CONF

    # Create a fake rauc bundle with that system image
    cat <<RAUC_MANIFEST >bundle-dir/manifest.raucm
    [update]
    compatible=SystemOTA Test Environment
    version=1

    [image.system]
    filename=system.img
    RAUC_MANIFEST

    rauc bundle --cert="$TEST_RAUC_KEYS_DIR/cert.pem" --key="$TEST_RAUC_KEYS_DIR/key.pem" bundle-dir bundle.img

    # Store /proc/cmdline and allow us to fake /proc/cmdline easily in the execute phase below.
    cp /proc/cmdline cmdline.orig
    cp cmdline.orig cmdline
    printf "%s rauc.slot=A\n" "$(cat cmdline.orig)" > cmdline
    mount --bind cmdline /proc/cmdline

    # Stop rauc and grab a cursor for the journal log
    systemctl stop rauc.service || true
    journalctl -u sysotad.service --cursor-file=cursor >/dev/null || true

    # Truncate the log files log and reset backend state
    truncate --size=0 /var/{backend,pre-install,post-install}.log
    echo 0 >/tmp/backend.state

execute: |
    rauc install bundle.img

    # Examine arguments (none) provided to each handler.
    MATCH 'pre-install: $' /tmp/pre-install.log
    MATCH 'post-install: $' /tmp/post-install.log

    # Examine what the pre-install handler saw in the bundle mount point.
    MATCH -- '-rw-r--r-- 1 root root  175 ... .. ..:.. manifest.raucm$' /tmp/pre-install.log
    MATCH -- '-rw-r--r-- 1 root root 4096 ... .. ..:.. system.img$' /tmp/pre-install.log

    # Examine environment variables provided by RAUC to the pre-install handler.
    MATCH 'RAUC_BUNDLE_MOUNT_POINT=/run/rauc/bundle' /tmp/pre-install.log
    MATCH 'RAUC_CURRENT_BOOTNAME=A$' /tmp/pre-install.log
    MATCH 'RAUC_IMAGE_CLASS_1=system$' /tmp/pre-install.log
    MATCH 'RAUC_IMAGE_DIGEST_1=.*' /tmp/pre-install.log
    MATCH 'RAUC_IMAGE_NAME_1=system.img$' /tmp/pre-install.log
    MATCH 'RAUC_MOUNT_PREFIX=/run/rauc$' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_BOOTNAME_1=B$' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_BOOTNAME_2=A$' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_CLASS_1=system$' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_CLASS_2=system$' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_DEVICE_1='"$TEST_RAUC_FAKE_SYSTEM_DIR"'/slot-b' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_DEVICE_2='"$TEST_RAUC_FAKE_SYSTEM_DIR"'/slot-a' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_NAME_1=system.1$' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_NAME_2=system.0$' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_PARENT_1=$' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_PARENT_2=$' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_TYPE_1=raw$' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_TYPE_2=raw$' /tmp/pre-install.log
    if [ "$(rauc --version)" = "rauc 1.5.1" ]; then
        # NOTE(zyga): RAUC adds trailing space here. Be careful when parsing.
        MATCH 'RAUC_SLOTS=1 2 $' /tmp/pre-install.log
        MATCH 'RAUC_TARGET_SLOTS=1 $' /tmp/pre-install.log
    else
        MATCH 'RAUC_SLOTS=1 2$' /tmp/pre-install.log
        MATCH 'RAUC_TARGET_SLOTS=1$' /tmp/pre-install.log
    fi
    MATCH 'RAUC_SYSTEM_CONFIG=/etc/rauc/system.conf$' /tmp/pre-install.log
    MATCH 'RAUC_UPDATE_SOURCE=/run/rauc/bundle$' /tmp/pre-install.log

    # Examine environment variables provided by RAUC to the post-install handler.
    MATCH 'RAUC_BUNDLE_MOUNT_POINT=/run/rauc/bundle$' </tmp/post-install.log
    MATCH 'RAUC_CURRENT_BOOTNAME=A$' </tmp/post-install.log
    MATCH 'RAUC_IMAGE_CLASS_1=system$' </tmp/post-install.log
    MATCH 'RAUC_IMAGE_DIGEST_1=.*$' </tmp/post-install.log
    MATCH 'RAUC_IMAGE_NAME_1=/run/rauc/bundle/system.img$' </tmp/post-install.log
    MATCH 'RAUC_MOUNT_PREFIX=/run/rauc$' </tmp/post-install.log
    MATCH 'RAUC_SLOT_BOOTNAME_1=B$' </tmp/post-install.log
    MATCH 'RAUC_SLOT_BOOTNAME_2=A$' </tmp/post-install.log
    MATCH 'RAUC_SLOT_CLASS_1=system$' </tmp/post-install.log
    MATCH 'RAUC_SLOT_CLASS_2=system$' </tmp/post-install.log
    MATCH 'RAUC_SLOT_DEVICE_1='"$TEST_RAUC_FAKE_SYSTEM_DIR"'/slot-b' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_DEVICE_2='"$TEST_RAUC_FAKE_SYSTEM_DIR"'/slot-a' /tmp/pre-install.log
    MATCH 'RAUC_SLOT_NAME_1=system.1$' </tmp/post-install.log
    MATCH 'RAUC_SLOT_NAME_2=system.0$' </tmp/post-install.log
    MATCH 'RAUC_SLOT_PARENT_1=$' </tmp/post-install.log
    MATCH 'RAUC_SLOT_PARENT_2=$' </tmp/post-install.log
    if [ "$(rauc --version)" = "rauc 1.5.1" ]; then
        # NOTE(zyga): RAUC adds trailing space here. Be careful when parsing.
        MATCH 'RAUC_SLOTS=1 2 $' </tmp/post-install.log
        MATCH 'RAUC_TARGET_SLOTS=1 $' </tmp/post-install.log
    else
        MATCH 'RAUC_SLOTS=1 2$' </tmp/post-install.log
        MATCH 'RAUC_TARGET_SLOTS=1$' </tmp/post-install.log
    fi
    MATCH 'RAUC_SLOT_TYPE_1=raw$' </tmp/post-install.log
    MATCH 'RAUC_SLOT_TYPE_2=raw$' </tmp/post-install.log
    MATCH 'RAUC_SYSTEM_CONFIG=/etc/rauc/system.conf$' </tmp/post-install.log
    MATCH 'RAUC_UPDATE_SOURCE=/run/rauc/bundle$' </tmp/post-install.log

restore: |
    umount /proc/cmdline || true

    rm -rf system-dir bundle-dir
    rm -f *.img
    rm -f /tmp/backend.{log,state}
    rm -f /tmp/{pre,post}-install.log
    rm -f cursor
    rm -f cmdline{,.orig}

debug: |
    cat /proc/cmdline
    test -f /tmp/backend.log && cat /tmp/backend.log
    test -f /tmp/backend.state && cat /tmp/backend.state
    test -f /tmp/pre-install.log && cat /tmp/pre-install.log
    test -f /tmp/post-install.log && cat /tmp/post-install.log
    journalctl --cursor-file=cursor -u rauc.service
    rauc --version
