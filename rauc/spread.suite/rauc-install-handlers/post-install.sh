#!/bin/sh
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.
set -e

# Log command line arguments.
echo "post-install: $*" >> /tmp/post-install.log

# Log all the RAUC environment variables.
env | grep ^RAUC | sort >> /tmp/post-install.log
