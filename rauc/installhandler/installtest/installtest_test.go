// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package installtest_test

import (
	"errors"
	"testing"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/rauc/installhandler"
	"gitlab.com/zygoon/sysota/rauc/installhandler/installtest"
)

func Test(t *testing.T) { TestingT(t) }

type suite struct{}

var _ = Suite(&suite{})

var errBoom = errors.New("boom")

func (s *suite) TestPreInstall(c *C) {
	h := installtest.Handler{}
	env := installhandler.Environment{}
	called := false

	c.Check(func() { _ = h.PreInstall(&env) }, PanicMatches, `please provide PreInstallFn callback function`)

	h.MockPreInstallFn(func(env *installhandler.Environment) error {
		called = true

		return nil
	})

	err := h.PreInstall(&env)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)

	h.MockPreInstallFn(func(env *installhandler.Environment) error {
		return errBoom
	})

	err = h.PreInstall(&env)
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *suite) TestPostInstall(c *C) {
	h := installtest.Handler{}
	env := installhandler.Environment{}
	called := false

	c.Check(func() { _ = h.PostInstall(&env) }, PanicMatches, `please provide PostInstallFn callback function`)

	h.MockPostInstallFn(func(env *installhandler.Environment) error {
		called = true

		return nil
	})

	err := h.PostInstall(&env)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)

	h.MockPostInstallFn(func(env *installhandler.Environment) error {
		return errBoom
	})

	err = h.PostInstall(&env)
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *suite) TestResetCallbacks(c *C) {
	h := &installtest.Handler{}

	h.MockPreInstallFn(func(env *installhandler.Environment) error { return nil })
	h.MockPostInstallFn(func(env *installhandler.Environment) error { return nil })

	h.ResetCallbacks()

	c.Check(h, DeepEquals, &installtest.Handler{})
}
