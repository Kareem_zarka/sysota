// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package installdbus provides D-Bus client to RAUC install handler.
package installdbus

import (
	"os"

	"github.com/godbus/dbus/v5"

	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/rauc/installhandler"
	"gitlab.com/zygoon/sysota/rauc/installhandler/installdbus/spec"
)

// Do calls a function with a D-Bus client and RAUC install handler environment.
func Do(fn func(client *Client, env *installhandler.Environment) error) error {
	env, err := installhandler.NewEnvironment(os.Environ())
	if err != nil {
		return err
	}

	conn, err := dbusutil.SystemBus()
	if err != nil {
		return err
	}

	defer func() {
		e := conn.Close()
		if err == nil {
			err = e
		}
	}()

	client := NewClient(conn)

	return fn(client, env)
}

// Client uses the RAUC interface offered by SystemOTA over D-Bus.
type Client struct {
	conn *dbus.Conn
}

// NewClient returns new Client instance using given bus connection.
func NewClient(conn *dbus.Conn) *Client {
	return &Client{conn: conn}
}

// PreInstall calls the org.oniroproject.sysota1.RAUC.PreInstall D-Bus method.
func (client *Client) PreInstall(env *installhandler.Environment) error {
	serviceObj := client.conn.Object(svcspec.BusName, svcspec.ObjectPath)
	return serviceObj.Call(spec.InterfaceName+"."+spec.PreInstall, 0, env.Environ()).Store()
}

// PostInstall calls the org.oniroproject.sysota1.RAUC.PostInstall D-Bus method.
func (client *Client) PostInstall(env *installhandler.Environment) error {
	serviceObj := client.conn.Object(svcspec.BusName, svcspec.ObjectPath)
	return serviceObj.Call(spec.InterfaceName+"."+spec.PostInstall, 0, env.Environ()).Store()
}
