// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package boothandler defines the interface between RAUC and external programs
// implementing "custom" boot loaders that are not directly implemented and
// supported by RAUC itself.
package boothandler

import (
	"gitlab.com/zygoon/sysota/boot"
)

// Interface is an interface between boot loader and RAUC.
//
// Custom boot backend is documented in detail at
// https://rauc.readthedocs.io/en/latest/integration.html#custom
//
// PrimarySlot and SetPrimarySlot respectively return and set the slot used for
// booting. SlotState and SetSlotState respectively return and set the boot
// state of a given slot.
type Interface interface {
	PrimarySlot() (boot.Slot, error)
	SetPrimarySlot(boot.Slot) error

	SlotState(boot.Slot) (boot.SlotState, error)
	SetSlotState(boot.Slot, boot.SlotState) error
}
