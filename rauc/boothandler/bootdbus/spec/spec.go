// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package spec defines the shared specification between D-Bus client and
// service implementing RAUC custom boot handler and install handler.
package spec

const (
	// InterfaceName is the name of the RAUC boot handler interface provided by SystemOTA.
	InterfaceName = "org.oniroproject.sysota1.RAUC.BootHandler"

	// Methods of the boot handler

	// GetPrimary is the name of the method returning the primary slot.
	GetPrimary = "GetPrimary"
	// SetPrimary is the name of the method setting the primary slot.
	SetPrimary = "SetPrimary"
	// GetState is the name of the method returning state of a given slot.
	GetState = "GetState"
	// SetState is the name of the method setting the state of a given slot.
	SetState = "SetState"
)
