// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package boottest contains test implementation of boothandler.Interface.
package boottest

import (
	"sync"

	"gitlab.com/zygoon/sysota/boot"
)

// Handler implements boothandler.Interface and is useful for writing tests.
type Handler struct {
	m sync.RWMutex

	primarySlotFn    func() (boot.Slot, error)
	setPrimarySlotFn func(boot.Slot) error

	slotStateFn    func(boot.Slot) (boot.SlotState, error)
	setSlotStateFn func(boot.Slot, boot.SlotState) error
}

// ResetCallbacks resets all callback functions to nil.
func (h *Handler) ResetCallbacks() {
	h.m.Lock()
	defer h.m.Unlock()

	h.primarySlotFn = nil
	h.setPrimarySlotFn = nil
	h.slotStateFn = nil
	h.setSlotStateFn = nil
}

// MockPrimarySlotFn sets the primary slot getter function.
func (h *Handler) MockPrimarySlotFn(fn func() (boot.Slot, error)) {
	h.m.Lock()
	defer h.m.Unlock()

	h.primarySlotFn = fn
}

// MockSetPrimarySlotFn sets the primary slot setter function.
func (h *Handler) MockSetPrimarySlotFn(fn func(boot.Slot) error) {
	h.m.Lock()
	defer h.m.Unlock()

	h.setPrimarySlotFn = fn
}

// MockSlotStateFn sets the slot state getter function.
func (h *Handler) MockSlotStateFn(fn func(boot.Slot) (boot.SlotState, error)) {
	h.m.Lock()
	defer h.m.Unlock()

	h.slotStateFn = fn
}

// MockSetSlotStateFn sets the slot state setter function.
func (h *Handler) MockSetSlotStateFn(fn func(boot.Slot, boot.SlotState) error) {
	h.m.Lock()
	defer h.m.Unlock()

	h.setSlotStateFn = fn
}

// PrimarySlot calls the PrimarySlotFn callback function.
func (h *Handler) PrimarySlot() (boot.Slot, error) {
	h.m.RLock()
	defer h.m.RUnlock()

	if h.primarySlotFn == nil {
		panic("please provide PrimarySlotFn callback function")
	}

	return h.primarySlotFn()
}

// SetPrimarySlot calls the SetPrimarySlotFn callback function.
func (h *Handler) SetPrimarySlot(slot boot.Slot) error {
	h.m.RLock()
	defer h.m.RUnlock()

	if h.setPrimarySlotFn == nil {
		panic("please provide SetPrimarySlotFn callback function")
	}

	return h.setPrimarySlotFn(slot)
}

// SlotState calls the SlotStateFn callback function.
func (h *Handler) SlotState(slot boot.Slot) (boot.SlotState, error) {
	h.m.RLock()
	defer h.m.RUnlock()

	if h.slotStateFn == nil {
		panic("please provide SlotStateFn callback function")
	}

	return h.slotStateFn(slot)
}

// SetSlotState calls the SetSlotStateFn callback function.
func (h *Handler) SetSlotState(slot boot.Slot, state boot.SlotState) error {
	h.m.RLock()
	defer h.m.RUnlock()

	if h.setSlotStateFn == nil {
		panic("please provide SetSlotStateFn callback function")
	}

	return h.setSlotStateFn(slot, state)
}
