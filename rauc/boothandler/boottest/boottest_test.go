// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package boottest_test

import (
	"testing"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/rauc/boothandler/boottest"
)

func Test(t *testing.T) { TestingT(t) }

type bootBackendSuite struct{}

var _ = Suite(&bootBackendSuite{})

func (s *bootBackendSuite) TestPrimarySlot(c *C) {
	var backend boottest.Handler

	c.Check(func() { _, _ = backend.PrimarySlot() }, PanicMatches, `please provide PrimarySlotFn callback function`)

	backend.MockPrimarySlotFn(func() (boot.Slot, error) {
		return boot.SlotA, nil
	})

	slot, err := backend.PrimarySlot()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotA)
}

func (s *bootBackendSuite) TestSetPrimarySlot(c *C) {
	var backend boottest.Handler

	c.Check(func() { _ = backend.SetPrimarySlot(boot.SlotA) }, PanicMatches, `please provide SetPrimarySlotFn callback function`)

	called := false

	backend.MockSetPrimarySlotFn(func(slot boot.Slot) error {
		c.Assert(slot, Equals, boot.SlotA)

		called = true

		return nil
	})

	err := backend.SetPrimarySlot(boot.SlotA)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *bootBackendSuite) TestSlotState(c *C) {
	var backend boottest.Handler

	c.Check(func() { _, _ = backend.SlotState(boot.SlotA) }, PanicMatches, `please provide SlotStateFn callback function`)

	called := false

	backend.MockSlotStateFn(func(slot boot.Slot) (boot.SlotState, error) {
		c.Assert(slot, Equals, boot.SlotA)

		called = true

		return boot.GoodSlot, nil
	})

	state, err := backend.SlotState(boot.SlotA)
	c.Assert(err, IsNil)
	c.Check(state, Equals, boot.GoodSlot)
	c.Check(called, Equals, true)
}

func (s *bootBackendSuite) TestSetSlotState(c *C) {
	var backend boottest.Handler

	c.Check(func() { _ = backend.SetSlotState(boot.SlotA, boot.GoodSlot) }, PanicMatches, `please provide SetSlotStateFn callback function`)

	called := false

	backend.MockSetSlotStateFn(func(slot boot.Slot, state boot.SlotState) error {
		c.Assert(slot, Equals, boot.SlotA)
		c.Assert(state, Equals, boot.GoodSlot)

		called = true

		return nil
	})

	err := backend.SetSlotState(boot.SlotA, boot.GoodSlot)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *bootBackendSuite) TestResetCallbacks(c *C) {
	backend := &boottest.Handler{}

	backend.MockPrimarySlotFn(func() (boot.Slot, error) { return boot.InvalidSlot, nil })
	backend.MockSetPrimarySlotFn(func(boot.Slot) error { return nil })
	backend.MockSlotStateFn(func(slot boot.Slot) (boot.SlotState, error) { return boot.InvalidSlotState, nil })
	backend.MockSetSlotStateFn(func(slot boot.Slot, state boot.SlotState) error { return nil })

	backend.ResetCallbacks()

	c.Check(backend, DeepEquals, &boottest.Handler{})
}
