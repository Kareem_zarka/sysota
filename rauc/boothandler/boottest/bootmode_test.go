// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package boottest_test

import (
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/rauc/boothandler/boottest"
)

type bootStateSuite struct{}

var _ = Suite(&bootStateSuite{})

func (s *bootStateSuite) TestBootModeReadWrite(c *C) {
	bootState := boottest.BootState{}

	c.Check(bootState.BootMode(), Equals, boot.Normal)

	err := bootState.SetBootMode(boot.Try, false)
	c.Assert(err, IsNil)
	c.Check(bootState.BootMode(), Equals, boot.Try)
}

func (s *bootStateSuite) TestBootModeChangeObserver(c *C) {
	changes := make([]boot.Mode, 0, 3)
	bootState := boottest.BootState{
		BootModeChanged: func(newBootMode boot.Mode, isRollback bool) error {
			changes = append(changes, newBootMode)
			return nil
		},
	}

	c.Assert(bootState.SetBootMode(boot.Normal, false), IsNil)
	c.Check(bootState.BootMode(), Equals, boot.Normal)
	c.Assert(bootState.SetBootMode(boot.Try, false), IsNil)
	c.Check(bootState.BootMode(), Equals, boot.Try)
	c.Assert(bootState.SetBootMode(boot.Normal, false), IsNil)
	c.Check(bootState.BootMode(), Equals, boot.Normal)

	c.Check(changes, DeepEquals, []boot.Mode{boot.Try, boot.Normal})
}

func (s *bootStateSuite) TestBootModeChangeGuard(c *C) {
	bootState := boottest.BootState{
		BootModeChanged: func(newBootMode boot.Mode, isRollback bool) error {
			if newBootMode != boot.Normal && newBootMode != boot.Try {
				return boot.ErrInvalidBootMode
			}
			return nil
		},
	}

	c.Assert(bootState.SetBootMode(boot.Normal, false), IsNil)
	c.Check(bootState.BootMode(), Equals, boot.Normal)
	c.Assert(bootState.SetBootMode(boot.Try, false), IsNil)
	c.Check(bootState.BootMode(), Equals, boot.Try)
	c.Assert(bootState.SetBootMode(boot.InvalidBootMode, false), ErrorMatches, `invalid boot mode`)
	c.Check(bootState.BootMode(), Equals, boot.Try)
}
