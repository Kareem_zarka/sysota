// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package dirs provides paths to well known directories that need to be shared
// across the SystemOTA codebase. In general you should only need to add paths
// here if the path needs to be referred from multiple modules.
package dirs

const (
	// Proc is the path to the /proc directory. It exists to reduce typo-prone literals.
	Proc = "/proc"
)
