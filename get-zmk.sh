#!/bin/sh
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.
set -ex

D="$(mktemp -d)"
trap 'rm -rf "$D"' EXIT

cd "$D"

wget https://github.com/zyga/zmk/releases/download/v0.5.1/zmk-0.5.1.tar.gz
wget https://github.com/zyga/zmk/releases/download/v0.5.1/zmk-0.5.1.tar.gz.asc

gpg --keyserver keyserver.ubuntu.com --recv-keys B76CED9B45CAF1557D271A6A2894E93A28C67B47
gpg --verify zmk-0.5.1.tar.gz.asc

tar zxf zmk-0.5.1.tar.gz
make -C zmk-0.5.1
sudo make -C zmk-0.5.1 install
