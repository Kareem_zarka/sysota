// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package boot_test

import (
	"errors"
	"testing"

	"gitlab.com/zygoon/sysota/boot"
	. "gopkg.in/check.v1"
)

func Test(t *testing.T) { TestingT(t) }

type slotStateSuite struct{}

var _ = Suite(&slotStateSuite{})

func (s *slotStateSuite) TestString(c *C) {
	c.Check(boot.InvalidSlot.String(), Equals, "invalid")
	c.Check(boot.SlotA.String(), Equals, "A")
	c.Check(boot.SlotB.String(), Equals, "B")
	c.Check(func() { _ = boot.Slot(42).String() }, PanicMatches, "invalid boot slot")
}

func (s *slotStateSuite) TestUnmarshalText(c *C) {
	var slot boot.Slot

	c.Check(slot, Equals, boot.InvalidSlot)

	c.Assert(slot.UnmarshalText([]byte("A")), IsNil)
	c.Check(slot, Equals, boot.SlotA)

	c.Assert(slot.UnmarshalText([]byte("B")), IsNil)
	c.Check(slot, Equals, boot.SlotB)

	c.Assert(slot.UnmarshalText([]byte("C")), ErrorMatches, "invalid boot slot")
}

func (s *slotStateSuite) TestMarshalText(c *C) {
	data, err := boot.SlotA.MarshalText()
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte(`A`))

	data, err = boot.SlotB.MarshalText()
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte(`B`))

	_, err = boot.Slot(42).MarshalText()
	c.Assert(err, ErrorMatches, `invalid boot slot`)
}

type stateSuite struct{}

var _ = Suite(&stateSuite{})

func (s *stateSuite) TestString(c *C) {
	c.Check(boot.InvalidSlotState.String(), Equals, "invalid")
	c.Check(boot.BadSlot.String(), Equals, "bad")
	c.Check(boot.GoodSlot.String(), Equals, "good")
	c.Check(boot.SlotState(42).String, PanicMatches, `invalid slot state`)
}

func (s *stateSuite) TestMarshalText(c *C) {
	data, err := boot.GoodSlot.MarshalText()
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte(`good`))

	data, err = boot.BadSlot.MarshalText()
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte(`bad`))

	_, err = boot.SlotState(42).MarshalText()
	c.Assert(errors.Is(err, boot.ErrInvalidSlotState), Equals, true)
}

func (s *stateSuite) TestUnmarshalText(c *C) {
	var bootState boot.SlotState

	err := bootState.UnmarshalText([]byte(`good`))
	c.Assert(err, IsNil)
	c.Check(bootState, Equals, boot.GoodSlot)

	err = bootState.UnmarshalText([]byte(`bad`))
	c.Assert(err, IsNil)
	c.Check(bootState, Equals, boot.BadSlot)

	err = bootState.UnmarshalText([]byte(`potato`))
	c.Assert(errors.Is(err, boot.ErrInvalidSlotState), Equals, true)
}

type modeSuite struct{}

var _ = Suite(&modeSuite{})

func (s *modeSuite) TestZeroValue(c *C) {
	var mode boot.Mode

	c.Check(mode, Equals, boot.Normal)
}

func (s *modeSuite) TestString(c *C) {
	c.Check(boot.InvalidBootMode.String(), Equals, "invalid")
	c.Check(boot.Normal.String(), Equals, "normal")
	c.Check(boot.Try.String(), Equals, "try")
	c.Check(func() { _ = boot.Mode(42).String() }, PanicMatches, `invalid boot mode`)
}

func (s *modeSuite) TestMarshalText(c *C) {
	data, err := boot.Normal.MarshalText()
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte(`normal`))

	data, err = boot.Try.MarshalText()
	c.Assert(err, IsNil)
	c.Check(data, DeepEquals, []byte(`try`))

	_, err = boot.Mode(42).MarshalText()
	c.Assert(err, ErrorMatches, `invalid boot mode`)
}

func (s *modeSuite) TestUnmarshalText(c *C) {
	var mode boot.Mode

	c.Assert(mode.UnmarshalText([]byte("normal")), IsNil)
	c.Check(mode, Equals, boot.Normal)

	c.Assert(mode.UnmarshalText([]byte("try")), IsNil)
	c.Check(mode, Equals, boot.Try)

	c.Assert(mode.UnmarshalText([]byte("potato")), ErrorMatches, `invalid boot mode`)
}

type miscSuite struct{}

var _ = Suite(&miscSuite{})

func (*miscSuite) TestSynthesizeInactiveSlot(c *C) {
	c.Check(boot.SynthesizeInactiveSlot(boot.SlotA), Equals, boot.SlotB)
	c.Check(boot.SynthesizeInactiveSlot(boot.SlotB), Equals, boot.SlotA)
	c.Check(boot.SynthesizeInactiveSlot(boot.InvalidSlot), Equals, boot.InvalidSlot)
}
