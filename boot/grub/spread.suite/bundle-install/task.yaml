# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: play-through of "rauc install" with GRUB backend
# This test doesn't run in LXD as RAUC cannot  mount a squashfs due to the
# sandbox blocking that. It would only work if RAUC would be able to detect
# and handle that situation and switch to FUSE. This test works fine in qemu.
backends: [-lxd]
environment:
    REBOOT_DELAY/quirk: 10
    REBOOT_DELAY/none: 0
prepare: |
    mkdir bundle-dir
    mkdir system-dir

    # Create a fake system image.
    # TODO: once GRUB boot asset updates are implemented, put some fake assets here.
    mkdir system-dir/etc
    mksquashfs system-dir bundle-dir/system.img -comp zstd

    # APPEND to the RAUC configuration file to set SystemOTA as custom
    # bootloader handler, pre-install handler and post-install handler.
    cat <<RAUC_SYSTEM_CONF >>/etc/rauc/system.conf
    [handlers]
    pre-install=/usr/libexec/sysota/rauc-pre-install-handler
    post-install=/usr/libexec/sysota/rauc-post-install-handler
    bootloader-custom-backend=/usr/libexec/sysota/rauc-custom-boot-handler
    RAUC_SYSTEM_CONF

    # Create a fake rauc bundle with that system image
    cat <<RAUC_MANIFEST >bundle-dir/manifest.raucm
    [update]
    compatible=SystemOTA Test Environment
    version=1

    [image.system]
    filename=system.img
    RAUC_MANIFEST

    rauc bundle --cert="$TEST_RAUC_KEYS_DIR/cert.pem" --key="$TEST_RAUC_KEYS_DIR/key.pem" bundle-dir bundle.img

    # Store /proc/cmdline and allow us to fake /proc/cmdline easily in the execute phase below.
    cp /proc/cmdline cmdline.orig
    cp cmdline.orig cmdline
    printf "%s rauc.slot=A\n" "$(cat cmdline.orig)" > cmdline
    mount --bind cmdline /proc/cmdline

    # Stop rauc and grab a cursor for the journal log
    systemctl stop rauc.service || true
    journalctl -u sysotad.service --cursor-file=cursor >/dev/null || true

    # Truncate the log files log and reset backend state
    truncate --size=0 /var/{backend,pre-install,post-install}.log
    echo 0 >/tmp/backend.state

    # Stop sysotad for the modification.
    systemctl stop sysotad.service

    # Add runtime override to set up reboot quirk.
    cat <<SYSOTAD_CONF >/run/sysota/sysotad.conf
    # Configure the reboot delay quirk.
    [Quirks]
    RebootDelay=$REBOOT_DELAY
    SYSOTAD_CONF

execute: |
    # Slot A is active, other variables are unset.
    grub-editenv "$SYSOTA_BOOT_DIR"/grubenv list | grep -E ^SYSOTA_ | MATCH '^SYSOTA_BOOT_ACTIVE=A$'
    grub-editenv "$SYSOTA_BOOT_DIR"/grubenv list | grep -E ^SYSOTA_ | NOMATCH '^SYSOTA_BOOT_TRY=.*$'
    grub-editenv "$SYSOTA_BOOT_DIR"/grubenv list | grep -E ^SYSOTA_ | NOMATCH '^SYSOTA_BOOT_TRIED=.*'

    rauc install bundle.img

    # Slot A is still active but slot B is now selected for next boot. The tested slot is not set at all.
    grub-editenv "$SYSOTA_BOOT_DIR"/grubenv list | grep -E ^SYSOTA_ | MATCH '^SYSOTA_BOOT_ACTIVE=A$'
    grub-editenv "$SYSOTA_BOOT_DIR"/grubenv list | grep -E ^SYSOTA_ | MATCH '^SYSOTA_BOOT_TRY=B$'
    grub-editenv "$SYSOTA_BOOT_DIR"/grubenv list | grep -E ^SYSOTA_ | NOMATCH '^SYSOTA_BOOT_TRIED=.*'

    # The system did not reboot immediately.
    if [ "$REBOOT_DELAY" -ne 0 ]; then
        test ! -f "$MOCK_REBOOT_LOG"
        sleep "$REBOOT_DELAY"
    fi

    # With sufficient time, the system did reboot.
    test -f "$MOCK_REBOOT_LOG"
    MATCH '^/usr/sbin/reboot$' "$MOCK_REBOOT_LOG"

restore: |
    umount /proc/cmdline || true

    rm -rf system-dir bundle-dir
    rm -f *.img
    rm -f cursor
    rm -f cmdline{,.orig}
    rm -f /run/sysota/sysotad.conf

debug: |
    cat /proc/cmdline
    journalctl --cursor-file=cursor -u rauc.service
