// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package grub implements the boot protocol for GRUB.
//
// This implementation is compatible with both the legacy and the EFI version of
// GRUB. It does not rely on any EFI-specific features. The internal protocol
// between the userspace and the bootloader is organized with three GRUB
// environment variables.
//
// There is one variable which needs to be permanently present and two variables
// which participate with the try-boot mechanism. The SYSOTA_BOOT_ACTIVE
// variable must be either set to "A" or "B" and define the slot which is booted
// into outside of the update process. When an update is attempted SysOTA sets
// SYSOTA_BOOT_TRY to the name of the slot, again either "A" or "B" that the
// bootloader should boot next. The bootloader script guarantees that this slot
// is only booted into once by deleting the variable before transferring control.
// Before erasing the variable, the value is copied to SYSOTA_BOOT_TRIED. Back
// in userspace, SysOTA can commit or rollback the update transaction by looking
// at this variable and either erasing it (rollback) or copying it to
// SYSOTA_BOOT_ACTIVE (commit).
//
// The implementation is not related to the similar RAUC boot script which is
// more generic but harder to reason about in the terms of transactional
// updates.
package grub

import (
	"errors"
	"fmt"

	"gitlab.com/zygoon/go-grub"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/rauc/installhandler"
)

const (
	// nameActiveSlot is a GRUB variable naming the active boot slot.
	//
	// Active slot is the slot selected by the bootloader script by default
	// unless the one-time boot variable is defined. In case the one-time boot
	// fails a manual or automatic power-cycle guarantees to recover to the
	// active slot.
	nameActiveSlot = "SYSOTA_BOOT_ACTIVE"
	// nameNextSlot is a GRUB variable naming the boot-once slot.
	//
	// The boot-next slot is booted into once. Before transferring control to
	// the kernel, the bootloader script saves the value of this variable into
	// SYSOTA_BOOT_TRIED and deletes the variable. This ensures that in case
	// of boot failure the system will recover and use the active slot after
	// forced power-cycle.
	nameNextSlot = "SYSOTA_BOOT_TRY"
	// nameTestedSlot is a GRUB variable naming the boot-tested slot.
	//
	// See the description of nameNextSlot to understand the relationship
	// between SYSOTA_BOOT_TRY and SYSOTA_BOOT_TRIED.
	nameTestedSlot = "SYSOTA_BOOT_TRIED"
)

var (
	// errActiveSlotUnset records GRUB environment not defining SYSOTA_BOOT_ACTIVE= variable.
	errActiveSlotUnset = errors.New(nameActiveSlot + " is not set")
	// errTestedSlotUnset records GRUB environment not defining SYSOTA_BOOT_TRIED= variable.
	errTestedSlotUnset = errors.New(nameTestedSlot + " is not set")
)

// Boot implements boot.Protocol for the GRUB bootloader.
type Boot struct {
	boot.DelayedReboot
	envPath string
}

// New creates a new GrubBoot operating on the given GRUB environment file.
//
// The file must be present and must be a correct GRUB environment block file.
// If the file is missing or corrupted all of the protocol methods will fail
// with an appropriate error.
func New(envPath string) *Boot {
	return &Boot{envPath: envPath}
}

// QueryActive returns the slot that is used for booting.
//
// Active slot is determined by the value of the SYSOTA_BOOT_ACTIVE variable.
func (b *Boot) QueryActive() (slot boot.Slot, err error) {
	env, err := loadEnv(b.envPath)
	if err != nil {
		return boot.InvalidSlot, &boot.UnknownActiveSlotError{Err: err}
	}

	slotName, ok := env.Lookup(nameActiveSlot)
	if !ok {
		return boot.InvalidSlot, &boot.UnknownActiveSlotError{Err: errActiveSlotUnset}
	}

	if err := slot.UnmarshalText([]byte(slotName)); err != nil {
		return boot.InvalidSlot, &boot.UnknownActiveSlotError{Err: fmt.Errorf("cannot recognize active slot: %w", err)}
	}

	return slot, nil
}

// QueryInactive returns the slot that is not used for booting.
func (b *Boot) QueryInactive() (boot.Slot, error) {
	// Find the active slot and return the other slot.
	slot, err := b.QueryActive()
	if err != nil {
		// Unwrap UnknownActiveSlotError to avoid confusing error messages.
		var realErr = err

		var e *boot.UnknownActiveSlotError

		if errors.As(err, &e) {
			realErr = e.Unwrap()
		}

		return boot.InvalidSlot, &boot.UnknownInactiveSlotError{Err: realErr}
	}

	return boot.SynthesizeInactiveSlot(slot), nil
}

// TrySwitch configures the bootloader for a one-off boot using the given slot.
func (b *Boot) TrySwitch(targetSlot boot.Slot) error {
	env, err := loadEnv(b.envPath)
	if err != nil {
		return err
	}

	// Set the boot-once variable to the name of the target slot.
	if err := env.Set(nameNextSlot, targetSlot.String()); err != nil {
		return fmt.Errorf("cannot set boot-next slot: %w", err)
	}

	return saveEnv(env, b.envPath)
}

// CommitSwitch re-configures a try-mode slot for continuous use.
func (b *Boot) CommitSwitch() error {
	env, err := loadEnv(b.envPath)
	if err != nil {
		return &boot.UnknownInactiveSlotError{Err: err}
	}

	slotName, ok := env.Lookup(nameTestedSlot)
	if !ok {
		return &boot.UnknownInactiveSlotError{Err: errTestedSlotUnset}
	}

	var slot boot.Slot

	// Set the persistent boot variable to the name of the booted one-time slot.
	if err := slot.UnmarshalText([]byte(slotName)); err != nil {
		return fmt.Errorf("cannot recognize tested slot: %w", err)
	}

	// Delete the name of the tested slot.
	env.Delete(nameTestedSlot)

	if err := env.Set(nameActiveSlot, slot.String()); err != nil {
		return fmt.Errorf("cannot set active slot: %w", err)
	}

	return saveEnv(env, b.envPath)
}

// Reboot gracefully reboots the system.
func (b *Boot) Reboot(_ boot.RebootFlags) error {
	return b.DelayedReboot.RebootSystem()
}

// CancelSwitch cancels the switch previously prepared by TrySwitch.
func (b *Boot) CancelSwitch() error {
	env, err := loadEnv(b.envPath)
	if err != nil {
		return err
	}

	// Delete variables holding the names of the boot-once slot and the
	// boot-once tested slot. In normal cases the nameNextSlot will be unset
	// but this allows us to cancel the switch in case we have not re-booted
	// yet.
	env.Delete(nameNextSlot)
	env.Delete(nameTestedSlot)

	return saveEnv(env, b.envPath)
}

// PreInstall does nothing.
func (b *Boot) PreInstall(env *installhandler.Environment) (err error) {
	// TODO: decide on how and when boot assets are updated and perform the matching
	// non-atomic, non A/B GRUB binary and script (if present).
	//
	// This is tracked by https://gitlab.eclipse.org/eclipse/oniro-core/sysota/-/issues/8
	return nil
}

// PostInstall reboots the system.
func (b *Boot) PostInstall(env *installhandler.Environment) error {
	return b.Reboot(0)
}

// loadEnv loads GRUB environment and wraps the error to SysOTA conventions.
func loadEnv(path string) (*grub.Env, error) {
	env, err := grub.LoadEnv(path)
	if err != nil {
		return nil, fmt.Errorf("cannot load GRUB environment: %w", err)
	}

	return env, nil
}

// saveEnv saves GRUB environment and wraps the error to SysOTA conventions.
func saveEnv(env *grub.Env, path string) error {
	if err := env.Save(path); err != nil {
		return fmt.Errorf("cannot save GRUB environment: %w", err)
	}

	return nil
}
