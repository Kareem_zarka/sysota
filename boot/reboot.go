// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package boot

import (
	"log"
	"os/exec"
	"sync"
	"time"
)

const rebootCommand = "reboot"

// RebootSystem gracefully reboots the system.
//
// Additional arguments are passed to the system reboot utility.
func RebootSystem(args ...string) error {
	log.Printf("Rebooting the system with: %s %s", rebootCommand, args)
	cmd := exec.Command(rebootCommand, args...)

	return cmd.Run()
}

// DelayedReboot implements reboot postponed by configurable delay.
//
// Delayed reboot works around a limitation in rauc-hawkbit-updater, which fails
// to mark the update as successful if a custom boot handler reboots the device
// during the bundle install operation.
//
// DelayedReboot contains a sync.Mutex and observes the same constraints on copying.
type DelayedReboot struct {
	m     sync.Mutex
	delay time.Duration
	timer *time.Timer
}

// RebootSystem gracefully reboots the system after the desired delay.
func (r *DelayedReboot) RebootSystem(args ...string) error {
	r.m.Lock()

	if r.delay == 0 {
		r.m.Unlock()

		return RebootSystem(args...)
	}

	// NOTE: this interacts with the exit-when-inactive feature.
	log.Printf("Scheduling system reboot after %s", r.delay)
	r.timer = time.AfterFunc(r.delay, func() {
		if err := RebootSystem(args...); err != nil {
			log.Printf("Cannot reboot the system: %s", err)
		}
	})

	r.m.Unlock()

	return nil
}

// SetRebootDelay causes the post-install handler to wait before rebooting.
func (r *DelayedReboot) SetRebootDelay(d time.Duration) {
	r.m.Lock()
	defer r.m.Unlock()

	r.delay = d
}

// RebootPending returns true if a reboot is pending.
func (r *DelayedReboot) RebootPending() bool {
	r.m.Lock()
	defer r.m.Unlock()

	return r.timer != nil
}
