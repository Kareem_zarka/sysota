// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package testboot provides a boot protocol that is suitable for testing.
package testboot

import (
	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/rauc/installhandler/installtest"
)

// Protocol is a boot.Protocol meant for testing
//
// All the methods of the boot.Protocol interface are exposed as callbacks.
type Protocol struct {
	QueryActiveFn   func() (boot.Slot, error)
	QueryInactiveFn func() (boot.Slot, error)
	TrySwitchFn     func(boot.Slot) error
	RebootFn        func(boot.RebootFlags) error
	CommitSwitchFn  func() error
	CancelSwitchFn  func() error
}

// ResetCallbacks resets all callback functions to nil.
func (proto *Protocol) ResetCallbacks() {
	*proto = Protocol{}
}

// QueryActive runs QueryActiveFn callback function.
func (proto *Protocol) QueryActive() (boot.Slot, error) {
	if proto.QueryActiveFn == nil {
		panic("please provide QueryActiveFn callback function")
	}

	return proto.QueryActiveFn()
}

// QueryInactive runs QueryInactiveFn callback function.
func (proto *Protocol) QueryInactive() (boot.Slot, error) {
	if proto.QueryInactiveFn == nil {
		panic("please provide QueryInactiveFn callback function")
	}

	return proto.QueryInactiveFn()
}

// TrySwitch runs TrySwitchFn callback function.
func (proto *Protocol) TrySwitch(slot boot.Slot) error {
	if proto.TrySwitchFn == nil {
		panic("please provide TrySwitchFn callback function")
	}

	return proto.TrySwitchFn(slot)
}

// Reboot runs RebootFn callback function.
func (proto *Protocol) Reboot(flags boot.RebootFlags) error {
	if proto.RebootFn == nil {
		panic("please provide RebootFn callback function")
	}

	return proto.RebootFn(flags)
}

// CommitSwitch runs CommitSwitchFn callback function.
func (proto *Protocol) CommitSwitch() error {
	if proto.CommitSwitchFn == nil {
		panic("please provide CommitSwitchFn callback function")
	}

	return proto.CommitSwitchFn()
}

// CancelSwitch runs CancelSwitchFn callback function.
func (proto *Protocol) CancelSwitch() error {
	if proto.CancelSwitchFn == nil {
		panic("please provide CancelSwitchFn callback function")
	}

	return proto.CancelSwitchFn()
}

// InstallAwareProtocol is a boot.Protocol aware of Install handlers.
type InstallAwareProtocol struct {
	Protocol
	installtest.Handler
}

// ResetCallbacks resets all callback functions to nil.
func (proto *InstallAwareProtocol) ResetCallbacks() {
	*proto = InstallAwareProtocol{}
}
