// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package testboot_test

import (
	"testing"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/boot/testboot"
	"gitlab.com/zygoon/sysota/rauc/installhandler"
	"gitlab.com/zygoon/sysota/rauc/installhandler/installtest"
)

func Test(t *testing.T) { TestingT(t) }

type bootProtoSuite struct{}

var _ = Suite(&bootProtoSuite{})

func (s *bootProtoSuite) TestQueryActive(c *C) {
	var proto testboot.Protocol

	c.Check(func() { _, _ = proto.QueryActive() }, PanicMatches, `please provide QueryActiveFn callback function`)

	proto.QueryActiveFn = func() (boot.Slot, error) {
		return boot.SlotA, nil
	}

	slot, err := proto.QueryActive()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotA)
}

func (s *bootProtoSuite) TestQueryInactive(c *C) {
	var proto testboot.Protocol

	c.Check(func() { _, _ = proto.QueryInactive() }, PanicMatches, `please provide QueryInactiveFn callback function`)

	proto.QueryInactiveFn = func() (boot.Slot, error) {
		return boot.SlotB, nil
	}

	slot, err := proto.QueryInactive()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotB)
}

func (s *bootProtoSuite) TestTrySwitch(c *C) {
	var proto testboot.Protocol

	c.Check(func() { _ = proto.TrySwitch(boot.SlotB) }, PanicMatches, `please provide TrySwitchFn callback function`)

	called := false
	proto.TrySwitchFn = func(slot boot.Slot) error {
		called = true

		c.Assert(slot, Equals, boot.SlotB)

		return nil
	}

	err := proto.TrySwitch(boot.SlotB)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *bootProtoSuite) TestReboot(c *C) {
	var proto testboot.Protocol

	c.Check(func() { _ = proto.Reboot(boot.RebootFlags(0)) }, PanicMatches, `please provide RebootFn callback function`)

	called := false
	proto.RebootFn = func(flags boot.RebootFlags) error {
		called = true

		c.Assert(flags, Equals, boot.RebootFlags(0))

		return nil
	}

	err := proto.Reboot(0)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)

	called = false
	proto.RebootFn = func(flags boot.RebootFlags) error {
		called = true

		c.Assert(flags, Equals, boot.RebootTryBoot)

		return nil
	}

	err = proto.Reboot(boot.RebootTryBoot)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *bootProtoSuite) TestCommitSwitch(c *C) {
	var proto testboot.Protocol

	c.Check(func() { _ = proto.CommitSwitch() }, PanicMatches, `please provide CommitSwitchFn callback function`)

	called := false
	proto.CommitSwitchFn = func() error {
		called = true

		return nil
	}

	err := proto.CommitSwitch()
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *bootProtoSuite) TestCancelSwitch(c *C) {
	var proto testboot.Protocol

	c.Check(func() { _ = proto.CancelSwitch() }, PanicMatches, `please provide CancelSwitchFn callback function`)

	called := false
	proto.CancelSwitchFn = func() error {
		called = true

		return nil
	}

	err := proto.CancelSwitch()
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *bootProtoSuite) TestResetCallbacks(c *C) {
	proto := testboot.Protocol{
		QueryActiveFn:   func() (boot.Slot, error) { return boot.InvalidSlot, nil },
		QueryInactiveFn: func() (boot.Slot, error) { return boot.InvalidSlot, nil },
		TrySwitchFn:     func(boot.Slot) error { return nil },
		RebootFn:        func(boot.RebootFlags) error { return nil },
		CommitSwitchFn:  func() error { return nil },
		CancelSwitchFn:  func() error { return nil },
	}

	proto.ResetCallbacks()

	c.Check(proto, DeepEquals, testboot.Protocol{})
}

type installBootProtoSuite struct{}

var _ = Suite(&installBootProtoSuite{})

func (s *installBootProtoSuite) TestPreInstallHandler(c *C) {
	var proto testboot.InstallAwareProtocol

	c.Check(func() { _ = proto.PreInstall(nil) }, PanicMatches, `please provide PreInstallFn callback function`)

	env := &installhandler.Environment{}
	called := false

	proto.MockPreInstallFn(func(env2 *installhandler.Environment) error {
		called = true

		c.Assert(env, Equals, env2)

		return nil
	})

	err := proto.PreInstall(env)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *installBootProtoSuite) TestPostInstallHandler(c *C) {
	var proto testboot.InstallAwareProtocol

	c.Check(func() { _ = proto.PostInstall(nil) }, PanicMatches, `please provide PostInstallFn callback function`)

	env := &installhandler.Environment{}
	called := false

	proto.MockPostInstallFn(func(env2 *installhandler.Environment) error {
		called = true

		c.Assert(env, Equals, env2)

		return nil
	})

	err := proto.PostInstall(env)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *installBootProtoSuite) TestResetCallbacks(c *C) {
	proto := &testboot.InstallAwareProtocol{
		Protocol: testboot.Protocol{
			QueryActiveFn:   func() (boot.Slot, error) { return boot.InvalidSlot, nil },
			QueryInactiveFn: func() (boot.Slot, error) { return boot.InvalidSlot, nil },
			TrySwitchFn:     func(boot.Slot) error { return nil },
			RebootFn:        func(boot.RebootFlags) error { return nil },
			CommitSwitchFn:  func() error { return nil },
			CancelSwitchFn:  func() error { return nil },
		},
		Handler: installtest.Handler{},
	}

	proto.Handler.MockPreInstallFn(func(*installhandler.Environment) error { return nil })
	proto.Handler.MockPostInstallFn(func(*installhandler.Environment) error { return nil })

	proto.ResetCallbacks()

	c.Check(proto, DeepEquals, &testboot.InstallAwareProtocol{})
}
