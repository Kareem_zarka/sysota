// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package piboot_test

import (
	"errors"
	"os"
	"path/filepath"

	"gitlab.com/zygoon/go-raspi/pimodel"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/boot/piboot"
)

type bootConfigSuite struct {
	bootDir string
}

var _ = Suite(&bootConfigSuite{})

func (s *bootConfigSuite) SetUpTest(c *C) {
	s.bootDir = c.MkDir()
}

// A number of revisions that have specific properties. Here we mostly care
// about the board type and in a specific case, about the processor used.

const (
	Cm1RevCode         = 0x0014
	Cm3PlusRevCode     = 0xa02100
	Cm3RevCode         = 0xa020a0
	Cm4RevCode         = 0xa03140
	Pi0RevCode         = 0x900092
	Pi0WRevCode        = 0x9000c1
	Pi1APlusRevCode    = 0x0012
	Pi1ARevCode        = 0x0007
	Pi1BPlusRevCode    = 0x0010
	Pi1BRevCode        = 0x0002
	Pi2BRevCodeBcm2836 = 0xa01040
	Pi2BRevCodeBcm2837 = 0xa02042
	Pi3APlusRevCode    = 0x9020e0
	Pi3BPlusRevCode    = 0xa020d3
	Pi3BRevCode        = 0xa02082
	Pi400RevCode       = 0xc03130
	Pi4BRevCode        = 0xa03111
)

const sampleSerial = 0x10000000aa586ea0

// TestTypicalSaveUseCase tests the typical use of BootConfig.
//
// Typical use case is to load the config.txt from a slot-specific
// subdirectory, modify os_prefix= in config.txt and root= in cmdline.txt, then
// set the BootMountPoint to the parent directory, set ConfigFileName to
// tryboot.txt and save.
func (s *bootConfigSuite) TestTypicalSaveUseCase(c *C) {
	// Preparation: a boot/slot-b directory with a vanilla copy of the /boot
	// directory from the new image we are trying to install.
	c.Assert(os.Mkdir(filepath.Join(s.bootDir, "slot-b"), 0o700), IsNil)
	c.Assert(os.WriteFile(filepath.Join(s.bootDir, "slot-b", "config.txt"), []byte("disable_overscan=1\n"), 0o600), IsNil)
	c.Assert(os.WriteFile(filepath.Join(s.bootDir, "slot-b", "cmdline.txt"), []byte(`root=/dev/mmcblk0p2`), 0o600), IsNil)

	// Load the config pretending that the slot-specific directory is the real /boot directory.
	// We have not re-located it yet, so it must be done this way.
	bootCfg, err := piboot.NewBootConfig(filepath.Join(s.bootDir, "slot-b"), Pi4BRevCode, sampleSerial)
	c.Assert(err, IsNil)

	// Modification time.

	bootCfg.BootMountPoint = s.bootDir
	bootCfg.ConfigFileName = "tryboot.txt"
	bootCfg.CmdLine.SetArg("root=", "/dev/mmcblk0p3")
	bootCfg.CmdLine.SetFlag("sysota.try")
	c.Assert(bootCfg.SetOSPrefix("slot-b/"), IsNil)
	c.Assert(bootCfg.SetCmdLineFile("cmdline.try"), IsNil)

	// Verify all the essentials computed values make sense.
	cmdlineFile, _, err := bootCfg.ComputedCmdLineFile()
	c.Assert(err, IsNil)
	c.Check(cmdlineFile, Equals, "slot-b/cmdline.try")

	kernel, _, err := bootCfg.ComputedKernel()
	c.Assert(err, IsNil)
	c.Check(kernel, Equals, "slot-b/kernel7l.img")

	// Save!

	c.Assert(bootCfg.Save(), IsNil)

	// Observe what we expected.

	data, err := os.ReadFile(filepath.Join(s.bootDir, "tryboot.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "disable_overscan=1"+"\n"+"os_prefix=slot-b/"+"\n"+"cmdline=cmdline.try"+"\n")

	data, err = os.ReadFile(filepath.Join(s.bootDir, "slot-b", "cmdline.try"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "root=/dev/mmcblk0p3 sysota.try")

	// Original config.txt was not modified.

	data, err = os.ReadFile(filepath.Join(s.bootDir, "slot-b", "config.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "disable_overscan=1\n")

	// Original cmdline.txt was not modified.

	data, err = os.ReadFile(filepath.Join(s.bootDir, "slot-b", "cmdline.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "root=/dev/mmcblk0p2")
}

func (s *bootConfigSuite) TestNewBootConfig(c *C) {
	bootCfg, err := piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, IsNil)

	c.Check(bootCfg.BootMountPoint, Equals, s.bootDir)
	c.Check(bootCfg.ConfigFileName, Equals, "config.txt")
	c.Check(bootCfg.RevisionCode(), Equals, pimodel.RevisionCode(Pi4BRevCode))
	c.Check(bootCfg.SerialNumber(), Equals, pimodel.SerialNumber(sampleSerial))

	// Since we all for the absence of the configuration file, providing a bogus
	// directory is technically not an error.
	_, err = piboot.NewBootConfig(filepath.Join(s.bootDir, "missing"), Pi4BRevCode, sampleSerial)
	c.Assert(err, IsNil)

	// Providing a bogus revision fails early on, since we look for the board type.
	_, err = piboot.NewBootConfig(s.bootDir, 0, sampleSerial)
	c.Assert(err, ErrorMatches, `cannot use boot configuration: cannot decode revision code 0000, unknown board type`)

	// Wrapped errors can be unwrapped.
	c.Assert(errors.Unwrap(err), ErrorMatches, `cannot decode revision code 0000, unknown board type`)

	// An existing but corrupted ConfigTxt is detected early on.
	// Depending on where the corruption is (invalid syntax vs valid but unrecognized syntax), the
	// exact error varies but both cases are reported correctly.
	c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(`[Corrupted`), 0o600), IsNil)
	_, err = piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, ErrorMatches, `cannot use boot configuration: line 1: cannot parse parameter out of "\[Corrupted"`)

	c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(`[Bogus]`), 0o600), IsNil)
	_, err = piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, ErrorMatches, `cannot use boot configuration: conditional filter error "\[Bogus\]": unknown filter`)

	// Since NewBootConfig loads the command line file, it can also fail, in a
	// pathological case (e.g. dangling symlink or directory instead of a file).
	c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(``), 0o600), IsNil)
	c.Assert(os.Mkdir(filepath.Join(s.bootDir, "cmdline.txt"), 0o755), IsNil)
	_, err = piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, ErrorMatches, `cannot use boot configuration: read .*/cmdline.txt: is a directory`)

	// For the same reason, if config.txt is a directory, we can fail while trying to open it for reading.
	c.Assert(os.Remove(filepath.Join(s.bootDir, "config.txt")), IsNil)
	c.Assert(os.Mkdir(filepath.Join(s.bootDir, "config.txt"), 0o755), IsNil)
	_, err = piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, ErrorMatches, `cannot use boot configuration: read .*/config.txt: is a directory`)
}

func (s *bootConfigSuite) TestNewTryBootConfig(c *C) {
	bootCfg, err := piboot.NewTryBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, IsNil)

	c.Check(bootCfg.BootMountPoint, Equals, s.bootDir)
	c.Check(bootCfg.ConfigFileName, Equals, "tryboot.txt")
	c.Check(bootCfg.RevisionCode(), Equals, pimodel.RevisionCode(Pi4BRevCode))
	c.Check(bootCfg.SerialNumber(), Equals, pimodel.SerialNumber(sampleSerial))
}

func (s *bootConfigSuite) TestEmptyBootDir(c *C) {
	bootCfg, err := piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, IsNil)

	cmdlineName, isOptional, err := bootCfg.ComputedCmdLineFile()
	c.Assert(err, IsNil)
	c.Check(cmdlineName, Equals, "cmdline.txt")
	c.Check(isOptional, Equals, true)
}

func (s *bootConfigSuite) TestComputedOSPrefix(c *C) {
	var revCode pimodel.RevisionCode

	for _, t := range []struct {
		configTxt string
		isDefault bool
		osPrefix  string

		stickyRevCode pimodel.RevisionCode
	}{
		{
			stickyRevCode: Pi4BRevCode, // sticky for remaining tests
			isDefault:     true,
		},
		{
			configTxt: "os_prefix=foo",
			osPrefix:  "foo",
		},
		{
			configTxt: "os_prefix=foo/",
			osPrefix:  "foo/",
		},
		{
			configTxt: "upstream_kernel=1",
			osPrefix:  "upstream/",
			isDefault: true,
		},
		{
			configTxt: "os_prefix=foo\n" + "upstream_kernel=1",
			osPrefix:  "foo",
		},
		{
			configTxt: "upstream_kernel=1\n" + "os_prefix=foo",
			osPrefix:  "foo",
		},
	} {
		if t.stickyRevCode != 0 {
			revCode = t.stickyRevCode
		}

		comment := Commentf("t: %#v", t)

		c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(t.configTxt), 0o600), IsNil, comment)

		bootCfg, err := piboot.NewBootConfig(s.bootDir, revCode, sampleSerial)
		c.Assert(err, IsNil, comment)

		osPrefix, isDefault, err := bootCfg.ComputedOSPrefix()
		c.Assert(err, IsNil, comment)
		c.Check(osPrefix, DeepEquals, t.osPrefix, comment)
		c.Check(isDefault, Equals, t.isDefault, comment)
	}
}

func (s *bootConfigSuite) TestComputedOverlayPrefix(c *C) {
	var revCode pimodel.RevisionCode

	for _, t := range []struct {
		configTxt     string
		isDefault     bool
		overlayPrefix string
		readmePath    string

		stickyRevCode pimodel.RevisionCode
	}{
		{
			stickyRevCode: Pi4BRevCode, // sticky for remaining tests
			isDefault:     true,
			overlayPrefix: "overlays/",
		},
		{
			configTxt:     "upstream_kernel=1",
			overlayPrefix: "overlays/",
			isDefault:     true,
		},
		{
			// upstream_kernel=1 does not affect overlays unless the README file is present.
			configTxt:     "upstream_kernel=1",
			readmePath:    "upstream/overlays/README",
			overlayPrefix: "upstream/overlays/",
			isDefault:     true,
		},
		{
			configTxt:     "os_prefix=slot-a/",
			overlayPrefix: "overlays/",
			isDefault:     true,
		},
		{
			// os_prefix= does not affect default overlays unless the README file is present.
			configTxt:     "os_prefix=slot-a/",
			overlayPrefix: "slot-a/overlays/",
			readmePath:    "slot-a/overlays/README",
			isDefault:     true,
		},
		{
			configTxt:     "overlay_prefix=custom/",
			overlayPrefix: "custom/",
		},
		{
			configTxt:     "overlay_prefix=custom-",
			overlayPrefix: "custom-",
		},
		{
			configTxt:     "os_prefix=slot-a/" + "\n" + "overlay_prefix=custom/",
			overlayPrefix: "custom/",
		},
		{
			configTxt:     "os_prefix=slot-a/" + "\n" + "overlay_prefix=custom/",
			readmePath:    "slot-a/custom/README",
			overlayPrefix: "slot-a/custom/",
		},
	} {
		if t.stickyRevCode != 0 {
			revCode = t.stickyRevCode
		}

		comment := Commentf("t: %#v", t)

		c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(t.configTxt), 0o600), IsNil, comment)

		readmePathName := filepath.Join(s.bootDir, t.readmePath)
		if t.readmePath != "" {
			c.Assert(os.MkdirAll(filepath.Dir(readmePathName), 0o755), IsNil, comment)
			c.Assert(os.WriteFile(readmePathName, []byte("# Non-empty"), 0o600), IsNil, comment)
		}

		bootCfg, err := piboot.NewBootConfig(s.bootDir, revCode, sampleSerial)
		c.Assert(err, IsNil, comment)

		overlayPrefix, isDefault, err := bootCfg.ComputedOverlayPrefix()
		c.Assert(err, IsNil, comment)
		c.Check(overlayPrefix, DeepEquals, t.overlayPrefix, comment)
		c.Check(isDefault, Equals, t.isDefault, comment)

		if t.readmePath != "" {
			c.Assert(os.Remove(readmePathName), IsNil)
		}
	}
}

func (s *bootConfigSuite) TestDefaultCmdLine(c *C) {
	bootCfg, err := piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, IsNil)

	c.Check(bootCfg.CmdLine, Equals, boot.CmdLine("console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 rootwait"))
}

func (s *bootConfigSuite) TestComputedCmdline(c *C) {
	for _, t := range []struct {
		configTxt string
		isDefault bool
		cmdline   string
		touch     string
	}{
		{
			isDefault: true,
			cmdline:   "cmdline.txt",
		},
		{
			configTxt: "os_prefix=foo-",
			cmdline:   "foo-cmdline.txt",
			isDefault: true,
		},
		{
			configTxt: "upstream_kernel=1",
			cmdline:   "upstream/cmdline.txt",
			isDefault: true,
		},
		{
			configTxt: "cmdline=cmdline.txt",
			cmdline:   "cmdline.txt",
			touch:     "cmdline.txt",
		},
		{
			configTxt: "os_prefix=slot-a/" + "\n" + "cmdline=cmdline.txt",
			cmdline:   "slot-a/cmdline.txt",
			touch:     "slot-a/cmdline.txt",
		},
		{
			// cmdline=/cmdline.txt makes os_prefix= irrelevant.
			configTxt: "os_prefix=slot-a/" + "\n" + "cmdline=/cmdline.txt",
			cmdline:   "cmdline.txt",
			touch:     "cmdline.txt",
		},
		{
			configTxt: "upstream_kernel=1" + "\n" + "cmdline=custom.txt",
			cmdline:   "upstream/custom.txt",
			touch:     "upstream/custom.txt",
		},
		{
			// cmdline=/cmdline.txt makes upstream_kernel=1 irrelevant.
			configTxt: "upstream_kernel=1" + "\n" + "cmdline=/custom.txt",
			cmdline:   "custom.txt",
			touch:     "custom.txt",
		},
	} {
		comment := Commentf("t: %#v", t)

		c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(t.configTxt), 0o600), IsNil, comment)

		if t.touch != "" {
			c.Assert(os.MkdirAll(filepath.Dir(filepath.Join(s.bootDir, t.touch)), 0o700), IsNil, comment)
			c.Assert(os.WriteFile(filepath.Join(s.bootDir, t.touch), nil, 0o600), IsNil, comment)
		}

		bootCfg, err := piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
		c.Assert(err, IsNil, comment)

		cmdline, isDefault, err := bootCfg.ComputedCmdLineFile()
		c.Assert(err, IsNil, comment)
		c.Check(cmdline, DeepEquals, t.cmdline, comment)
		c.Check(isDefault, Equals, t.isDefault, comment)
	}
}

func (s *bootConfigSuite) TestComputedKernel(c *C) {
	var revCode pimodel.RevisionCode

	for _, t := range []struct {
		configTxt string
		isDefault bool
		kernel    string

		stickyRevCode pimodel.RevisionCode
	}{
		{
			stickyRevCode: Pi4BRevCode, // sticky for remaining tests
			isDefault:     true,
			kernel:        "kernel7l.img",
		},
		{
			configTxt: "kernel=linux.img",
			kernel:    "linux.img",
		},
		{
			configTxt: "os_prefix=slot-a/\nkernel=linux.img",
			kernel:    "slot-a/linux.img",
		},
		{
			configTxt: "os_prefix=slot-a/\nkernel=/linux.img",
			kernel:    "linux.img",
		},
		{
			// kernel=/linux.img makes os_prefix= irrelevant.
			configTxt: "os_prefix=/slot-a/\nkernel=/linux.img",
			kernel:    "linux.img",
		},
		{
			configTxt: "os_prefix=/slot-a/\nkernel=linux.img",
			kernel:    "slot-a/linux.img",
		},
		{
			configTxt: "upstream_kernel=1\nkernel=linux.img",
			kernel:    "upstream/linux.img",
		},
		{
			configTxt: "arm64_bit=1",
			kernel:    "kernel8.img",
			isDefault: true,
		},
		{
			configTxt: "arm64_bit=1\nupstream_kernel=1",
			kernel:    "upstream/kernel8.img",
			isDefault: true,
		},
		{
			configTxt: "arm64_bit=1\nkernel=linux.img",
			kernel:    "linux.img",
		},
	} {
		if t.stickyRevCode != 0 {
			revCode = t.stickyRevCode
		}

		comment := Commentf("t: %#v", t)

		c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(t.configTxt), 0o600), IsNil, comment)

		bootCfg, err := piboot.NewBootConfig(s.bootDir, revCode, sampleSerial)
		c.Assert(err, IsNil, comment)

		kernel, isDefault, err := bootCfg.ComputedKernel()
		c.Assert(err, IsNil, comment)
		c.Check(kernel, DeepEquals, t.kernel, comment)
		c.Check(isDefault, Equals, t.isDefault, comment)
	}
}

func (s *bootConfigSuite) TestComputedKernelDefaults32Bit(c *C) {
	boards := map[pimodel.RevisionCode]string{
		Pi0RevCode:         "kernel.img",
		Pi0WRevCode:        "kernel.img",
		Cm1RevCode:         "kernel.img",
		Pi1ARevCode:        "kernel.img",
		Pi1BRevCode:        "kernel.img",
		Pi2BRevCodeBcm2836: "kernel7.img",
		Pi2BRevCodeBcm2837: "kernel7.img",
		Pi3BRevCode:        "kernel7.img",
		Cm3RevCode:         "kernel7.img",
		Pi3APlusRevCode:    "kernel7.img",
		Pi3BPlusRevCode:    "kernel7.img",
		Pi4BRevCode:        "kernel7l.img",
		Cm4RevCode:         "kernel7l.img",
		Pi400RevCode:       "kernel7l.img",
	}

	for code, expectedKernel := range boards {
		comment := Commentf("revision code: %s, expected kernel: %s", code, expectedKernel)
		bootCfg, err := piboot.NewBootConfig(s.bootDir, code, sampleSerial)
		c.Assert(err, IsNil, comment)

		kernel, _, err := bootCfg.ComputedKernel()
		c.Assert(err, IsNil, comment)
		c.Check(kernel, Equals, expectedKernel, comment)
	}
}

func (s *bootConfigSuite) TestComputedKernelDefaults64Bit(c *C) {
	c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(`arm64_bit=1`), 0o600), IsNil)

	boards := []pimodel.RevisionCode{
		Pi0RevCode,
		Pi0WRevCode,
		Cm1RevCode,
		Pi1ARevCode,
		Pi1BRevCode,
		Pi2BRevCodeBcm2836,
		Pi2BRevCodeBcm2837,
		Pi3BRevCode,
		Cm3RevCode,
		Pi3APlusRevCode,
		Pi3BPlusRevCode,
		Pi4BRevCode,
		Cm4RevCode,
		Pi400RevCode,
	}

	for _, code := range boards {
		comment := Commentf("revision code: %s", code)
		bootCfg, err := piboot.NewBootConfig(s.bootDir, code, sampleSerial)
		c.Assert(err, IsNil, comment)

		kernel, _, err := bootCfg.ComputedKernel()
		c.Assert(err, IsNil, comment)
		// The kernel is set unconditionally, making it possible to create a non-booting configuration.
		c.Check(kernel, Equals, "kernel8.img", comment)
	}
}

func (s *bootConfigSuite) TestComputedInitrdFiles(c *C) {
	for _, t := range []struct {
		configTxt string
		isDefault bool
		files     []string
	}{
		{
			isDefault: true,
		},
		{
			configTxt: "ramfsfile=initrd.gz",
			files:     []string{"initrd.gz"},
		},
		{
			configTxt: "ramfsfile=a,b,c",
			files:     []string{"a", "b", "c"},
		},
		{
			configTxt: "initramfs initrd.gz followkernel",
			files:     []string{"initrd.gz"},
		},
		{
			configTxt: "os_prefix=slot-a/\ninitramfs initrd.gz followkernel",
			files:     []string{"slot-a/initrd.gz"},
		},
		{
			// initramfs /initrd.gz ... makes os_prefix= irrelevant.
			configTxt: "os_prefix=slot-a/\ninitramfs /initrd.gz followkernel",
			files:     []string{"initrd.gz"},
		},
		{
			configTxt: "upstream_kernel=1\ninitramfs initrd.gz followkernel",
			files:     []string{"upstream/initrd.gz"},
		},
		{
			configTxt: "ramfsfile=foo\ninitramfs bar followkernel",
			files:     []string{"bar"},
		},
		{
			configTxt: "initramfs bar followkernel\nramfsfile=foo",
			files:     []string{"foo"},
		},
	} {
		comment := Commentf("t: %#v", t)

		c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(t.configTxt), 0o600), IsNil, comment)

		bootCfg, err := piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
		c.Assert(err, IsNil, comment)

		files, isDefault, err := bootCfg.ComputedInitrdFiles()
		c.Assert(err, IsNil, comment)
		c.Check(files, DeepEquals, t.files, comment)
		c.Check(isDefault, Equals, t.isDefault, comment)
	}
}

func (s *bootConfigSuite) TestComputedDeviceTreeFiles(c *C) {
	var stickyRevCode pimodel.RevisionCode

	for _, t := range []struct {
		stickyRevCode pimodel.RevisionCode
		revCode       pimodel.RevisionCode

		configTxt  string
		isDefault  bool
		files      []string
		errPattern string
	}{
		{
			revCode:   Pi0RevCode,
			isDefault: true,
			files:     []string{"bcm2708-rpi-zero.dtb"},
		},
		{
			revCode:   Pi0WRevCode,
			isDefault: true,
			files:     []string{"bcm2708-rpi-zero-w.dtb"},
		},
		{
			revCode:   Pi1ARevCode,
			isDefault: true,
			files:     []string{"bcm2708-rpi-a.dtb", "bcm2708-rpi-b.dtb"},
		},
		{
			revCode:   Pi1BRevCode,
			isDefault: true,
			files:     []string{"bcm2708-rpi-b.dtb"},
		},
		{
			revCode:   Pi1APlusRevCode,
			isDefault: true,
			files:     []string{"bcm2708-rpi-a-plus.dtb", "bcm2708-rpi-b-plus.dtb"},
		},
		{
			revCode:   Pi1BPlusRevCode,
			isDefault: true,
			files:     []string{"bcm2708-rpi-b-plus.dtb"},
		},
		{
			revCode:   Cm1RevCode,
			isDefault: true,
			files:     []string{"bcm2708-rpi-cm.dtb"},
		},
		{
			revCode:   Pi2BRevCodeBcm2836,
			isDefault: true,
			files:     []string{"bcm2709-rpi-2-b.dtb"},
		},
		{
			revCode:   Pi2BRevCodeBcm2837,
			isDefault: true,
			files:     []string{"bcm2710-rpi-2-b.dtb"},
		},
		{
			revCode:   Pi3BRevCode,
			isDefault: true,
			files:     []string{"bcm2710-rpi-3-b.dtb"},
		},
		{
			revCode:   Pi3APlusRevCode,
			isDefault: true,
			files:     []string{"bcm2710-rpi-3-a-plus.dtb", "bcm2710-rpi-3-b-plus.dtb"},
		},
		{
			revCode:   Pi3BPlusRevCode,
			isDefault: true,
			files:     []string{"bcm2710-rpi-3-b-plus.dtb"},
		},
		{
			revCode:   Cm3RevCode,
			isDefault: true,
			files:     []string{"bcm2710-rpi-cm3.dtb"},
		},
		{
			revCode:   Cm3PlusRevCode,
			isDefault: true,
			files:     []string{"bcm2710-rpi-cm3.dtb"}, // Same as plain CM3
		},
		{
			revCode:   Pi4BRevCode,
			isDefault: true,
			files:     []string{"bcm2711-rpi-4-b.dtb"},
		},
		{
			revCode:   Pi400RevCode,
			isDefault: true,
			files:     []string{"bcm2711-rpi-400.dtb"},
		},
		{
			revCode:   Cm4RevCode,
			isDefault: true,
			files:     []string{"bcm2711-rpi-cm4.dtb"},
		},
		// Upstream kernel mode (only some variants tested).
		{
			revCode:   Pi0RevCode,
			configTxt: "upstream_kernel=1",
			isDefault: true,
			files:     []string{"upstream/bcm2835-rpi-zero.dtb", "upstream/bcm2708-rpi-zero.dtb"},
		},
		{
			revCode:   Pi3BPlusRevCode,
			configTxt: "upstream_kernel=1",
			isDefault: true,
			files:     []string{"upstream/bcm2837-rpi-3-b-plus.dtb", "upstream/bcm2710-rpi-3-b-plus.dtb"},
		},
		{
			// BCM2711 is both the package and silicon family name and no duplication is necessary.
			revCode:   Pi4BRevCode,
			configTxt: "upstream_kernel=1",
			isDefault: true,
			files:     []string{"upstream/bcm2711-rpi-4-b.dtb"},
		},
		// Misc tests.
		{
			configTxt: "os_prefix=slot-a/",
			isDefault: true,
			files:     []string{"slot-a/bcm2711-rpi-4-b.dtb"},

			stickyRevCode: Pi4BRevCode,
		},
		{
			configTxt: "os_prefix=slot-a/" + "\n" + "device_tree=custom.dtb",
			files:     []string{"slot-a/custom.dtb"},
		},
		{
			// device_tree=/custom.dtb makes os_prefix= irrelevant.
			configTxt: "os_prefix=slot-a/" + "\n" + "device_tree=/custom.dtb",
			files:     []string{"custom.dtb"},
		},
		{
			configTxt: "device_tree=foo.dtb",
			files:     []string{"foo.dtb"},
		},
	} {
		if t.stickyRevCode != 0 {
			stickyRevCode = t.stickyRevCode
		}

		revCode := stickyRevCode

		if t.revCode != 0 {
			revCode = t.revCode
		}

		comment := Commentf("t: %#v", t)
		c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(t.configTxt), 0o600), IsNil, comment)
		bootCfg, err := piboot.NewBootConfig(s.bootDir, revCode, sampleSerial)
		c.Assert(err, IsNil, comment)

		files, isDefault, err := bootCfg.ComputedDeviceTreeFiles()

		if t.errPattern == "" {
			c.Assert(err, IsNil, comment)
			c.Check(files, DeepEquals, t.files, comment)
			c.Check(isDefault, Equals, t.isDefault, comment)
		} else {
			c.Assert(err, ErrorMatches, t.errPattern)

		}
	}
}

func (s *bootConfigSuite) TestNonCoherentNonMatchingOSPrefix(c *C) {
	// The os_prefix= parameter must be coherent among all the sections, even
	// those not matching the current model. Here the Pi4B model matches [pi4]
	// but not the [pi3] above it.
	c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(`
[pi3]
os_prefix=potato/
[pi4]
os_prefix=tomato/
`), 0o600), IsNil)

	_, err := piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, ErrorMatches, `conflicting definition of os_prefix, `+
		`first defined in .*/config.txt:3 as potato/, `+
		`then in .*/config.txt:5 as tomato/`)
}

func (s *bootConfigSuite) TestNonCoherentMatchingOSPrefix(c *C) {
	// The os_prefix= parameter must be coherent among all the sections matching
	// the current model. Here the Pi4B model matches both [pi4] and [all].
	c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(`
[pi4]
os_prefix=potato/
[all]
os_prefix=tomato/
`), 0o600), IsNil)

	_, err := piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, ErrorMatches, `conflicting definition of os_prefix, `+
		`first defined in .*/config.txt:3 as potato/, `+
		`then in .*/config.txt:5 as tomato/`)
}

func (s *bootConfigSuite) TestConditionalSections(c *C) {
	configTxtName := filepath.Join(s.bootDir, "config.txt")

	c.Assert(os.WriteFile(configTxtName, []byte(`
[all]
cmdline=all.txt

[pi3]
cmdline=pi3.txt

[pi4]
cmdline=pi4.txt

[none]
cmdline=none.txt
`), 0o600), IsNil)

	allCmdline := filepath.Join(s.bootDir, "all.txt")
	c.Assert(os.WriteFile(allCmdline, []byte(`all`), 0o600), IsNil)

	pi3Cmdline := filepath.Join(s.bootDir, "pi3.txt")
	c.Assert(os.WriteFile(pi3Cmdline, []byte(`pi3`), 0o600), IsNil)

	pi4Cmdline := filepath.Join(s.bootDir, "pi4.txt")
	c.Assert(os.WriteFile(pi4Cmdline, []byte(`pi4`), 0o600), IsNil)

	noneCmdline := filepath.Join(s.bootDir, "none.txt")
	c.Assert(os.WriteFile(noneCmdline, []byte(`none`), 0o600), IsNil)

	bootCfg, err := piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, IsNil)

	cmdlineName, isOptional, err := bootCfg.ComputedCmdLineFile()
	c.Assert(err, IsNil)
	c.Check(cmdlineName, Equals, "pi4.txt")
	c.Check(isOptional, Equals, false)

	c.Check(bootCfg.CmdLine, Equals, boot.CmdLine("pi4"))

	// When the model is changed, we get the expected results.
	bootCfg, err = piboot.NewBootConfig(s.bootDir, Cm1RevCode, sampleSerial)
	c.Assert(err, IsNil)

	cmdlineName, isOptional, err = bootCfg.ComputedCmdLineFile()
	c.Assert(err, IsNil)
	c.Check(cmdlineName, Equals, "all.txt")
	c.Check(isOptional, Equals, false)

	c.Check(bootCfg.CmdLine, Equals, boot.CmdLine("all"))

	bootCfg, err = piboot.NewBootConfig(s.bootDir, Pi3BRevCode, sampleSerial)
	c.Assert(err, IsNil)

	cmdlineName, isOptional, err = bootCfg.ComputedCmdLineFile()
	c.Assert(err, IsNil)
	c.Check(cmdlineName, Equals, "pi3.txt")
	c.Check(isOptional, Equals, false)

	c.Check(bootCfg.CmdLine, Equals, boot.CmdLine("pi3"))
}

func (s *bootConfigSuite) TestMissingCmdline(c *C) {
	configTxtName := filepath.Join(s.bootDir, "config.txt")

	c.Assert(os.WriteFile(configTxtName, []byte(`
cmdline=missing.txt
`), 0o600), IsNil)

	_, err := piboot.NewBootConfig(s.bootDir, Pi4BRevCode, sampleSerial)
	c.Assert(err, ErrorMatches, "cannot use boot configuration: open .*missing.txt: no such file or directory")
}
