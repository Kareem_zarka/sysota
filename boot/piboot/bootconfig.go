// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package piboot

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/zygoon/go-raspi/picfg"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
	"gitlab.com/zygoon/go-raspi/pimodel"

	"gitlab.com/zygoon/sysota/boot"
)

const (
	arm64BitParamName       = "arm64_bit"
	cmdLineParamName        = "cmdline"
	kernelParamName         = "kernel"
	osPrefixParamName       = "os_prefix"
	overlayPrefixParamName  = "overlay_prefix"
	ramfsfileParamName      = "ramfsfile"
	initramfsParamName      = "initramfs"
	upstreamKernelParamName = "upstream_kernel"
	deviceTreeParamName     = "device_tree"

	// defaultCmdLine is the default command line baked into start.elf.
	defaultCmdLine = "console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 rootwait"
)

// BootConfig describes configuration of the boot system.
//
// A Raspberry Pi boot configuration comprises the contents of the FAT
// boot partition and, on certain models, the contents EEPROM.
//
// This type does not implement EEPROM handling yet.
//
// Configuration comprises:
// - The Raspberry Pi model revision code and serial number.
// - The contents of config.txt or tryboot.txt files
// - The designated command line file
// - The designated kernel, initrd files and device tree files
// - The README marker file in case overlay_prefix= and os_prefix= are both used.
//
// At runtime, the config file is interpreted by the first stage and second
// stage bootloaders. This type assists in determining the set of parameters
// that affect the boot system as well as manipulating it to the extent that
// SystemOTA requires.
type BootConfig struct {
	// Data provided explicitly which can be manipulated at will.

	BootMountPoint string
	ConfigFileName string

	// Data provided explicitly which affects data loaded below
	// and cannot be changed without re-creating the instance.

	revCode   pimodel.RevisionCode
	serialNum pimodel.SerialNumber
	model     pimodel.Model // Derived from revCode.

	// Data loaded from the file system.

	ConfigTxt picfg.ConfigTxt
	CmdLine   boot.CmdLine

	// Simulator for reading and writing parameters in ConfigTxt.
	sim *condfilter.Simulator
}

// NewBootConfig loads and analyzes config.txt from the given boot mount point.
func NewBootConfig(bootMountPoint string, revCode pimodel.RevisionCode, serialNum pimodel.SerialNumber) (*BootConfig, error) {
	return newBootConfigFile(bootMountPoint, configTxtName, revCode, serialNum)
}

// NewTryBootConfig loads and analyzes tryboot.txt from the given boot mount point.
func NewTryBootConfig(bootMountPoint string, revCode pimodel.RevisionCode, serialNum pimodel.SerialNumber) (*BootConfig, error) {
	return newBootConfigFile(bootMountPoint, trybootTxtName, revCode, serialNum)
}

// RevisionCode returns the Raspberry Pi revision code passed to NewBootConfig or NewTryBootConfig.
func (bootCfg *BootConfig) RevisionCode() pimodel.RevisionCode {
	return bootCfg.revCode
}

// SerialNumber returns the Raspberry Pi serial number passed to NewBootConfig or NewTryBootConfig.
func (bootCfg *BootConfig) SerialNumber() pimodel.SerialNumber {
	return bootCfg.serialNum
}

func newBootConfigFile(bootMountPoint, configFileName string, revCode pimodel.RevisionCode, serialNum pimodel.SerialNumber) (*BootConfig, error) {
	bootCfg := &BootConfig{
		BootMountPoint: bootMountPoint,
		ConfigFileName: configFileName,
		revCode:        revCode,
		serialNum:      serialNum,
	}

	model, err := revCode.BoardType()
	if err != nil {
		return nil, &BootConfigError{Err: err}
	}

	bootCfg.model = model

	if err := bootCfg.loadConfigFile(configFileName); err != nil {
		return nil, &BootConfigError{Err: err}
	}

	sim, err := condfilter.NewSimulator(&bootCfg.ConfigTxt, bootCfg.revCode, serialNum)
	if err != nil {
		return nil, &BootConfigError{Err: err}
	}

	bootCfg.sim = sim

	cmdLineFile, isOptional, err := bootCfg.ComputedCmdLineFile()
	if err != nil {
		return nil, err
	}

	if err := bootCfg.loadCmdLineFile(cmdLineFile, isOptional); err != nil {
		return nil, &BootConfigError{Err: err}
	}

	return bootCfg, nil
}

// Save saves the boot configuration to the filesystem.
//
// The ConfigFileName and BootMountPoint can be freely modified before calling
// this function. Missing directories are created if necessary.
func (bootCfg *BootConfig) Save() error {
	cmdLineFileName, _, err := bootCfg.ComputedCmdLineFile()
	if err != nil {
		return err
	}

	if err := bootCfg.saveConfigFile(bootCfg.ConfigFileName); err != nil {
		return err
	}

	return bootCfg.saveCmdLineFile(cmdLineFileName)
}

// loadConfigFile loads and decodes the given config file, relative to BootMountPoint.
func (bootCfg *BootConfig) loadConfigFile(configFileName string) error {
	pathName := filepath.Join(bootCfg.BootMountPoint, configFileName)

	f, err := os.Open(pathName)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if os.IsNotExist(err) {
		return nil
	}

	defer func() { _ = f.Close() }()

	return picfg.NewDecoder(f).Decode(&bootCfg.ConfigTxt)
}

// saveConfigFile saves the data from ConfigTxt to the given config file, relative to BootMountPoint.
func (bootCfg *BootConfig) saveConfigFile(configFileName string) error {
	pathName := filepath.Join(bootCfg.BootMountPoint, configFileName)

	if err := os.MkdirAll(filepath.Dir(pathName), 0o755); err != nil {
		return err
	}

	f, err := os.OpenFile(pathName, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0o644)
	if err != nil {
		return err
	}

	defer func() {
		_ = f.Close()
	}()

	return picfg.NewEncoder(f).Encode(bootCfg.ConfigTxt)
}

// loadCmdLineFile loads CmdLine from the given file.
func (bootCfg *BootConfig) loadCmdLineFile(cmdlineFileName string, isOptional bool) error {
	pathName := filepath.Join(bootCfg.BootMountPoint, cmdlineFileName)

	data, err := os.ReadFile(pathName)
	if err != nil {
		// If the file is required or if the error is something other than ErrNotExist then return the error.
		if !isOptional || !errors.Is(err, os.ErrNotExist) {
			return err
		}

		bootCfg.CmdLine = defaultCmdLine
	} else {
		bootCfg.CmdLine = boot.CmdLine(bytes.TrimSpace(data))
	}

	return nil
}

// saveCmdLine file saves the CmdLine to the given file.
func (bootCfg *BootConfig) saveCmdLineFile(cmdLineFileName string) error {
	pathName := filepath.Join(bootCfg.BootMountPoint, cmdLineFileName)

	if err := os.MkdirAll(filepath.Dir(pathName), 0o755); err != nil {
		return err
	}

	return os.WriteFile(pathName, []byte(bootCfg.CmdLine), 0o644)
}

// SetOSPrefix sets os_prefix= parameter.
//
// The prefix is set when the value is coherent.
func (bootCfg *BootConfig) SetOSPrefix(value string) error {
	return bootCfg.sim.SetCoherentParameter(osPrefixParamName, value)
}

// SetCmdLineFile sets cmdline= parameter.
//
// The cmdline= is set when the value is coherent.
func (bootCfg *BootConfig) SetCmdLineFile(value string) error {
	return bootCfg.sim.SetCoherentParameter(cmdLineParamName, value)
}

// Arm64Bit returns the value of the "arm64_bit=" parameter.
//
// The parameter is considered true only when the string value is equal to "1".
func (bootCfg *BootConfig) Arm64Bit() (arm64Bit bool, isDefault bool, err error) {
	arm64BitParam := bootCfg.sim.Parameter(arm64BitParamName)
	if arm64BitParam != nil {
		// NOTE(zyga): I've tested that both "0" and "2" pick the 32bit kernel
		// while exactly "1" picks the 64bit kernel.
		arm64Bit = arm64BitParam.Value == "1"
	} else {
		// By default, arm64_bit= is inactive.
		arm64Bit = false
		isDefault = true
	}

	return arm64Bit, isDefault, nil
}

// UpstreamKernel returns the value of the upstream_kernel= parameter.
//
// The parameter is considered true only when the string value is equal to "1".
func (bootCfg *BootConfig) UpstreamKernel() (upstreamKernel bool, isDefault bool, err error) {
	upstreamKernelParam := bootCfg.sim.Parameter(upstreamKernelParamName)
	if upstreamKernelParam != nil {
		upstreamKernel = upstreamKernelParam.Value == "1"
	} else {
		// By default, upstream_kernel= is inactive.
		upstreamKernel = false
		isDefault = true
	}

	return upstreamKernel, isDefault, nil
}

// ComputedOSPrefix returns the effective value of the os_prefix= parameter.
//
// The os_prefix= parameter must be *coherent* according to the definition
// in CoherentParameter.
func (bootCfg *BootConfig) ComputedOSPrefix() (osPrefix string, isDefault bool, err error) {
	osPrefixParam, err := bootCfg.sim.CoherentParameter(osPrefixParamName)
	if err != nil {
		return "", false, err
	}

	upstreamKernel, _, err := bootCfg.UpstreamKernel()
	if err != nil {
		return "", false, err
	}

	if osPrefixParam != nil {
		osPrefix = osPrefixParam.Value
	} else {
		// The default value is no prefix. Using upstream_kernel=1 changes the
		// default to "upstream/".
		if upstreamKernel {
			osPrefix = "upstream/"
		}
		isDefault = true
	}

	return osPrefix, isDefault, nil
}

// ComputedOverlayPrefix returns the effective value of the overlay_prefix= parameter.
//
// The computed value depends on the computed OS prefix, on the presence of
// overlay_prefix= parameter *and* on the existence of a README file under a
// particular location.
//
// The overlay_prefix= parameter must be *coherent* according to the definition
// in CoherentParameter.
func (bootCfg *BootConfig) ComputedOverlayPrefix() (overlayPrefix string, isDefault bool, err error) {
	overlayPrefixParam, err := bootCfg.sim.CoherentParameter(overlayPrefixParamName)
	if err != nil {
		return "", false, err
	}

	if overlayPrefixParam != nil {
		overlayPrefix = overlayPrefixParam.Value
	} else {
		overlayPrefix = "overlays/"
		isDefault = true
	}

	osPrefix, _, err := bootCfg.ComputedOSPrefix()
	if err != nil {
		return "", false, err
	}

	if osPrefix != "" {
		readmePathName := filepath.Join(bootCfg.BootMountPoint, osPrefix+overlayPrefix+"README")

		fi, err := os.Stat(readmePathName)
		if err != nil && !os.IsNotExist(err) {
			return "", false, err
		}

		// If ${os_prefix}${overlay_prefix}README is not a regular file then
		// os_prefix= is ignored for the purpose of finding overlays.
		if os.IsNotExist(err) || !fi.Mode().IsRegular() {
			osPrefix = ""
		}
	}

	return osPrefix + overlayPrefix, isDefault, nil
}

// ComputedCmdLineFile returns the computed value of cmdline= parameter.
//
// The upstream_kernel=1 parameter causes os_prefix to be set to "upstream/",
// unless it was explicitly assigned by the configuration file. The cmdline
// defaults to "cmdline.txt".
//
// The returned name is "${os_prefix}${cmdline}", relative to BootMountDir.
func (bootCfg *BootConfig) ComputedCmdLineFile() (cmdline string, isDefault bool, err error) {
	osPrefix, _, err := bootCfg.ComputedOSPrefix()
	if err != nil {
		return "", false, err
	}

	cmdLineParam := bootCfg.sim.Parameter(cmdLineParamName)
	if cmdLineParam != nil {
		cmdline = cmdLineParam.Value
	} else {
		// The default is "cmdline.txt".
		cmdline = "cmdline.txt"
		isDefault = true
	}

	return joinOsPrefix(osPrefix, cmdline), isDefault, nil
}

// ComputedKernel returns the computed value of kernel= parameter.
//
// The upstream_kernel=1 parameter causes os_prefix to be set to "upstream/",
// unless it was explicitly assigned by the configuration file. The kernel
// defaults to "kernel.img", "kernel7.img", "kernel7l.img" or "kernel8.img",
// depending on the capabilities of the hardware and on the computed value of
// the arm64_bit= parameter.
//
// The returned name is "${os_prefix}${kernel}", relative to BootMountDir.
func (bootCfg *BootConfig) ComputedKernel() (kernel string, isDefault bool, err error) {
	osPrefix, _, err := bootCfg.ComputedOSPrefix()
	if err != nil {
		return "", false, err
	}

	kernelParam := bootCfg.sim.Parameter(kernelParamName)
	if kernelParam != nil {
		kernel = kernelParam.Value
	} else {
		// The default value depends on the revision code and arm_64bit= flag.
		arm64Bit, _, err := bootCfg.Arm64Bit()
		if err != nil {
			return "", false, err
		}

		kernel, err = defaultKernel(bootCfg.revCode, arm64Bit)
		if err != nil {
			return "", false, err
		}
		isDefault = true

	}

	return joinOsPrefix(osPrefix, kernel), isDefault, nil
}

// ComputedDeviceTreeFiles returns the computed value of device_tree= parameter.
//
// The upstream_kernel=1 parameter causes os_prefix to be set to "upstream/",
// unless it was explicitly assigned by the configuration file. The device tree
// depends on the value of upstream_kernel and the system-on-chip used.
//
// The returned name is "${os_prefix}${device_tree}", relative to BootMountDir.
func (bootCfg *BootConfig) ComputedDeviceTreeFiles() (files []string, isDefault bool, err error) {
	deviceTreeParam := bootCfg.sim.Parameter(deviceTreeParamName)

	if deviceTreeParam != nil {
		files = []string{deviceTreeParam.Value}
	} else {
		// The default value depends on the revision code and upstream_kernel= flag.
		upstreamKernel, _, err := bootCfg.UpstreamKernel()
		if err != nil {
			return nil, false, err
		}

		files, err = defaultDeviceTree(bootCfg.revCode, upstreamKernel)
		isDefault = true
		if err != nil {
			return nil, false, err
		}
	}

	osPrefix, _, err := bootCfg.ComputedOSPrefix()
	if err != nil {
		return nil, false, err
	}

	for idx := range files {
		files[idx] = joinOsPrefix(osPrefix, files[idx])
	}

	return files, isDefault, err
}

// ComputedInitrdFiles returns the list of ramdisk files to concatenate and load.
//
// The files are defined by either the special initramfs parameter or by the
// ramfsfile= parameter. The later definition wins. In both cases, the files
// observe the OS prefix mechanism, either configured directly or by the
// upstream_kernel=1 parameter.
func (bootCfg *BootConfig) ComputedInitrdFiles() (files []string, isDefault bool, err error) {
	initramfsParam := bootCfg.sim.Parameter(initramfsParamName)
	ramfsfileParam := bootCfg.sim.Parameter(ramfsfileParamName)

	// When both are present, the later definition wins.
	if param := pickParam(ramfsfileParam, initramfsParam); param != nil {
		fileListString := param.Value
		if param == initramfsParam {
			// Note that initramfs couples file (list) and an expression
			// encoding the load address. We only want to find the file list
			// here, so take the first argument.
			fileListString = strings.SplitN(fileListString, " ", 2)[0]
		}

		// Firmware allows defining comma-separated list of files that are
		// concatenated in memory.
		files = strings.Split(fileListString, ",")
	}

	// Prefix files with the computed value of OS prefix.
	osPrefix, _, err := bootCfg.ComputedOSPrefix()
	if err != nil {
		return nil, false, err
	}

	for idx := range files {
		files[idx] = joinOsPrefix(osPrefix, files[idx])
	}

	return files, files == nil, nil
}

// BootConfigError reports problems with boot configuration.
type BootConfigError struct {
	Err error
}

// Error implements the error interface.
func (e *BootConfigError) Error() string {
	return fmt.Sprintf("cannot use boot configuration: %s", e.Err)
}

// Unwrap returns the error wrapped inside a BootConfigError.
func (e *BootConfigError) Unwrap() error {
	return e.Err
}
